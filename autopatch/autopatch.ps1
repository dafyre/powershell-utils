
#USE PSWINDOWSUPDATE module to Install Updates & Reboot

$user = [Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()
$isAdmin = $user.isInRole([Security.Principal.WindowsBuiltInRole] "Administrator")
$patchFile="C:\Drivers\oktopatch"

#sudo
if ($isAdmin -eq $false) {
    write-host "Not an admin!"

    $CommandLine = "-File `"" + $MyInvocation.MyCommand.Definition + "`" $paramInfo"
    #Start-Process -FilePath Powershell.exe -Verb RunAs -ArgumentList $CommandLine
    Start-Process -FilePath Powershell.exe -Verb RunAs -ArgumentList $CommandLine
    exit 0
}


#Settings
$MAIL_FROM="$($env:computername)@gcsu.edu"
$MAIL_TO="brant.wells@gcsu.edu"
$MAINT_WINDOW=5..7
$SMTP_SERVER="o365smtp.gcsu.edu"

$logDate=(get-date).tostring("MM-dd-yyyy_HHmms")
mkdir -force "C:\Drivers\Scripts\Logs" | Out-Null


$logFile="C:\Drivers\Scripts\Logs\autopatch_$logDate.log"
$logCleanup=(get-date).adddays(-14)

start-transcript -f $logFile
$doPatch=$false

while ($doPatch -eq $false -and ((get-date).hour -in $MAINT_WINDOW)) {

    $doPatch=test-path $patchFile

    if ($doPatch -eq $true) {
        del  $patchFile
    } else {
        $doPatch=$false
        write-host "$patchFile not found during maintenace window.  Not patching."
        send-mailmessage -from $MAIL_FROM -to $MAIL_TO -SmtpServer $SMTP_SERVER -Attachments $logFile -Subject "[ALERT] Autopatch NOT Completed"
    
    }
}

if ((get-date).hour -notin $MAINT_WINDOW) {
    write-host "Script run outside of maintenance window.  Not Patching!"
    stop-transcript

    send-mailmessage -from $MAIL_FROM -to $MAIL_TO -SmtpServer $SMTP_SERVER -Attachments $logFile -Subject "[ALERT] Autopatch Outside of Window"
    exit -2048
}


write-host "Cleaing old log files..."

Get-ChildItem *.log -erroraction silentlycontinue | Where {$_.CreationTime -lt $logCleanup} | foreach {
    write-host "Removing $_" 
    Remove-Item $_ -Force
}

$doReboot=$false

try {
    install-windowsupdate -acceptall -ignorereboot
    $doReboot=$true
}  catch {
    $doReboot=$false
}

if ($doReboot -eq $true) {
    write-host "Updates Completed.  Rebooting computer."
    stop-transcript -erroraction SilentlyContinue | Out-Null
    
    send-mailmessage -from $MAIL_FROM -to $MAIL_TO -SmtpServer $SMTP_SERVER -Attachments $logFile -Subject "Autopatch Completed"

    restart-computer -force
}

