<#

.DESCRIPTION
Scan the provided hostList or IPSubnet (between IPStart and IPEnd)

.EXAMPLE

To scan the 192.168.15.0/24 subnet for SSL Certs on Port 443, 8443, and 9443:
./sslcert_scanner.ps1 -hostList "192.168.15.1/24"  -Port "443","8443","9443"

To scan a single host using the default ports:

./sslcert_scanner.ps1 -hostList "my.example.com","192.168.10.1/24"


*NOTE:

By Default output is written to $logFile which is named certscanner.log.  To view the results afterwards, add the -display switch.
By Default Ports 443, and 8443 are checked.
By Default the full /24 (1 - 254) subnet is scanned when running with a specified subnet.

.DESCRIPTION
   Date       Updated By    Details
   05/02/2023     BW        Adjusted hostlist to use both IP/CIDR and Hostname format using / as the delimiter.
   11/22/2021     BW        Added PingCheck parameter for pinging a host.  If it's not up, skip it.
   11/19/2021     BW        Marked IPStart and IPEnd as not mandatory. .1 and .254 will scan the entire subnet listed.
   09/28/2021     BW        Initial Release.




#>
Param(
    [Parameter(ParameterSetName='hostList',Mandatory = $false) ] [string[]] $hostList,  #single host dns name, IP, or subnet in the form of a.b.c.d/yy
    [Parameter(Mandatory = $false) ] [switch] $pingCheck,
    [Parameter(Mandatory = $false) ] [string[]] $portList=("443","8443","8006","8445"),
    [Parameter(Mandatory = $false) ] [switch] $display,
    [Parameter(Mandatory = $false) ] [string] $logFile = "$PWD\certscanner.log",
    [Parameter(Mandatory = $false) ] [switch] $useDNS,
    [Parameter(Mandatory = $false) ] [bool] $save = $true
)

#Ignore Invalid SSL Certs
[Net.ServicePointManager]::ServerCertificateValidationCallback = { $true }

#$logFile="$PWD\certscanner.log"


function cidrToIpRange {
    param (
        [string] $cidrNotation
    )

    $addr, $maskLength = $cidrNotation -split '/'
    [int]$maskLen = 0
    if (-not [int32]::TryParse($maskLength, [ref] $maskLen)) {
        throw "Cannot parse CIDR mask length string: '$maskLen'"
    }
    if (0 -gt $maskLen -or $maskLen -gt 32) {
        throw "CIDR mask length must be between 0 and 32"
    }
    $ipAddr = [Net.IPAddress]::Parse($addr)
    if ($ipAddr -eq $null) {
        throw "Cannot parse IP address: $addr"
    }
    if ($ipAddr.AddressFamily -ne [Net.Sockets.AddressFamily]::InterNetwork) {
        throw "Can only process CIDR for IPv4"
    }

    $shiftCnt = 32 - $maskLen
    $mask = -bnot ((1 -shl $shiftCnt) - 1)
    $ipNum = [Net.IPAddress]::NetworkToHostOrder([BitConverter]::ToInt32($ipAddr.GetAddressBytes(), 0))
    $ipStart = ($ipNum -band $mask) + 1
    $ipEnd = ($ipNum -bor (-bnot $mask)) - 1

    # return as tuple of strings:
    ([BitConverter]::GetBytes([Net.IPAddress]::HostToNetworkOrder($ipStart)) | ForEach-Object { $_ } ) -join '.'
    ([BitConverter]::GetBytes([Net.IPAddress]::HostToNetworkOrder($ipEnd)) | ForEach-Object { $_ } ) -join '.'
}

function get-nextaddress {
    param ([string]$address )
   
    $a = [System.Net.IpAddress]::Parse($address) ## turn the string to IP address
    $z = $a.GetAddressBytes() ## and then to an array of bytes
    if ($z[3] -eq 255) ## last octet full
    {
     $z[3] = 0 ## so reset
   
     if ($z[2] -eq 255) ## third octet full
     {
      $z[2] = 0 ## so reset   
      $z[1] += 1 ## increment second octet
     }
     else
     {
      $z[2] += 1 ##  increment third octect
     }
    }
    else
    {
     $z[3] += 1 ## increment last octet
    }
    $c = [System.Net.IpAddress]($z) ## recreate IP address
    return $c.ToString()
   }

function get-iplist ($ipcidr) {
    $ip,$mask=$ipcidr.split("/")
    if ($mask -eq 32) {
        $ipList +=$ip
        return $ipList
    }
    

    $start,$end=cidrtoiprange $ipcidr
    $ipList=@()
    $currentIP=$start
    $ipList +=$start

    do {
    $nextIP=get-nextaddress($currentIP)
    if ($ipList -notcontains $nextIP) {$ipList +=$nextIP}
    $currentIP=$nextIP
    } until ($currentIP -eq $end)
    return $ipList
}




#if ($PSCmdLet.ParameterSetName -eq 'IPRange') {
#    #$IPList=new-object system.collections.ArrayList
#    foreach ($network in $IPSubnet) {
#        $hostList+=get-iplist($network)
#    }
#    #$hostList
#}


$tmpList=new-object System.Collections.ArrayList

foreach ($item in $hostList) {
    if ($item -match "/") {
        write-host "Getting list of IPs in $item"
        $ipList=get-iplist($item)

        foreach ($ipItem in $ipList) {
            #write-host "Adding $ipItem"
            [void] $tmpList.add($ipItem)
        }

    } else {
        #write-host "Adding $item"
        [void] $tmpList.add($item)
    }
}

$hostList=$tmpList


write-host "SSL Scanner"
write-host "Checking $($hostList.count) hosts for SSL Certificates on ports $($portList -join `", `")"
$reportList=new-object System.Collections.ArrayList


$pass = 0
foreach ($objHost in $hostList) {
    $hostUp=$false
    $pass++
    if ($pingCheck -eq $true) {
        $hostUp=test-connection -computername $objHost -count 1 -quiet
    }

    $DNSDone=$false

    foreach ($objPort in $portList) {
        $req=$null
        $objURI = "https://$objHost`:$objPort"
        #write-host "Checking $objURI"
        $tmpObject=""
        $tmpObject=[pscustomobject] @{
            HostIP=$objHost
            PortNumber=$objPort
            Status=""
            ExpirationDate=""
            CertIssuer=""
            certSubject=""
            ServerType=""
            ServerName=""
        }
        if (($pingCheck -eq $true -and $hostUp -eq $true) -or ($pingCheck -eq $false)) {
            write-host "Checking $objURI ($pass of $($hostlist.count))"
            $req = [net.httpwebrequest]::Create($objURI)
            $req.allowautoredirect=$false

        
            $req.timeout=1000
            try { [void] $req.getresponse() } catch { Write-host "" <# Nothing to do here #>}

            $tmpObject.ServerType=$req.ServerType 

            if ($req.servicepoint.certificate) {
                write-host "REQ Object:"
                $req|gm
                

                $tmpObject.ExpirationDate = $req.servicepoint.certificate.getexpirationdatestring()
                $tmpObject.CertSubject=$req.servicepoint.certificate.Subject
                $tmpObject.CertIssuer=$req.servicepoint.certificate.Issuer
                $tmpOBject.status="Valid"

                if ([datetime]::Now -gt $tmpObject.ExpirationDate) {
                    $tmpOBject.status="Expired"
                }
            }
            else {
                $tmpObject.Status = "Not_SSL"
            }
            
            if ($useDNS -eq $true -and $DNSDone -eq $false) {    
                $DNSDone=$true
                try { 
                    [int] $IP1=$objHost.split(".")[0] 
                } catch {
                    #Nothing to do there
                    $x=0
                }

                if ($IP1 -is [int]) {
                    $tmp=( resolve-dnsname $objHost -erroraction silentlycontinue )[0]
                    $tmpObject.ServerName=$tmp.namehost
                }
            }
            [void] $reportList.add($tmpObject)
            sleep -Milliseconds 30
        }

    }
}

if ($display -eq $true) {
    $reportList|Out-GridView
} 

if ($save -eq $true) {
    $myCSV=$reportList|convertto-csv -notypeinformation
    $myCSV|out-file -filepath $logFile
}


