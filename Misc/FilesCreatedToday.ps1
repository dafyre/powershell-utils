﻿Param(
[Parameter(Mandatory=$false) ] [string] $folder="$($env:home)"
)


$files = get-childitem $folder * -recurse

foreach ($file in $files) {

 if ($file.CreationTime -gt [datetime]::Today -or $file.LastWriteTime -gt [datetime]::today) {

  write-host -ForegroundColor Yellow "File created today: $($file.fullname)"

 }
}