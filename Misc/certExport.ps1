$exportFile="$PWD\2019Cert.pfx"
$CertListToExport = Get-ChildItem -Path "Cert:\LocalMachine\My"

$myCert=$CertListToExport|where {$_.FriendlyName -eq '2019Cert'}

$myPassword=read-host "Enter a secure Password, DO NOT FORGET THIS PASSWORD!" -assecurestring

export-pfxcertificate -cert $myCert -FilePath $exportFile  -password $myPassword 
