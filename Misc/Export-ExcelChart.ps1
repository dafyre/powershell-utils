function Export-ExcelChart {
    Param(
        [Parameter(Mandatory=$true)] $xlFile,
        [Parameter(Mandatory=$true)] $destFile,
        [Parameter(Mandatory=$false)] $sheetName="Sheet1"
    )

    try {$excelApp=new-object -ComObject "Excel.Application"} 
    catch {write-host "There was an error.  This usually means that excel is not installed."; exit -128}

    try {
        $workbook=$excelApp.Workbooks.open($xlFile)
    } catch {
        write-host "Unable to Open $xlFile!";
        write-host $error[0]; 
         #exit -1024
         read-host -Prompt "Hit the enter key to exit this app."
         break
         exit -1024; 
    }
    #>
    
    $worksheet=$workbook.worksheets($sheetName)
    $chartCount=0
    foreach ($chart in $worksheet.ChartObjects([System.Type]::Missing)) {
        $x=$chartCount++

        write-host "Processing Charts..."
        $excelApp.goto($chart.TopLeftCell,$true)
        $destFile="$destFile`_$($chartCount.tostring("000")).png"
        write-host "Exporting chart to $destFile"

        if ($chart.Chart.Export($destFile,"PNG",$false)) {
            write-host "Exported $chartName to $destFile."
        } else { write-host "Error saving chart..." }
        #Check on this
        #[System.Runtime.Interopservices.Marshal]::ReleaseComObject($activate) | Out-Null
        #[System.Runtime.Interopservices.Marshal]::ReleaseComObject($xlsRng) | Out-Null
        $excelApp.DisplayAlerts=$false
        $x=[System.Runtime.Interopservices.Marshal]::ReleaseComObject($chart)
        $x=[System.Runtime.Interopservices.Marshal]::ReleaseComObject($worksheet)
        $workbook.close($false,$null,$null)
        $x=[System.Runtime.Interopservices.Marshal]::ReleaseComObject($workbook)
        $excelApp.Quit()
        $x=[System.Runtime.Interopservices.Marshal]::ReleaseComObject($excelApp)
    
    }
}