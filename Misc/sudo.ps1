#Place this near the top of your script.  If you are not running as an admin already, it will relaunch the script (in a new PowerShell Window) 
#and prompt you for your admin credentials or give you the yes/no box.

write-host "Checking for admin privileges..."

$user=[Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()
$isAdmin=$user.isInRole([Security.Principal.WindowsBuiltInRole] "Administrator")

if ($isAdmin -eq $false) {
    #This is how to elevate permissions.
    write-host -foregroundcolor Red "Elevating Privileges..."
	#$startingPath=$PWD
    $CommandLine="-File `"" + $MyInvocation.MyCommand.Definition + "`""
	write-host $CommandLine
    Start-Process -FilePath Powershell.exe -Verb RunAs -ArgumentList $CommandLine

}