﻿#Free

$freemem=get-wmiobject -class Win32_OperatingSystem

 $objFree=[pscustomobject]@{
   totalMemory="$([math]::round($freemem.TotalVisibleMemorySize/1024,2)) MB"
   usedMemory = "$([math]::round(($freemem.TotalVisibleMemorySize - $freemem.FreePhysicalMemory)/1024,2)) MB"
   freeMemory="$([math]::round($freemem.FreePhysicalMemory/1024,2)) MB"
   #freeVirtMem ="$([math]::round($freemem.freevirtualmemory/1024,2)) MB"
   totalSwap = "$([math]::round($freemem.SizeStoredInPagingFiles/1024,2)) MB"
   usedSwap = "$([math]::round(($freemem.SizeStoredInPagingFiles - $freemem.FreeSpaceInPagingFiles)/1024,2)) MB"
   freeSwap = "$([math]::round($freemem.FreeSpaceInPagingFiles/1024,2)) MB"
   

}


 $objFree|ft -auto
