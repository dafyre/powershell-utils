<#
adInfo Employeelist Compare
#>

$adInfo=import-csv -path "detailedinfo.csv"
$employeeList=import-csv -path "employeelist.csv"
$isEmployee=$false
$pass=0

$comparison=new-object system.collections.arrayList
foreach ($user in $adInfo) {
    $pass++
    $userName=$user.userprincipalName.split("@")[0]
    $thirdPartyID=$userName.replace("_",".")

    #write-host "Checking $userName"
    write-progress "Checking $userName" -PercentComplete (($pass/$adInfo.count)*100)

    $employee=$employeeList|where {$_.thirdPartyID -eq $thirdPartyID}

    if ([string]::isnullorempty($employee)) {
        #write-host write-host -ForegroundColor Yellow  "$userName was not found in Banner."
        $isEmployee=$false
    } else {
        $isEmployee=$true
        #write-host "$userName was found in Banner."
    }

    
    if (!$isEmployee) {
        $comparisonObj=[pscustomObject]@{
            userName=$user.username
            displayName=$user.displayname
            UserPrincipalName=$user.userprincipalName
            Created=$user.created
            AccountExpirationDate=$user.AccountExpirationDate
            Department=$user.Department
            EmployeeID=$user.EmployeeID
            EmployeeNumber=$user.EmployeeNumber
            Enabled=$user.Enabled
            LastLogonDate=$user.LastLogonDate
            LockedOut=$user.LockedOut
            Modified=$user.Modified
            OfficePhone=$user.OfficePhone
            PasswordLastSet=$user.PasswordLastSet
            PasswordNeverExpires=$user.PasswordNeverExpires
            Description=$user.Description
            BannerStatus="NIB"
            TermDate=""
        }
     }
     
     if ($isEmployee -eq $true) {
         $banStatus=""
         $banSet=$false

         if ($user.Description -like "*RETIRE*" -and $banSet -eq $false) { 
             $banStatus="RETIRED" 
             $banSet=$true
         }

         if ((![string]::isnullorempty($employee.TerminationDate) -or ![string]::isnullorempty($user.AccountExpirationDate)) -and $banSet -eq $false) {
            $banStatus="TERM"
            $banSet=$true
        }
        
         if ($employee.Fulltimestatus -eq "PT" -or $employee.Fulltimestatus -eq "FT" -and $banSet -eq $false) {
             $banStatus="EMPLOYED"
             $banSet=$true
         }


         if ($banSet=$false) {$banStatus="OTHER"}

        $comparisonObj=[pscustomObject]@{
            userName=$user.username
            displayName=$user.displayname
            UserPrincipalName=$user.userprincipalName
            Created=$user.created
            AccountExpirationDate=$user.AccountExpirationDate
            Department=$user.Department
            EmployeeID=$user.EmployeeID
            EmployeeNumber=$user.EmployeeNumber
            Enabled=$user.Enabled
            LastLogonDate=$user.LastLogonDate
            LockedOut=$user.LockedOut
            Modified=$user.Modified
            OfficePhone=$user.OfficePhone
            PasswordLastSet=$user.PasswordLastSet
            PasswordNeverExpires=$user.PasswordNeverExpires
            Description=$user.Description
            BannerStatus=$banStatus
            TermDate=$employee.TerminationDate
        }
     }
     $x=$comparison.add($comparisonObj)
     #if ($pass -gt 10) { break }
}

#$comparison|Out-GridView

$comparison|convertto-CSV -NoTypeInformation|out-file -filePath "ComparisonDetails.csv"



