param (
[string] $command, 
[int] $interval = 10
)

do { 
clear
$now=[datetime]::Now
 
write-host "Running $command with a $interval second interval.  $now"
invoke-expression $command

# Just to let the user know what is happening
#"Waiting for $interval seconds. Press Ctrl-C to stop execution..."
write-host "Running $command with a $interval second interval.  $now"
sleep $interval 
} 
# loop FOREVER
while (1 -eq 1)

