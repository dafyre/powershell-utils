<#

Exports a list of snapshots to a CSV file for use with Import-SnapshotTask


.Description

Date        Updated By  Details
12/19/2019      BW      changed foreach/where/select to use foreach-object/where-object/select-object
11/19/2019      BW      Initial Coding



.EXAMPLE
To export all snapshot scheduled taskss for ALL VMsto "AllSnapshots.csv" :

Note: On a large vCenter deployment this can take a long time.

./export-SnapshotTask -outputFile "AllSnapshots.csv"

To export all snapshot scheduled tasks for a list of VMs: DC1,DC_TEST, and DNS_TEST to TestSnaps.csv

./export-SnapshotTask -outputfile ("DC1","DC_TEST","DNS_TEST") -outputfile "TestSnaps.csv"

If you leave off the -outputFile, any snapshots that would have been exported are displayed in a gridview.

#>

Param(
    [Parameter(Mandatory = $false) ] [string[]] $vmList = "",
    [Parameter(Mandatory = $false) ] [string] $outputFile=""
)

$hasVI = get-command connect-viserver* -ErrorAction SilentlyContinue

if ([string]::IsNullOrEmpty($hasVI) -eq $true) {

    write-host -foregroundcolor "Yellow" "You MUST have the VMware PowerCLI Modules installed for this to work."
    write-host -foregroundcolor "yellow" "Open PowerShell as an administrator and then "
    write-host " "
    write-host -foregroundcolor "white" "install-module vmware.powercli"
    write-host " "
    write-host -foregroundcolor "Yellow" "And then try this script again."

    exit -2048;
}


$weekdayList=new-object system.collections.arraylist
[void] $weekDayList.add("Sunday")
[void] $weekDayList.add("Monday")
[void] $weekDayList.add("Tuesday")
[void] $weekDayList.add("Wednesday")
[void] $weekDayList.add("Thursday")
[void] $weekDayList.add("Friday")
[void] $weekDayList.add("Saturday")

write-host "Loading Scheduled Tasks from vCenter..."
$taskList=(get-view ScheduledTaskManager).ScheduledTask | foreach-object { (Get-View $_).info | select-object * } | where-object { $_.Action.Name -eq 'CreateSnapshot_Task' } | Select-object *

$exportList=new-object System.Collections.ArrayList
$vmList=$vmList.tolower()

foreach ($snapTask in $taskList) {
    $TaskName=$snapTask.Name
    $TaskDescription=$snaptask.Description
    $TaskType=($snapTask.scheduler.gettype()).Name
    $VMName=(get-view $snapTask.Entity).Name
    $objTask=[pscustomobject] @{
        VMName=$VMName
        TaskName=$TaskName
        TaskDescription=$TaskDescription
        TaskType=$TaskType
        TaskTime=""
        Weekday=0
        DayOfMonth=0
        DayOffset=""
        wsWeekday=""
        Interval=0
          
    }

        #DONE: Convert TaskTime to Local Time
        $startTZ=[system.timezoneinfo]::utc
        $localTZ=[system.timezoneinfo]::Local
        $tmpDate=get-date -date $snapTask.scheduler.activetime
        $tmpDate=[System.TimeZoneInfo]::ConvertTimeBySystemTimezoneId($tmpDate,$startTZ.id,$localTZ.id)

        if ($snaptask.scheduler.hour -lt 5) { $tmpDate=$tmpDate.adddays(1) }
        $taskTime=get-date -date $tmpDate -format "hh:mm tt"

        $objTask.tasktime=$taskTime
        $objTask.Interval=$snapTask.scheduler.Interval

    switch ($TaskType) {
        'DailyTaskScheduler' {
            #Nothing Special to do for Daily
        }

        'MonthlyByDayTaskScheduler' {
            $objTask.DayofMonth=$snapTask.scheduler.Day

        }

        'MonthlyByWeekdayTaskScheduler' {
            $objTask.DayOffSet=$snaptask.scheduler.offset
            $objTask.wsweekday=(Get-Culture).TextInfo.ToTitleCase($snaptask.scheduler.weekday)
            $objTask.weekday=$weekdayList.indexof($objTask.wsWeekday)

        }

        'WeeklyTaskScheduler' {
            #Handles Weekly / x Interval based on weekly
            $tmpSchedule=$snapTask.scheduler
            if ($tmpSchedule.Sunday -eq $true ) {$objTask.weekday=0}
            if ($tmpSchedule.Monday -eq $true) {$objTask.weekday=1}
            if ($tmpSchedule.Tuesday -eq $true) {$objTask.weekday=2}
            if ($tmpSchedule.Wednesday -eq $true) {$objTask.weekday=3}
            if ($tmpSchedule.Thursday -eq $true) {$objTask.weekday=4}
            if ($tmpSchedule.Friday -eq $true) {$objTask.weekday=5}
            if ($tmpSchedule.Saturday -eq $true) {$objTask.weekday=6}
            $objTask.wsWeekday=$weekdaylist[$objTask.weekday]

        }
    }
    if ([string]::isnullorempty($vmList) -eq $true) {
        [void] $exportList.add($objTask)
    } else { 
        #write-host "$($objTask.vmname) - $($vmList.indexof($objTask.vmName))"
        if ($vmList.indexof($objTask.vmName.tolower()) -ne -1) { 
            [void] $exportList.add($objTask) 
        } 
    }
}



if ([string]::IsNullOrEmpty($outputFile) -eq $false) { 
    write-host "Saving $($exportList.count) records to $outputFile"
    $exportList | export-csv -notypeinformation -path $outputFile 
} else {
    write-host "Displaying $($exportList.count) records."
    $titleMsg="Snapshot Schedules"
    if ([string]::IsNullOrEmpty($vmList) -eq $false) { $titleMsg="$titleMsg for $($vmList)" }
    $exportList |out-gridview -title $titleMsg
}

