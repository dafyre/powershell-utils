
<#
#Get List of Events from vCenter ranging from date range and up to -maxsamples number of records.
PS C:\Users\bwells> get-vmhost|where {$_.Name -match "wkr-26"
>> }|get-vievent -start "11-13-2019 07:00" -maxsamples 10000 -finish "11-13-2019 10:30"|Select ObjectName,Severity,CreatedTime,FullFormattedMessage|out-gridview2
#>

write-host "Loading ESXi Host List..."
$hostDetail = get-vmhost | get-view

$details=new-object system.collections.arraylist

foreach ($myHost in $hostDetail) {
  $dc=get-datacenter -vmhost $myHost.name
  $cluster=get-cluster -vmHost $myHost.name
  $hostName=$myHost.Name
  $biosVersion=$myhost.hardware.biosinfo.biosversion
  $vendor=$myHost.hardware.systeminfo.vendor
  $modelNo=$myHost.hardware.systeminfo.model
  $data=[pscustomobject] @{
      Name = $hostName
      DataCenter=$dc
      Cluster=$cluster
      BiosVersion=$biosVersion
      Vendor=$vendor
      Model=$modelNo
  }
  #$data="$($myHost.Name) - $dc - $biosVersion"
  [void] $details.add($data)
}

$details|out-gridview