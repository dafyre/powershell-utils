<#

Imports a CSV file of Snapshot Schedules created by export-SnapshotTask.

.Description

Date        Updated By  Details
11/21/2019      BW      Initial Coding.


See posts from LucD at https://communities.vmware.com/thread/541573 for more details.

.EXAMPLE

To import snapshots from the file "AllSnapshots.csv":

./Import-SnapshotTask -importfile "AllSnapshots.csv"

This will recreate all the snapshots that are stored in "AllSnapshots.csv"



#>

Param(
    [Parameter(Mandatory = $true) ] [string] $importFile = ""
)

$hasVI = get-command connect-viserver* -ErrorAction SilentlyContinue

if ([string]::IsNullOrEmpty($hasVI) -eq $true) {

    write-host -foregroundcolor "Yellow" "You MUST have the VMware PowerShell Modules installed for this to work."
    write-host -foregroundcolor "yellow" "Open PowerShell as an administrator and then "
    write-host " "
    write-host -foregroundcolor "white" "install-module vmware.powercli"
    write-host " "
    write-host -foregroundcolor "Yellow" "And then try this script again."

    exit -2048;
}

$weekdayList=new-object system.collections.arraylist
[void] $weekDayList.add("Sunday")
[void] $weekDayList.add("Monday")
[void] $weekDayList.add("Tuesday")
[void] $weekDayList.add("Wednesday")
[void] $weekDayList.add("Thursday")
[void] $weekDayList.add("Friday")
[void] $weekDayList.add("Saturday")

$taskList=import-csv -path $importFile

foreach ($task in $taskList) {
    $VMName=$task.vmName
    $taskName=$task.TaskName
    $taskDescription=$task.taskdescription
    $taskType=$task.tasktype
    $taskTime=$task.tasktime
    [int] $weekday=$task.weekday
    $taskDayofMonth=$task.DayOfMonth
    $taskDayOffSet=$task.DayOffSet
    $wsWeekday=$task.wsWeekday
    $taskInterval=$task.Interval


    [string] $today = get-date -format "MM-dd-yyyy"
    $time = get-date -format "hh:mm tt"  -date "$taskTime"
    [datetime] $today = get-date -format "MM-dd-yyyy hh:mm tt" -date "$today $time"

    $finalTZ="UTC"
    $localTZ=[system.timezoneinfo]::Local
    $runDate=[System.TimezoneInfo]::ConvertTimeBySystemTimezoneId($today,$localTZ.id,$finalTZ)   

    $spec=new-object vmware.vim.scheduledtaskspec
    $spec.Name=$TaskName
    $spec.Description=$TaskDescription
    write-host "$vmName - $TaskType"

    switch ($TaskType) {
        'DailyTaskScheduler' {
            $spec.scheduler = new-object vmware.vim.dailytaskscheduler
            $spec.scheduler.hour = $runDate.hour
            $spec.scheduler.minute=$ruNDate.minute
            $spec.scheduler.interval=$taskInterval
            $spec.scheduler.activetime=$runDate
        }

        'MonthlyByDayTaskScheduler' {
            if ($runDate.day -ne $taskDayOfmonth) { 
                do {
                    $runDate=$runDate.adddays(1)
                } until ($runDate.Day -eq $taskDayOfMonth)
            }

            $spec.scheduler=new-object vmware.vim.MonthlyByDayTaskScheduler
            $spec.scheduler.hour=$rundate.hour
            $spec.scheduler.minute=$rundate.minute
            $spec.scheduler.day=$taskDayofMonth
            $spec.scheduler.activetime = $runDate
            $spec.scheduler.interval=$taskInterval

        }

        'MonthlyByWeekdayTaskScheduler' {
            $spec.scheduler=new-object vmware.vim.MonthlyByWeekdayTaskScheduler
            $spec.scheduler.hour=$runDate.hour
            $spec.scheduler.minute=$runDate.minute
            $spec.scheduler.offset=$taskDayOffset.tolower()
            $spec.scheduler.Weekday=$wsWeekday.tolower()
            $spec.scheduler.activetime=$runDate
            $spec.scheduler.interval=$taskInterval

        }

        'WeeklyTaskScheduler' {
            $wsWeekday=$weekdayList[$weekday]
            write-host $VMName
            write-host "Weekday is $wsWeekday, Weekday Number = $weekday"
            if ($runDate.dayofweek -ne $wsWeekday) {
                do {
                    [datetime] $runDate=$runDate.adddays(1)
                    #write-host "$wsWeekday - $($runDate.dayofweek) $($runDate.dayofweek -eq $wsWeekday)"
                } until($runDate.dayofweek -eq $wsWeekday)
            }

            if ($runDate.hour -lt 5) {
                $rundate=$rundate.adddays(1)
                $weekday++
                if ($weekday -gt 6) {$weekday = 0}
                $wsWeekday=$weekdayList[$weekday]
            }

            $spec.scheduler = new-object VMware.Vim.WeeklyTaskScheduler
            $spec.scheduler.hour=$runDate.hour
            $spec.scheduler.minute=$rundate.minute
            $spec.scheduler.interval=$taskInterval
            $spec.scheduler.activetime = $runDate
            $spec.scheduler.interval = $taskInterval

            write-host "WS Weekday $wsWeekday, Weekday num $weekday"            
            switch($weekday) {
                0 { $spec.scheduler.Sunday=$true }
                1 { $spec.scheduler.Monday =$true }
                2 { $spec.scheduler.Tuesday=$true }
                3 { $spec.scheduler.Wednesday=$true }
                4 { $spec.scheduler.Thursday=$true }
                5 { $spec.scheduler.Friday=$true }
                6 { $spec.scheduler.Saturday=$true }
            }

            #$spec.scheduler
            #read-host
            
        }
    }
    
    $vmRef = (get-view -viewtype virtualmachine -property Name -Filter @{"Name" = "^$($vmName)$" }).MoRef
    ($spec.action = New-Object VMware.Vim.MethodAction).Name = "CreateSnapshot_Task"
    $spec.action.argument = New-Object VMware.Vim.MethodActionArgument[] (4)
    ($spec.action.argument[0] = New-Object VMware.Vim.MethodActionArgument).Value = $spec.name
    ($spec.action.argument[1] = New-Object VMware.Vim.MethodActionArgument).Value = $spec.description
    ($spec.action.argument[2] = New-Object VMware.Vim.MethodActionArgument).Value = $false # Snapshot memory
    ($spec.action.argument[3] = New-Object VMware.Vim.MethodActionArgument).Value = $false # quiesce guest file system (requires VMware Tools)

    [void] (Get-View -Id 'ScheduledTaskManager-ScheduledTaskManager').CreateScheduledTask($vmRef, $spec)    

}   