Param(
    [Parameter(Mandatory = $false)][string] $vmHost="",
    [Parameter(Mandatory = $false)][switch] $Report

)

$myVI=$global:defaultviservers

if ($myVI.count -lt 1) {
    write-host -foregroundcolor Yellow "You are not connected to any VMware Servers!"
    exit 0
}

if ([string]::isnullorempty($vmHost)) {
    write-host "Loading all hosts from Connected VMware Servers..."
    $myHosts=get-VMHost
}

if ([string]::isnullorempty($vmHost) -eq $false) {
    $myHosts=get-vmhost $vmHost
}

$serverDetail=new-object system.collections.arraylist


foreach ($server in $myHosts) {
    if ($report -eq $true) { write-host "Processing $($server.Name)..." }
    #$serverNICS=""
    $serverVMK=""

    $tmpDetail=[pscustomobject] @{
        ServerName=$server.Name
        ServerStatus=$server.ConnectionState
        Manufacturer=$server.Manufacturer
        Model=$server.model
        CPUType=$server.ProcessorType
        MaxEVCMode=$server.Maxevcmode
        VmwareVersion=$server.version
        VmwareBuild=$server.build
        ManagementInfo=new-object system.collections.arraylist
        PhysicalAdapters=new-object system.collections.arraylist
        VMkernelAdapters=""
        VSwitchInfo=new-object system.collections.arraylist
        DataStores = ""
        ISCSITargets = new-object system.collections.arraylist
        IQN=""
        NTPServers=""

    }
    
    $serverView=get-view $server.id
    $serverNetInfo=get-view $serverView.ConfigManager.NetworkSystem
    
    foreach ($pnic in $serverNetInfo.networkinfo.pnic) {
        $tmpNic=[pscustomobject]@{
            Name=""
            Mac=""
            ConnectedSwitchPort=""
        }
        $pInfo=$serverNetinfo.querynetworkhint($pnic.Device)
                
        $tmpNic.Name=$pnic.Device
        $tmpNic.Mac=$pnic.Mac
        $tmpNic.ConnectedSwitchPort="Switch: $($pinfo.connectedswitchport.devid), IP: $($pinfo.connectedswitchport.MGMTAddr), Port: $($pinfo.connectedswitchport.portid)"
        
        [void] $tmpDetail.PhysicalAdapters.add($tmpNic)
    }


    $serverNICS=$server|get-vmhostnetworkadapter -physical|select *
    #$tmpDetail.PhysicalAdapters=$serverNICS|Select Name,Mac,ConnectedSwitchPort
    $tmpDetail.datastores = $server | get-datastore | Select Name, FreeSpaceGB, CapacityGB
    $serverVMK=$server|get-vmhostNetworkAdapter -vmkernel|Select *
    $tmpDetail.VMKernelAdapters=$serverVMK

    foreach ($vmk in $serverVMK) {
        $tmpMan=[pscustomobject] @{
            PortGroupName=$vmk.PortGroupName
            IPAddress=$vmk.IP
            VMKName=$vmk.name
            VMKMac=$vmk.mac
        }
        if ($vmk.managementtrafficenabled) {
            [void] $tmpDetail.ManagementInfo.add($tmpMan)
        }
    }

    $vslist=""
    $vslist=$server|get-virtualswitch|select *

    foreach ($vswitch in $vslist) {
        $tmpSwitch=[pscustomobject] @{
            Name=""
            Nic=""
            PortGroups=new-object system.collections.arraylist
        }


        
        $tmpSwitch.nic=$vswitch.nic -join ", "
        $tmpSwitch.name=$vSwitch.name

        $pgList=$server|get-virtualportgroup|select *|where {$_.virtualswitch.name -eq $vswitch.name}

        foreach ($pg in $pgList) {
            $tmpPG=[pscustomobject] @{
                pgName=""
                pgVlan=""
                pgSwitch=""
            }

            $tmpPG.pgName=$pg.Name
            $tmpPG.pgVlan=$pg.VlanID
            $tmpPG.pgSwitch=$pg.VirtualSwitchname

            [void] $tmpSwitch.portgroups.add($tmpPG)
        }

        [void] $tmpDetail.vSwitchInfo.add($tmpSwitch)
    }

    $vStorage = $server.ExtensionData.Config.StorageDevice.HostBusAdapter | where {$_.ConfiguredSendTarget -ne $null}
    $tmpDetail.IQN=$vStorage.IscsiName
    foreach ($target in $vStorage.ConfiguredSendTarget) {
        [void] $tmpDetail.ISCSITargets.add($target.Address)
    }

    $tmPDetail.NTPServers = $server.extensiondata.Config.DateTimeInfo.ntpconfig.Server

   [void] $serverdetail.add($tmpDetail)

}

if ($Report -eq $false) { $serverDetail } 

if ($Report -eq $true) {

        foreach ($item in $serverdetail) {
        $reportdata=""
        # NTP Servers
        $ntpservers=$item.ntpservers -join ", "
        
        #Datastores
        $dsList=""
        foreach ($store in $item.datastores) {
            #write-host "Checking $($store.name)"
                #$dsList = "$dsList $($store.name)`t`t`t$($store.capacitygb)`n"
                $dsList = "$dsList $($store.name)`n"
        }


        #Virtual Switch Information
        $vsList=""
        foreach ($vswitch in $item.vswitchinfo) {
            $vsList=$vsList + "vSwitch Name: $($vswitch.name)`n`tNetwork Interfaces: $($vSwitch.nic -join ",")`n"
            $vsList = $vsList + "`tPort Groups `n`t`tName, VLAN, Switch`n"
            foreach ($vsPG in $vswitch.portgroups) {
                $vsList = $vsList + "`t`t$($vspg.pgName), $($vspg.pgVlan), $($vspg.pgSwitch)`n"
            }
            $vsList=$vsList+"`n"

        }

        #Management Info
        $manInfo=""
        $maninfo = "Port Group Name, IP Address, VMKName`n"
        foreach ($minfo in $item.managementinfo) {
            $maninfo=$maninfo+"$($minfo.portgroupname), $($minfo.ipaddress), $($minfo.vmkName)`n"

        }

        #VMK Info
        $vmkInfo=""
        $vmkinfo="VMK Name, IP, PortGroupName`n"
        foreach ($vmk in $item.vmkerneladapters) {
            $vmkinfo=$vmkinfo+"$($vmk.Name), $($vmk.ip), $($vmk.portgroupname)`n"
        }

        #Physical NIC Info
        $phyNicInfo=""
        $phyNicInfo="Name, MAC Address, SwitchPort`n"
        foreach ($pnic in $item.physicaladapters) {
            $phyNicInfo=$phyNicInfo+"$($pnic.name), $($pnic.mac), $($pnic.connectedswitchport)`n"
        }

$reportdata = @"
$($item.servername) VMware $($item.vmwareversion) Documentation
Management Information:
$maninfo

NTP Servers: $ntpservers
ISCSI IQN: $($item.iqn)
ISCSI Targets: $($item.ISCSITargets -join ", ")
Port: 3260

DataStores: 
$dsList



Virtual Switches

$vsList

VMKERNEL Adapters:

$vmkInfo

Physical Adapters:
$phyNicInfo

"@

        $outFile=$item.servername
        $outFile="$outfile`_report.txt"
        write-host saving data to "$outFile"
        $reportdata|out-file -filepath "$outfile"

        } #End foreach $item in $serverdetail
}
