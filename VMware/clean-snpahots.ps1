Param(
    [Parameter(Mandatory = $false) ] [int] $maxJobs=3,
    [Parameter(Mandatory = $false) ] [bool] $showDebug=$false,
    [Parameter(Mandatory = $false) ] [bool] $showServers=$false,
    [Parameter(Mandatory = $false) ] [int] $taskUpdate=15,
    [Parameter(Mandatory = $true) ] [string] $vmListFile,
    [Parameter(Mandatory = $true) ] [string] $vcCredFile,
    [Parameter(Mandatory = $false) ] [string] $vcCredKey="$vcCredFile.key",
    [Parameter(Mandatory = $false) ] [string[]] $viServers,
    [Parameter(Mandatory = $false) ] [switch] $generateKeyFile
)

<#

Date        Updated By  Details
10-12-2023      BW      cleanSnapshot now works well enough for general release
9-26-2023       BW      Initial Internal release
09-26-2023      BW      Misc adjustments to highlight when a duplicate is found
09-22-2023      BW      Added Master Timer, Individual Server Timer, and a Duplicate checker.
09-22-2023      BW      Added 500ms delay after remove-snapshot is called to give it time to process in vCenter.
09-22-2023      BW      Initial coding complete, first successful test run.


.DESCRIPTION

VMLISTFILE FORMAT:
CSV FILE FORMAT [optional field]
#Lines that start with # are ignored
VMName,KeepType,KeepInterval[,CleanupMatch]
WWW_SERVER,WEEKLY,6
#The line below will perform a clean up on DUMM_VM_2022, bu tonly for snpashots matching _VM
DUMM_VM_2022,WEEKLY,3,_VM
#ANOTHER_DUMM_COMPUTER,WEEKLY,3

.EXAMPLE

Generate a credential file & key named Myfile.cred and myfile.key 
!!! CAUTION !!! This will overwrite existing files!

clean-snapshots -generatekeyfile -vccredfile Myfile.cred

Parse though mylistofvmstoclean.txt and cleaning snapshots that match in the "CleanupMatch" column of the file.
If no cleanup match is given, then we use the KeepType Column (DAILY,WEEKLY,MONTHLY) as the CleanupMatch

./cleansnapshots -vccredfile myfile.cred -vmlistfile mylistofvmstoclean.txt


#>

if ($generateKeyFile -eq $true) {
    write-host -foregroundcolor Green "Generating VMware Credential File"

    
    write-host -foregroundcolor Yellow "Please enter your VCenter Admin Credentials..."
    $username = read-host "Username: "
    $password = read-host "Password: " -AsSecureString
    $VCCred = New-Object System.Management.Automation.PSCredential -ArgumentList $username, $password

        #Create a 32Byte key
    $keyData=New-Object Byte[] 32

    [Security.Cryptography.RNGCryptoServiceProvider]::Create().GetBytes($keyData)

    set-content -path $vcCredKey $keyData

    $myPassSecure=$VCCred.password

    $myPass=$myPassSecure | convertfrom-securestring -key $keyData


    set-content $vccredFile $VCCred.UserName
    add-content $vccredFile $myPass

    write-host "Credential Stored in $vccredFile"
    write-host "Key Stored in $vcCredkey"
    

    write-host -foregroundcolor cyan "Key files generated.  run the command dwith the -vccredfile parameter to use the new files."
    exit 0

}

### SETTINGS HERE ###

if ([string]::IsNullOrEmpty($vmListFile)) {
    write-host -foregroundcolor yellow "Missing Parameter: -vmlistfile "
    $vmListFile=read-host -p "Enter the name of the list file:"
    if ([string]::IsNullOrEmpty($vmListFile)) {
        write-host -foregroundcolor red "No VM list given.  Aborting!"
        exit -1024
    }
}

if ([string]::IsNullOrEmpty($vcCredFile)) {
    write-host -foregroundcolor yellow "Missing Parameter: -vcCredFile "
    $vcCredFile=read-host -p "Enter the name of the credential file:"
    if ([string]::IsNullOrEmpty($vcCredFile)) {
        write-host -foregroundcolor red "No Credential file given.  Aborting!"
        exit -1025
    }
    $vcCredKey="$vcCredFile.key"
}


[int] $activeCount=0
[int] $lastProgress=0
#[int] $maxJobs=3
[int] $serverCount=0
[int] $serversDone=0
[bool] $showDebug=$false
[bool] $showServer=$false
[int] $taskUpdate=3 #how often to refresh the task, in seconds


$jobList=new-object System.Collections.ArrayList
$masterTimer = new-object System.Diagnostics.Stopwatch

#$viServers="vc8.gcsu.local","vc-app1.gcsu.local","tc-app.gcsu.local"

### END SETTINGS HERE ###

function readCred($keyFile,$credFile) {

    try {
        $keyData = get-content $keyFile -ErrorAction SilentlyContinue
        $credData=get-content $credFile -ErrorAction SilentlyContinue
    } catch {
        return $false
    }

    $username=$credData[0]
    $password = $credData[1] | convertto-securestring -key $keyData

    $objCred=new-object System.Management.automation.pscredential -ArgumentList $username,$password

    return $objCred
}


function textWithClear($message) {
    $esc=$([char] 27)
    $escend="$esc[0m"
    $up1Line="$esc[1A"
    $clrtoend="$esc[K"
    $retval=$up1Line+$message+$clrtoend
    
    return $retval
}

### BEGIN PROGRAM ###

$origWarning=$WarningPreference
$WarningPreference="SilentlyContinue"

#write-host "Warning Preference: $WarningPreference"
$masterTimer.start()

try {
    $objCred=readCred -keyfile $vcCredKey -credFile $vcCredFile
} catch {$objCred=$false}

if ($objCred -eq $null -or $objCred -eq $false ) {
    write-host -foregroundcolor red "Invalid Credentials!"
    exit -1031

}

try {
    $vmList=import-csv $vmlistfile
} catch {
    write-host -foregroundcolor Red "Error loading $vmlistfile"
    exit -1032
}


connect-viserver -credential $objCred $viServers

$loadCount=2 #leaving room for a primary progress bar

foreach ($vm in $vmList) {
    $firstChar=$vm.vmname.tochararray()[0]
    $vmName=$vm.VMName
    $vmKeepType=$vm.KeepType
    [int] $vmKeepinterval=$vm.KeepInterval
    $vmCleanupMatch=$vm.cleanupmatch

    if ($liveRun -eq $true) { $vmLiveRun=$true }

    switch ($vmKeepType){
        'DAILY' { $vmKeepDate=(get-date).adddays(-1 * $vmKeepinterval) }
        'WEEKLY' { $vmKeepDate=(get-date).adddays(-1 * 7 * $vmKeepinterval) }
        'MONTHLY' { $vmKeepDate=(get-date).adddays(-1 * 30 * $vmKeepinterval) }
        default { write-host -foregroundcolor red 'There was an invalid keep type or date specified!'; exit -1024 }
    }

    if ([string]::IsNullOrEmpty($vm.cleanupmatch) -eq $true) {$vmCleanupMatch=$vmKeepType
    }
    
    $tmpObject=@{
        VMName=$vmName
        KeepType=$vmKeepType
        KeepDate=$vmKeepDate
        Interval=$vmKeepinterval
        Search=$vmCleanupMatch
        Live=$vmLiveRun
        SnapList=""
        snapPass=0
        snapCount=0
        Progress=0
        progID=0
        VMTask=$null
        VMTaskID=$null
        Timer=new-object System.Diagnostics.Stopwatch
        Status=""
    }

    if ($firstChar -ne "#") {
        write-host $(textWitHClear("Loading Details for $vmname ..."))
        $loadCount++
        $tmpObject.progID=$loadCount
        $hasMatch=$false

        
        foreach ($tJob in $joblist) {
            #if ($tmpObject.vmname -eq "SIEMENS_2020") {
            #    write-host "`n $($tJob.vmname) - $($tmpObject.vmname)"
            #}

            if ($tjob.vmname -eq $tmpObject.vmname -and $tjob.keeptype -eq $tmpObject.keeptype -and $tjob.Search -eq $tmpObject.Search) {
                write-host -foregroundcolor "Yellow" "`n $($tmpObject.vmname): Duplicate detected!"
                $hasMatch=$true
                #read-host "Duplicate!"
                break
            }
        }
        
        if ($tmpObject.SnapList.count -gt 0 -and $hasMatch -eq $false) { 
            $null=$joblist.add($tmpObject) 
        }

    } else {
        Write-host -ForegroundColor Cyan "`nSkipping $vmName`n"
    }
}

$runningsnaps=$null
$activeCount=0
$serverCount=$joblist.count

$pass=0

write-host "Loading Running Tasks..."

$runningSnaps=get-task -Status 'Running'| where {$_.name -eq 'RemoveSnapshot_Task'}

#only counts objects in the job list against the active count.
$activeCount = ($runningSnaps|where {$_.extensiondata.info.entityName -match $joblist.vmname -and $_State -eq 'Running'}).count
$keepGoing=$true
$pass=0
while ($keepGoing -eq $true) {
    $pass++
    $ajText="Active Job Status: $activeCount / $maxJobs, $($jobList.count) Servers Remaining."
    $ajMessage=textWithClear($ajText)
    $totalProgress=(($serverCount-$joblist.count)/$serverCount) * 100
    write-progress -ID 1 -Activity "Checking Servers" -Status $ajText -percentcomplete $totalProgress

    #write-host $ajMessage

    foreach ($server in $joblist) {
        if ([console]::KeyAvailable) {
            $x=[System.Console]::ReadKey()
            if ($x.key -eq "q" -or $x.key -eq "Q") {
                write-host "Pressed Q, Stopping Script."
                exit -16384
            }
    
            if ($x.key -eq "d" -or $x.key -eq "D") {
                $showDebug=!$showDebug
                write-host "`nDebug State is now $showDebug"
            }
    
            if ($x.key -eq "s" -or $x.key -eq "S") {
                $showServer=!$showServer
                write-host "`nServer State is now $showDebug"
            }

            if ($x.key -eq "t" -or $x.key -eq "T") {
                write-host -foregroundcolor Yellow "`nUpdate Timer"
                [int] $taskUpdate=read-host -p "New Timer Value (in seconds)"
                if (($taskUpdate -is [int]) -eq $false) {$taskUpdate=15}
                write-host -ForegroundColor Yellow "New Timer: $taskUpdate"
            }
        }

        if ($showServer -eq $true) {
            if ($server.status -eq 'ACTIVE') {
                write-host "Pass: $pass) VM Name: $($server.vmname), Snap Count: $($server.snapCount), Task Count: $($server.vmtask.count), Task ID: $($server.vmtask.id), Status: $($server.status)"
                start-sleep -milliseconds 500
            }
        }

        
        if ($activeCount -lt $maxJobs) {
            if ($server.status -ne 'ACTIVE') { 
                $activeCount++ 
                $server.status='ACTIVE'
            }
            $server.timer.start()

            #We don't have enough active jobs!  Let's set one up,but only if it isn't busy already!!
            if ($server.status -eq $null) {
                $server.SnapList=get-vm $vmName | get-snapshot | where {$_.Name -match $tmpObject.search -and $_.Created -lt $tmpObject.KeepDate}
                $server.snapcount=$tmpObject.snaplist.count
                write-host "$($server.vmname) marked active."

            }
        }

        #The server may be doing something, let's check it
        if ($server.status -eq "ACTIVE") {
            #It's been a few seconds, let's go check it again
            if ($server.Timer.Elapsed.seconds -eq 0) {$server.Timer.start()}
            if ($server.Timer.elapsed.seconds -ge $taskUpdate) {
                #The server is marked as active, has no tasks, but also still has snapshots to clean
                #Let's go check and see if there are any running
                $server.snaplist=$server.SnapList=get-vm $server.vmName | get-snapshot | where {$_.Name -match $server.search -and $_.Created -lt $server.KeepDate} #Refresh the snapshot list
                $server.vmtask=$server.vmTask=get-task -Status 'Running'| where {$_.extensiondata.info.entityName -match $server.vmname} #Refresh the Task List

                #Oh, look, we have a task!  Let's track it!
                if ($server.vmtask.count -gt 0) {
                    $server.vmtask=$server.vmtask[0]
                    $server.vmtaskID=$server.VMTask.Id
                    $server.snapcount=$server.SnapList.Count
                } else {
                    #But what if we actually don't have tasks?  Let's set the task and task id to null.
                    $server.vmtask=$null
                    $server.vmtaskid=$null
                    $server.snapCount=0
                }

                if ($server.vmtaskID -ne $null -and $server.VMTask -ne $null) {
                    if ($server.SnapList.count -gt 0) {
                        $server.activity="Cleaning snapshot $($server.snappass) of $($server.snapcount)"
                        $server.snapcount++
                        write-progress -id $server.progID -Activity $server.VMName -Status $server.activity -PercentComplete $server.VMTask.PercentComplete
                    }
                }


                #We have no tasks, but DO have snapshots to clean...
                if ($server.VMTaskID -eq $null -and $server.SnapList.count -gt 0) {
                    $server.vmtask = $server.snaplist[0] | remove-snapshot -RunAsync -confirm:$false
                    start-sleep -milliseconds 500
                    $server.vmtaskid = $server.VMTask.Id
                    $server.snapPass++
                    if ($server.snapcount -eq 0) {$server.snapcount=$server.SnapList.count}
                    $server.activity="Cleaning snapshot $($server.snappass) of $($server.snapcount)"
                    write-progress -id $server.progID -Activity $server.VMName -Status $server.activity -PercentComplete $server.VMTask.PercentComplete
                }

                if ($server.vmtaskID -eq $null -and $server.SnapList.count -eq 0) {
                    #We have no tasks.  We have no snapshots to clean, we're done.
                    $server.status="DONE"
                    $activeCount--
                    Write-Progress -id $server.progID -Activity $server.vmname -status "Completed." -PercentComplete 100
                    Write-Progress -id $server.progID -Activity $server.vmname -status "Completed." -Completed
                    $serverTime="{0:dd}:{0:hh}:{0:mm}:{0:ss}" -f $server.timer.elapsed
                    write-host "$($server.vmname): Took $serverTIme to clean."
                    $null=$jobList.remove($server)
                    
                    break;
                }
                $server.timer.reset()
            }

#            if ($showServer -eq $true) {
#                $server|format-table -auto
#                Read-Host
#            }
        }
    }

    #No more jobs in the bucket
    if ($jobList.count -le 0) {

        write-host "Snapshot Cleanup Completed!"
        $keepGoing=$false
        write-progress -id 1 -activity "Checking Servers" -status "Completed!" -PercentComplete 100
        write-progress -id 1 -activity "Checking Servers" -status "Completed!" -Completed
        $masterTimer.stop()
        $totalTime="{0:dd}:{0:hh}:{0:mm}:{0:ss}" -f $masterTimer.elapsed
        write-host "Full Job took: $totalTime"

    }
    #start-sleep -seconds 3
} #end endless loop









