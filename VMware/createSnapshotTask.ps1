<#

Creates a Weekly Snapshot for the specified VM.

.Description

Date        Updated By  Details
11/19/2019      BW      Send time in UTC, so the VCSA will display and use the correct times in the local timezone.
11/06/2019      BW      Account for DST Changes in timezone calculation
10/31/2019      BW      Added prompting for vcServer and vmName
10/30/2019      BW      Initial coding and testing.


See posts from LucD at https://communities.vmware.com/thread/541573 for more details.

.EXAMPLE

To set up a snapshot every day at 11:45 PM
.\createSnapshotTask.ps1 -vcCred $myCred -vmName "MyVM" -daily -taskTime "11:45 PM"

To set up a snapshot every week on Thursday at 4:45 PM
.\createSnapshotTask.ps1 -vccred $myCred -vmname "MyVM" -weekly -wsWeekday "Thursday" -taskTime "4:45 PM"

To run a snapshot on the 16th of every month at 9:45 AM
.\createSnapshotTask.ps1 -vcCred $myCred -vmName "MyVM" -Monthly -dayofMonth 16 -tasktime "9:45 AM"


#>
Param(
    [Parameter(Mandatory = $false) ] [string] $vcServer="",
    [Parameter(Mandatory = $false) ] [string] $vmName="",
    [Parameter(Mandatory = $false,ParameterSetName='WeeklySnapshots') ] [switch] $weekly,
    [Parameter(Mandatory = $false,ParameterSetName='WeeklySnapshots') ] [string] $wsWeekDay="Monday",
    [Parameter(Mandatory = $false, ParameterSetName = 'WeeklySnapshots') ] [string] $wsInterval = 1,
    #[Parameter(Mandatory = $false, ParameterSetName = 'MonthlySnapshots') ] [switch] $monthly,
    [Parameter(Mandatory = $false, ParameterSetName = 'DailySnapshots') ] [switch] $daily,
    [Parameter(Mandatory = $false, ParameterSetName = 'MonthlySnapshots') ] [switch] $monthly,
    [Parameter(Mandatory = $false, ParameterSetName = 'MonthlySnapshots') ] [int] $dayOfMonth,

    [Parameter(Mandatory = $false) ] [string] $taskTime = "05:15 AM",
    [Parameter(Mandatory = $false) ] [switch] $snapMemory,
    [Parameter(Mandatory = $false) ] [switch] $quiesceFS,
    [Parameter(Mandatory = $false) ] [pscredential] $vcCred
)

$hasVI = get-command connect-viserver* -ErrorAction SilentlyContinue

if ([string]::IsNullOrEmpty($hasVI) -eq $true) {
    try {
        import-module VMware.PowerCLI
    } catch {
        write-host -foregroundcolor "Yellow" "You MUST have the VMware PowerShell Modules installed for this to work."
        write-host -foregroundcolor "yellow" "Open PowerShell as an administrator and then "
        write-host " "
        write-host -foregroundcolor "white" "install-module vmware.powercli"
        write-host " "
        write-host -foregroundcolor "Yellow" "And then try this script again."

        exit -2048;
    }
}


if ($global:DefaultVIServers.count -lt 1) { 
    if ($vcCred -eq $null ) {
    
        write-host -foreground Yellow "Please enter your VCenter Admin Credentials..."
        $username = read-host "Username: "
        $password = read-host "Password: " -AsSecureString
        $VCCred = New-Object System.Management.Automation.PSCredential -ArgumentList $username, $password
        $password = ""
    } 

    if ([string]::IsNullOrEmpty($vcServer) -eq $true) {
        write-host "Which vCenter Server are we connecting to? " -NoNewline
        $vcServer=read-host
    }
    
    connect-viserver -server $vcServer -Credential $vcCred 
}

if ([string]::IsNullOrEmpty($vmName)) {
    write-host "Which VM do you want to snapshot? " -NoNewline
    $vmName=Read-Host

}

$objVM=get-vm $vmName
if (($objVM|measure-object).count -ne 1) {
    write-host -foregroundcolor red "Could not find $vmName!"
    exit -1024;
}


#$vmRef=$objvm.extensiondata.moref
$vmRef = (get-view -viewtype virtualmachine -property Name -Filter @{"Name" = "^$($vmName)$" }).MoRef

if ($daily -eq $true) { $taskName = "$vmName Daily Snapshot" }
if ($weekly -eq $true) { $taskName = "$vmName Weekly Snapshot" }
if ($monthly -eq $true) { $taskName = "$vmName Monthly Snapshot" }

$today = get-date -format "MM-dd-yyyy"

$time = get-date -format "hh:mm tt"  -date "$taskTime"
#[datetime] $origDate=get-date -format "MM-dd-yyyy hh:mm tt" -date "$today $time"
#[datetime] $utcDate = get-date -format "yyyy-MM-dd`THH:mm:ss`Zzz00" -date "$today $time"
#[datetime] $utcDate=get-date -format "o" -date "$runDate $time"
#[datetime] $utcDate=get-date -format "MM-dd-yyyy hh:mm tt" -date "$today $time"
[datetime] $today = get-date -format "MM-dd-yyyy hh:mm tt" -date "$today $time"
$finalTZ="UTC"
$localTZ=[system.timezoneinfo]::Local
$runDate=[System.TimeZoneInfo]::ConvertTimeBySystemTimezoneId($today,$localTZ.id,$finalTZ)

$spec = New-Object VMware.Vim.ScheduledTaskSpec
$spec.name = $taskName
$spec.enabled = $true
if ( $notifyEmail ) { $spec.notification = $notifyEmail }

#Daily Parameter
if ($daily -eq $true) {
    $spec.description = "Every Day at $time"
    $spec.scheduler = new-object vmware.vim.dailytaskscheduler
    $spec.scheduler.hour = $runDate.hour
    $spec.scheduler.minute = $ruNDate.minute
    $spec.scheduler.interval=1
    $spec.scheduler.ActiveTime= $runDate
}

#Weeekly Parameter Set

if ($weekly -eq $true) {

    $weekdayList=new-object system.collections.arraylist
    [void] $weekDayList.add("Sunday")
    [void] $weekDayList.add("Monday")
    [void] $weekDayList.add("Tuesday")
    [void] $weekDayList.add("Wednesday")
    [void] $weekDayList.add("Thursday")
    [void] $weekDayList.add("Friday")
    [void] $weekDayList.add("Saturday")

    $weekDay=$weekdayList.IndexOf($wsWeekDay)
    #write-host "Weekday Says: $weekDay"
    if ($weekday -eq -1) {
        write-host "Weekday is incorrect!" -foregroundcolor Red
        exit -1024
    }
    #if ((get-date  -date $taskTime) -lt (get-date))  { do { [datetime] $runDate = (get-date -Date $runDate).adddays(1) } until ($runDate.Dayofweek -eq $wsWeekDay) }"
    if ($runDate.DayOfWeek -ne $wsWeekDay) { 
        do {
            $runDate=$runDate.adddays(1)
            #write-host "Rundate Says: $runDate - $($runDate.DayOfWeek) - $wsWeekDay "
         } until ($runDate.DayofWeek -eq $wsWeekday)
    }    
    #write-host "Checking hour..."
    #the -lt 5 here is for EST ...   adjust accordingly if you are in another timezone
    if ($runDate.hour -lt 5) {
        $runDate=$runDate.adddays(1)
        $weekDay=$weekDay+1 #Timezone adjustment moves it back a day, so we need to increment our day of the week by one.
        if ($weekday -gt 6) {$weekday=0}

    }

    #2write-host "Actual Run Date: $ruNDate"
    $spec.description = "Every $wsWeekday at $time"
    $spec.scheduler = new-object VMware.Vim.WeeklyTaskScheduler
    $spec.scheduler.hour = $runDate.hour
    $spec.scheduler.minute = $runDate.minute
    $spec.scheduler.interval = $wsInterval
    $spec.scheduler.activetime = $runDate

    switch ($weekDay) {
        0 {$spec.scheduler.Sunday = $true }
        1 {$spec.scheduler.Monday = $true }
        2 {$spec.scheduler.Tuesday = $true }
        3 {$spec.scheduler.Wednesday = $true }
        4 {$spec.scheduler.Thursday = $true }
        5 {$spec.scheduler.Friday = $true }
        6 {$spec.scheduler.Saturday = $true }

    }

}
#End Weekly Parameter Set

if ($monthly -eq $true) {
    #if ((get-date  -date $taskTime) -lt (get-date))  { do { [datetime] $runDate = (get-date -Date $runDate).adddays(1) } until ($runDate.Day -eq $dayofMonth) }
    if ($runDate.day -ne $dayOfmonth) { 
        do {
            $runDate=$runDate.adddays(1)
         } until ($runDate.Day -eq $dayOfMonth)
    }

    $description="$dayOfMonth th of every month at $taskTime"
    $spec.description = $description
    $spec.scheduler = new-object VMware.Vim.MonthlyByDayTaskScheduler
    $spec.scheduler.hour = $runDate.hour
    $spec.scheduler.minute = $runDate.minute
    $spec.scheduler.day = $dayofmonth
    $spec.scheduler.activetime = $runDate
    $spec.scheduler.interval = 1
}

#Schedule Check
#write-host $vmName
#$spec.Scheduler
#read-host "Check the schedule here..."


($spec.action = New-Object VMware.Vim.MethodAction).Name = "CreateSnapshot_Task"
$spec.action.argument = New-Object VMware.Vim.MethodActionArgument[] (4)
($spec.action.argument[0] = New-Object VMware.Vim.MethodActionArgument).Value = $spec.name
($spec.action.argument[1] = New-Object VMware.Vim.MethodActionArgument).Value = $spec.description
($spec.action.argument[2] = New-Object VMware.Vim.MethodActionArgument).Value = $false # Snapshot memory
($spec.action.argument[3] = New-Object VMware.Vim.MethodActionArgument).Value = $false # quiesce guest file system (requires VMware Tools)

$x=(Get-View -Id 'ScheduledTaskManager-ScheduledTaskManager').CreateScheduledTask($vmRef, $spec)