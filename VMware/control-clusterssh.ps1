<#
Enable SSH on PRovided cluster
#>
Param(
    [Parameter(Mandatory = $true) ] [string] $clusterName,
    [Parameter(Mandatory = $false) ] [switch] $start,
    [Parameter(Mandatory = $false) ] [switch] $stop
)

write-host "Loading $clusterName"

$clusterDetail=get-cluster $clusterName
$hostList=$clusterDetail|get-vmhost

foreach ($vmhost in $hostList) { 

    if ($start) {
        write-host "Starting SSH on $($vmHost.name)"
        Start-VMHostService -HostService  ($vmHost|get-vmhostservice|where {$_.Key -eq "TSM-SSH"})
    }

    if ($stop) {
        write-host "Stopping SSH on $($vmhost.name)"
        Stop-VMHostService -HostService  ($vmHost|get-vmhostservice|where {$_.Key -eq "TSM-SSH"}) -confirm:$false
        
    }
}

