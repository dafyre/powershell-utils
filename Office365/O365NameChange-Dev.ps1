﻿<#

.SYNOPSIS
This command prompts for the necessary information to perform a UPN change on an Office 365 Account (Staff or Faculty only!).





.DESCRIPTION
   Date UpdatedBy Details
   11/02/2017 BW  Added $debug flag and output for identifying whether or not we are in PSISE or not.
   08/04/2017 BW  Removed Stop error condition so that end-user can see any error messages when things work incorrectly.
   07/21/2017 BW  Initial Release
   07/21/2017 BW  Remove Prompt to change the Display Name.  O365 does not allow it to be changed when it syncs with AD.


#>

#Are we debugging the application?
$debug=$false

function Pause ($Message = "Press any key to continue . . .") {
 if ($psISE) {
  write-host $Message
  write-host "...running inside of ISE.  Press the Enter key to continue."
  $x=read-host
  return
 }
 
 write-host -NoNewline $message
 $key=$Host.ui.rawui.readkey("NoEcho,IncludeKeyDown")

}


if ($debug -eq $true) { write-host -ForegroundColor Yellow "Are we running in PowerShell ISE? $psISE" }


if (!$psISE) {
 write-host -foreground Yellow "Please enter your Office 365 Admin Credentials..."
 $username=read-host "Username: "
 $password=read-host "Password: " -AsSecureString
 $MSOLCredential = New-Object System.Management.Automation.PSCredential -ArgumentList $username,$password
} else {
 $MSOLCredential=Get-Credential

}



try {
  Connect-MsolService -Credential $MSOLCredential -ErrorAction Stop
  } catch {
  
  Write-Error "There was a problem loging you in to your O365 Account!"
  exit
}


#$groupEmail=[Microsoft.VisualBasic.Interaction]::InputBox("Please type the email address of the Distribution List you want to reload...","Distribution List Email") 


$currentUPN=""
$newUPN=""
$currentDisplayName=""
$newDisplayName=""

while ($true) {
 write-host "*************************************************************"
 write-host "*                                                           *"
 write-host "*             OFFICE 365 Name Change Application            *"
 write-host "*                                                           *"
 write-host "*************************************************************"
 write-host "  1) Current UPN: $currentUPN                              "
 write-host "  2) New UPN: $newUPN                                      "
 #write-host "  3) New Display Name: $newDisplayName                     "  #cannot change O365 Display name when it is synced with AD
 write-host "*************************************************************"
 write-host "* 3) Make the Changes                                       *"
 write-host "*                                                           *"
 write-host "*                                                           *"
 write-host "*                                                           *"
 write-host "*                                                           *"
 write-host "*                                                           *"
 write-host "*                                                           *"
 write-host "*                                                           *"
 write-host "*                                                           *"
 write-host "*                                                           *"
 write-host "*                     9) Exit Program                       *"
 write-host "*************************************************************"

 $choice=read-host "Please make your selection from the menu above"

 switch ($choice) {
  1 {
   #Current UPN
   write-host "Enter the User's CURRENT Email: " -NoNewline
   $temp=read-host
   if ([string]::IsNullOrEmpty($temp) -eq $false) { $currentUPN=$temp }
   clear

  }
  2 {
   #New UPN
   write-host "Enter the User's NEW Email: " -NoNewline
   $temp=read-host
   if ([string]::IsNullOrEmpty($temp) -eq $false) { $newUPN=$temp }
   clear
  }

  <#3 {
   #New Display Name
   write-host "Enter the User's New Display Name: " -NoNewline
   $temp=read-host
   if ([string]::IsNullOrEmpty($temp) -eq $false) { $newDisplayName=$temp }
   clear
  }#>

  3 {
     #Set-MsolUser -UserPrincipalName $currentUPN -DisplayName $newDisplayName -ErrorAction stop
     Set-MsolUserPrincipalName -UserPrincipalName $currentUPN -NewUserPrincipalName $newUPN
     write-host -foreground Yellow "Check for errors and then press the enter key to resume, or CTRL-C if the error needs to be reviewed."
     read-host
  }

  9 {
   write-host "Type Yes to quit the application..." -NoNewline
   #$exit=read-host
   #if ($exit.ToLower() -eq 'yes') { exit }
   exit 0
   
  }

  default {  
   Pause -message "You have made an invalid selection.  Press any key to redisplay the menu."
   clear

  }
 }
}