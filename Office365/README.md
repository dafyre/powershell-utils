Office 365 Utilities
===

# Connect-O365.ps1
An example of how to connect to Office 365 with PowerShell.

# export-O365Group.ps1
This script will export the specified group to a CSV file.

```-o365Cred```: Office 365 Credentials.  Can be supplied via get-credential.  If not provided, the script will prompt for them.

```-groupEmail```: The Email Address of the group you are exporting.

```-output```: What file do you want to save to?

```-distributionList```: If your group is a Distribution List, then use this switch.  If not provided, the script will process the group as one of the new Office 365 Groups (Unified Group).


# Office365-MailTrafficReports.ps1
This script generates Office 365 Mail Traffic Reports whenever you like, as opposed to being stuck waiting on Microsoft to send them out.

**Script REQUIRES** the ImportExcel module from https://github.com/dfinke/ImportExcel .  Instructions for installing are inside the script.

```-creds```: Office 365 Login Credentials.  Use get-credential for this.

```-start```, ```-end```: Start and End Date for the Reports.

```-aggregate```: Defaults to summary.  See Microsoft Documentation for other options.

```-doTopRecipients```: Generate a Top Recipients report.

```-doTopSenders```: Generate a Top Senders report.

```-doSummary```: Generate an email summary report.

```-doDLP```: Generate a Data Leak Prevention report.


```-doSPAM```: Generate a Spam report.

```-doAll```: Will run all of the reports and saves you (and me!) some typing.

```-doEmail```:  If you want to send the reports via emails, specify this switch.

```-fromEmail```: Who are the emails coming from?

```-toEmail```: Who are the emails going to?

```-emailCreds```: Defaults to using your O365 Credentials.

```-smtpServer```: SMTP Server you want to use for sending the messages.  Defaults to Office 365.

```-smtpPort```: SMTP Port to connect to.  Defaults to 587.

# O365-UPNChange.ps1
This script will change the UserPrincipalName for an end user in O365.  This is useful for Name Changes and such.

# O365DistributionGroupExport.ps1
This script takes you through the process of exporting a DISTRIBUTION GROUP **ONLY**.

_this has been deprecated by export-O365Group.ps1 and will no longer be updated.

# O365DistributionListUploadFromCSV.ps1
This script will upload a list of email addresses into an Office 365 Distribution list that has already been created.

# O365Logon.ps1
An Example of how to connect to Office 365.


# O365Module.ps1
Not in use.  Can probably be deleted, I just haven't gotten around to it.

# O365NameChange-Dev.ps1
Menu Driven Name/UPN Change.

# O365UnifiedGrouPExport.ps1

Exports and Office 365 Unified Group (Office 365 Group) users to a CSV File.

_this has been deprecated by export-O365Group.ps1 and will no longer be updated._
