<#
.DESCRIPTION
This script will Return a list of groups for the given $searchDisplayname.
It also Prompts for O365 Credentials if a Session does not already exist and are not sent as a parameter.


DATE        Updated By  Details
04/16/2019      BW      Initial Code.
#>
Param(
[Parameter(Mandatory=$false) ] $o365Cred=$null, #Office 365 Admin Credentials
[Parameter(Mandatory=$false) ] [switch] $displayWindow,
[Parameter(Mandatory=$true) ] [string] $searchDisplayName

)

$Session=$global:Session

if ($o365Cred  -eq $null) {
    write-host -foreground Yellow "Please enter your Office 365 Admin Credentials..."
    $username=read-host "Username: "
    $password=read-host "Password: " -AsSecureString
    $o365Cred = New-Object System.Management.Automation.PSCredential -ArgumentList $username,$password
}

if ($Session -eq $null) {
    $Session = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri https://ps.outlook.com/powershell/ -Credential $o365Cred -Authentication Basic -AllowRedirection
    Import-PSSession -AllowClobber $Session
    $global:Session=$Session
}

write-host "Loading all groups..."
$allGroups=get-group -resultsize unlimited

write-host "Checking all groups..."
$groupList=$allGroups|where {$_.members -like "*$searchDisplayName*" -and [string]::isnullorempty($_.WindowsEmailAddress) -eq $false}

if ($displayWindow -eq $false) {
    $groupList|select DisplayName,GroupType,ManagedBy,WindowsEmailAddress,RecipientType,Notes|ft -AutoSize
} else {
    $groupList|select DisplayName,GroupType,ManagedBy,WindowsEmailAddress,RecipientType,Notes|out-gridview -title "Group list for $searchDisplayName"
}


