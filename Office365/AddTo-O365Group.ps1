<#

.SYNOPSIS

This command allows you to feed a file of email addresses into an Office 365 Unified Group.

You must *FIRST* create the O365 Group in the Web Portal, and then run this script.  The first Prompt is a request for the Group Email address (ie: myemailgroup@@mydomain.net) 
and the file dialog box is a request for the file containing the email addresses you wish to load.

The file containing the email addresses should simply be one email address per line:

email.one@mydomain.net
email.two@mydomain.net
email.three@mydomain.net



.DESCRIPTION
   Date UpdatedBy Details
   03/28/2019 BW  Now Pulls the entire group for groups of more than 1k users for making the backup.
                  Switched to using get-content as opposed to import-csv.  This removes the requirement of opening
                  the file to add a header.
   01/02/2017 BW  Now makes a backup of the O365 Group's email list before executing any commands against it.
   11/14/2016 BW  Added support for determining if an email should be treated as a contact or a regular user.
   10/19/2016 BW  List now removes users from group when they do not exist in the CSV File chosen.  Also generates log file in the same folder the script is run from.
   10/10/2016 BW  Added check for Empty O365 group, updated Synopsis
   09/26/2016 BW  Added -allowClobber to Import-PSSession to fix issues where the powershell remote session has expired.
   09/19/2016 BW  Initial Release.

#>

Param(
[Parameter(Mandatory=$false) ] $o365Cred=$null #Office 365 Admin Credentials

)


$VerbosePreference="Continue"


function New-OpenFileDialog  
{ 
    param( 
    [string]$Title, 
    [string]$Directory, 
    [string]$Filter="CSV and TXT Files (*.csv,*.txt)|*.csv;*.txt" 
    ) 
        [System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms") | Out-Null 
        $objForm = New-Object System.Windows.Forms.OpenFileDialog 
        $objForm.Title = $Title 
        $objForm.InitialDirectory = $Directory 
        $objForm.Filter = $Filter 
        $objForm.ShowHelp = $True 
         
        $retVal = $objForm.ShowDialog() 
         
        If ($retVal -eq "OK") 
        { 
            Return $objForm.FileName 
        } 
        Else 
        { 
            Exit 
        } 
} 



<#

Program Begins here:


#>


#Prompt for O365 Credentials
if ($o365Cred -eq $null) {
    write-host -foreground Yellow "Please enter your Office 365 Admin Credentials..."
    $username=read-host "Username: "
    $password=read-host "Password: " -AsSecureString
    $o365Cred = New-Object System.Management.Automation.PSCredential -ArgumentList $username,$password
    $password=""
 }

$Session = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri https://ps.outlook.com/powershell/ -Credential $O365Cred -Authentication Basic -AllowRedirection
Import-PSSession -AllowClobber $Session



#Windows / VB.Net Libraries
[System.Reflection.Assembly]::LoadWithPartialName('Microsoft.VisualBasic') | Out-Null


$groupEmail=[Microsoft.VisualBasic.Interaction]::InputBox("Please type the email address of the O365 Unified Group you want to reload...","Distribution List Email") 

if ($groupEmail.length -eq 0) {
  write-host "No group given, exiting."
  exit

}

write-host "Group email is $groupemail" 


$o365Group=new-object System.Collections.ArrayList

$dGroup=get-unifiedgroup $groupEmail

if (!$dGroup) {
 write-host "Cannot find a group with this an email address of $groupEmail"
 exit

}

#$group=get-distributiongroupmember $groupEmail -Resultsize Unlimited
$group=get-unifiedgroupLinks -LinkType Members -ResultSize Unlimited $groupEmail

write-host "Reading Group: $groupEmail... $($group.count) records to read..."
foreach ($user in $group) {
 #$tmpInfo=@{}
 $displayName=$user.DisplayName
 $primaryEmail=$user.PrimarySmtpAddress.tolower()
 $retval=$o365Group.add($primaryEmail)

}

#$o365Group|Out-GridView


$csvFile=New-OpenFileDialog -Title "Load CSV file" -Directory="C:\" -Filter "CSV (*.csv)|*.csv"

if ($csvFile.length -eq 0) {
 write-host "Unable to continue without a file to load!"
 exit

}

write-host "Loading data from $csvFile..."
$backupFile=$csvFile.split(".")[0]


$backupFile="$backupFile.O365Backup.$(date -format "MMddyyyyHms").csv"
write-host "Backing up O365 Group to $backupFile ..."

$group|foreach ($_) {"$($_.DisplayName), $($_.PrimarySmtpaddress.tolower())"}|out-file $backupFile

#$listServ=import-csv -Path $csvFile
$listServ=get-content $csvFile


$listServeEmails=new-object System.Collections.ArrayList

foreach ($item in $listServ) {
 $tEmail=$item.tolower()
 $tEmail=$tEmail.trim()

 $retval=$listServeEmails.add($tEmail)

}

write-host "$groupEmail : $($listServeEmails.count) in the ListServe , and $($o365Group.count) in the O365 List."

$pass=0

$logFile="$csvFile.log"

start-transcript -path $logFile -Append


#Adds all Users from CSV File into the list.
foreach ($user in $listServeEmails) {
 if ($o365Group.count -eq 0) {$inList=$false} else { $inList=$o365Group -contains $user }
 if ($inList -eq $false) {
  $pass++
  #write-host "$pass) Adding $user to $groupEmail"
  $percentComplete=(($pass/$listServeEmails.count)*100)
  write-progress -Activity "Adding $user to $groupEmail..." -PercentComplete $percentComplete
  #Add-DistributionGroupMember -Identity $groupEmail -Member $user
  add-unifiedgroupLinks -identity $groupEmail -LinkType Members -Links $user
 }
}

stop-transcript
