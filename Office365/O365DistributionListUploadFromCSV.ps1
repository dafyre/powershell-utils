﻿<#

.SYNOPSIS

This command allows you to feed a file of email addresses into an Office 365 Distribution Group.

You must *FIRST* create the O365 Group in the Web Portal, and then run this script.  The first Prompt is a request for the Group Email address (ie: myemailgroup@@mydomain.net) 
and the file dialog box is a request for the file containing the email addresses you wish to load.

THE FIRST LINE of the CSV FILE MUST BE:

Email,

The rest of the lines are the email addresses


Email
email.one@mydomain.net
email.two@mydomain.net
email.three@mydomain.net

This version will make the Distribution group match what is in the CSV file you upload.  
***Users not in the CSV file will not be in the O365 Distribution List!***


.DESCRIPTION
   Date UpdatedBy Details
   01/02/2017 BW  Now makes a backup of the O365 Group's email list before executing any commands against it.
   11/14/2016 BW  Added support for determining if an email should be treated as a contact or a regular user.
   10/19/2016 BW  List now removes users from group when they do not exist in the CSV File chosen.  Also generates log file in the same folder the script is run from.
   10/10/2016 BW  Added check for Empty O365 group, updated Synopsis
   09/26/2016 BW  Added -allowClobber to Import-PSSession to fix issues where the powershell remote session has expired.
   09/19/2016 BW  Initial Release.

#>



#CONFIGUATION OPTIONS:

#This is the primary email domain, and is used to determine whether or not a contact records needs to be used vs a User Mailbox.

Param(
[Parameter(Mandatory=$false) ] $o365Cred=$null, #Office 365 Admin Credentials
[Parameter(Mandatory=$true) ] $emailDomain
)


$VerbosePreference="Continue"


function New-OpenFileDialog  
{ 
    param( 
    [string]$Title, 
    [string]$Directory, 
    [string]$Filter="CSV and TXT Files (*.csv,*.txt)|*.csv;*.txt" 
    ) 
        [System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms") | Out-Null 
        $objForm = New-Object System.Windows.Forms.OpenFileDialog 
        $objForm.Title = $Title 
        $objForm.InitialDirectory = $Directory 
        $objForm.Filter = $Filter 
        $objForm.ShowHelp = $True 
         
        $retVal = $objForm.ShowDialog() 
         
        If ($retVal -eq "OK") 
        { 
            Return $objForm.FileName 
        } 
        Else 
        { 
            Exit 
        } 
} 



<# Program Begins here: #>

if ($o365Cred -eq $null) {
    write-host -foreground Yellow "Please enter your Office 365 Admin Credentials..."
    $username=read-host "Username: "
    $password=read-host "Password: " -AsSecureString
    $o365Cred = New-Object System.Management.Automation.PSCredential -ArgumentList $username,$password
    $password=""
}

$Session = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri https://ps.outlook.com/powershell/ -Credential $o365Cred -Authentication Basic -AllowRedirection
Import-PSSession -AllowClobber $Session


<#
#Prompt for O365 Credentials
if ($Session.Availability -ne "Available") {
 $UserCredential = Get-Credential
 $Session = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri https://ps.outlook.com/powershell/ -Credential $UserCredential -Authentication Basic -AllowRedirection
 Import-PSSession -AllowClobber $Session
}
#>



#Windows / VB.Net Libraries
[System.Reflection.Assembly]::LoadWithPartialName('Microsoft.VisualBasic') | Out-Null


$groupEmail=[Microsoft.VisualBasic.Interaction]::InputBox("Please type the email address of the Distribution List you want to reload...","Distribution List Email") 

if ($groupEmail.length -eq 0) {
  write-host "No group given, exiting."
  exit

}

write-host "Group email is $groupemail" 


$o365Group=new-object System.Collections.ArrayList

$dGroup=get-distributiongroup $groupEmail

if (!$dGroup) {
 write-host "Cannot find a group with this an email address of $groupEmail"
 exit

}

$group=get-distributiongroupmember $groupEmail -Resultsize Unlimited

foreach ($user in $group) {
 $tmpInfo=@{}
 $displayName=$user.DisplayName
 $primaryEmail=$user.PrimarySmtpAddress.tolower()
 $retval=$o365Group.add($primaryEmail)

}

#$o365Group|Out-GridView


$csvFile=New-OpenFileDialog -Title "Load CSV file" -Directory="C:\" -Filter "CSV (*.csv)|*.csv"

if ($csvFile.length -eq 0) {
 write-host "Unable to continue without a file to load!"
 exit

}

write-host "Loading data from $csvFile..."
$backupFile=$csvFile.split(".")[0]


$backupFile="$backupFile.O365Backup.$(date -format "MMddyyyyHms").csv"
write-host "Backing up O365 Group to $backupFile ..."

$group|foreach ($_) {"$($_.DisplayName), $($_.PrimarySmtpaddress.tolower())"}|out-file $backupFile

$listServ=import-csv -Path $csvFile

$listServeEmails=new-object System.Collections.ArrayList

foreach ($item in $listServ) {
 $tEmail=$item.email.tolower()
 $tEmail=$tEmail.trim()

 $retval=$listServeEmails.add($tEmail)

}

write-host "$groupEmail : $($listServeEmails.count) in the ListServe , and $($o365Group.count) in the O365 List."

$pass=0

$logFile="$csvFile.log"

start-transcript -path $logFile -Append


#Removes People from O365 that are not in the CSV File and $remove is set to $true
if ($o365Group.count -gt 0) {


 foreach ($user in $o365Group) {
  $inlist=$listServeEmails -contains $user
  if ($inList -eq $false -and [string]::IsNullOrEmpty($user) -eq $false) {
   if ($VerbosePreference -ne "Continue") {write-host "Removing $user from $groupEmail"}
   write-verbose "Removing $user from $groupEmail"
   remove-distributiongroupmember -Identity $groupEmail -member $user -confirm:$false

  }
 }
}
#end removes from o365 that are not in CSV file

#Adds all Users from CSV File that are not already in the list.
foreach ($user in $listServeEmails) {
 if ($o365Group.count -eq 0) {$inList=$false} else { $inList=$o365Group -contains $user }
 if ($inList -eq $false) {
  $pass++
  $emailCheck=$user.split('@')[1]
  if ($VerbosePreference -ne "Continue") {write-host "$pass) Adding $user to $groupEmail"}
  write-verbose "$pass) Adding $user to $groupEmail"
  Write-Verbose "Email Check says: $emailCheck"
  if ($emailCheck  -ne $emailDomain) {
   #If the Email address is not an @MYDOMAIN.COM email address, then treat it as a mail contact record. 
   $user=get-mailcontact $user
   $user=$user.Id
   $user=$user.tostring()
  }

  #write-host "$groupEmail : $user is in List Serv? $inList"
 
  Add-DistributionGroupMember -Identity $groupEmail -Member $user
 }
}

stop-transcript
