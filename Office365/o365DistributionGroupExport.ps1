﻿<#

.SYNOPSIS

This command allows you to export a list of members from an O365 Distribution group to a CSV file for easy sharing.

The CSV File will be formatted as:
Display Name, email address




.DESCRIPTION
   Date UpdatedBy Details
   01/23/2017  BW  Modified Save dialog to return false when no file name is chosen or the cancel button is clicked.
   01/18/2017  BW  Original Release

#>

function New-SaveFileDialog  
{ 
    param( 
    [string]$Title, 
    [string]$Directory, 
    [string]$Filter="CSV and TXT Files (*.csv,*.txt)|*.csv;*.txt" 
    ) 
        [System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms") | Out-Null 
        $objForm = New-Object System.Windows.Forms.SaveFileDialog 
        $objForm.Title = $Title 
        $objForm.InitialDirectory = $Directory 
        $objForm.Filter = $Filter
        $objForm.ShowHelp = $True 
         
        $retVal = $objForm.ShowDialog() 
         
        If ($retVal -eq "OK") 
        { 
            Return $objForm.FileName 
        } 
        Else 
        { 
            return $false
        } 
} 




<#

Program Begins here:


#>


#Prompt for O365 Credentials
if ($Session.Availability -ne "Available") {
 $UserCredential = Get-Credential
 $Session = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri https://ps.outlook.com/powershell/ -Credential $UserCredential -Authentication Basic -AllowRedirection
 Import-PSSession -AllowClobber $Session
}




#Windows / VB.Net Libraries
[System.Reflection.Assembly]::LoadWithPartialName('Microsoft.VisualBasic') | Out-Null


$groupEmail=[Microsoft.VisualBasic.Interaction]::InputBox("Please type the email address of the Distribution List you want to reload...","Distribution List Email") 

if ($groupEmail.length -eq 0) {
  write-host "No group given, exiting."
  exit

}

write-host "Group email is $groupemail" 


$csvFile=New-SaveFileDialog -Title "Load CSV file" -Directory="C:\" -Filter "CSV (*.csv)|*.csv"

Get-DistributionGroupMember $groupEmail|select @{Label="Name";Expression={$_.DisplayName}},@{Label="Email Address";Expression={$_.PrimarySmtpAddress}}|export-csv -notype -path $csvFile

write-host "File saved as $csvFile..."




