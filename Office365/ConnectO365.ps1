#Prompt for O365 Credentials
if ($Session.Availability -ne "Available") {
 $UserCredential = Get-Credential
 $Session = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri https://ps.outlook.com/powershell/ -Credential $UserCredential -Authentication Basic -AllowRedirection
 Import-PSSession -AllowClobber $Session
}