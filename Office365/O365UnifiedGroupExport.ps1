﻿<#

.SYNOPSIS

This command allows you to export a list of members from an O365 Group (the new kind) to a CSV file for easy sharing.

The CSV File will be formatted as:
Display Name, email address




.DESCRIPTION
   Date UpdatedBy Details
   01/23/2017  BW  Modified save dialog box to return $false if no file is chosen or the canel button is pressed.

   01/19/2017  BW  Original Release

#>

#Copied Utility functions from O365Module.ps1

function New-SaveFileDialog  { 
    param( 
    [string]$Title, 
    [string]$Directory, 
    [string]$Filter="CSV and TXT Files (*.csv,*.txt)|*.csv;*.txt" 
    ) 
        [System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms") | Out-Null 
        $objForm = New-Object System.Windows.Forms.SaveFileDialog 
        $objForm.Title = $Title 
        $objForm.InitialDirectory = $Directory 
        $objForm.Filter = $Filter 
        $objForm.ShowHelp = $True 
         
        $retVal = $objForm.ShowDialog() 
         
        If ($retVal -eq "OK") 
        { 
            Return $objForm.FileName 
        } 
        Else 
        { 
            return $false
        } 
} 



#Program Begins here:


#Prompt for O365 Credentials if necessary!

if ($Session.Availability -ne "Available") {
 $UserCredential = Get-Credential
 $Session = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri https://ps.outlook.com/powershell/ -Credential $UserCredential -Authentication Basic -AllowRedirection
 Import-PSSession -AllowClobber $Session
}

if ($Session.Availability -ne "Available") {
 write-error "There was an error activating your session."
 exit

}


#Windows / VB.Net Libraries
[System.Reflection.Assembly]::LoadWithPartialName('Microsoft.VisualBasic') | Out-Null


$groupEmail=[Microsoft.VisualBasic.Interaction]::InputBox("Please type the email address of the Distribution List you want to reload...","Distribution List Email") 

if ($groupEmail.length -eq 0) {
  write-host "No group given, exiting."
  exit

}

write-host "Group email is $groupemail" 


$csvFile=New-SaveFileDialog -Title "Load CSV file" -Directory="C:\" -Filter "CSV (*.csv)|*.csv"

Get-UnifiedGroupLinks $groupEmail -LinkType Members|select @{Label="Name";Expression={$_.DisplayName}},@{Label="Email Address";Expression={$_.PrimarySmtpAddress}}|export-csv -notype -path $csvFile

write-host "File saved as $csvFile..."
