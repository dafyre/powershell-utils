﻿<#
.Synopsis
Powershell Module Required by O365* scripts.

.Description

This module contains commands for things such as displaying File Open and Save Dialog boxes, as well as a Windows Input and Popup message boxes.

For portability and ease of use for other end-users, the functions below are copied into the scripts where they are needed.


Date UpdatedBy Details
01/24/2017 BW Initial release


#>

#Load Windows API Calls and dot Net assemblies
[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.VisualBasic")

###

function New-OpenFileDialog  
{ 
    param( 
    [string]$Title, 
    [string]$Directory, 
    [string]$Filter="CSV and TXT Files (*.csv,*.txt)|*.csv;*.txt" 
    ) 
        [System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms") | Out-Null 
        $objForm = New-Object System.Windows.Forms.OpenFileDialog 
        $objForm.Title = $Title 
        $objForm.InitialDirectory = $Directory 
        $objForm.Filter = $Filter 
        $objForm.ShowHelp = $True 
         
        $retVal = $objForm.ShowDialog() 
         
        If ($retVal -eq "OK") 
        { 
            Return $objForm.FileName 
        } 
        Else 
        { 
            Exit 
        } 
} 


function New-SaveFileDialog  
{ 
    param( 
    [string]$Title, 
    [string]$Directory, 
    [string]$Filter="CSV and TXT Files (*.csv,*.txt)|*.csv;*.txt" 
    ) 
        [System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms") | Out-Null 
        $objForm = New-Object System.Windows.Forms.SaveFileDialog 
        $objForm.Title = $Title 
        $objForm.InitialDirectory = $Directory 
        $objForm.Filter = $Filter 
        $objForm.ShowHelp = $True 
         
        $retVal = $objForm.ShowDialog() 
         
        If ($retVal -eq "OK") 
        { 
            Return $objForm.FileName 
        } 
        Else 
        { 
            return $false
        } 
} 



function Show-MessageBox (){
    param( 
    [string]$Title="Message Box", 
    [Parameter(Mandatory=$true)][string]$Message,
    [string]$MessageType='YesNo'
    ) 
 
 switch ($messageType) {
  'YesNo' {$MessageType='YesNo,Question'}
  'YesNoAlert' {$MessageType='YesNo,exclamation'}
  'YesNoCancel' {$MessageType='YesNoCancel,Question'}
   'OKAlert' {$MessageType='OKOnly,exclamation'}
 }
 $msgBox=[Microsoft.VisualBasic.Interaction]::MsgBox($Message,$MessageType,$title)

 return $msgBox
}
