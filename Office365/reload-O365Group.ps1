<#
Reload-O365Group

.SYNOPSIS
    Reload Office 365 Distribution List to CSV

.DESCRIPTION
   Date         UpdatedBy       Details
   10/30/2019   BW              Changed Unified Group to Boolean
   08/15/2019   BW              Formatting updates for the code.  Release to Sysadmin share.
   03/28/2019   BW              Now prompts for necessary credentials, email group names and output files.
   03/06/2019   BW              Updated to work with both Office 365 Unified Groups as well as Distribution Lists.
#>

Param(
[Parameter(Mandatory=$false) ] [pscredential] $o365Cred, #Office 365 Admin Credentials
[Parameter(Mandatory=$true) ] [string] $groupEmail="crashdummy@mydomain.local",  #email of the group you want to export
[Parameter(Mandatory=$true) ] $output="output.csv", #File to Save to
)

$distributionList=$true

write-host -ForegroundColor "Yellow" "Checking for Office 365 PowerShell Module..."

$hasModule = try {get-command connect-msolservice} catch {$hasModule=$false}

if (!$hasModule) {
    Write-host -foregroundcolor "Red" "Office 365 PowerShell Modules not installed.  Cannot continue."   
}

if ($output -eq $false) {
    write-host -foregroundcolor "Red" "No output file given.  Cannot continue."
    Exit
}


if ($o365Cred -eq $null) {
    write-host -foreground Yellow "Please enter your Office 365 Admin Credentials..."
    $username=read-host "Username: "
    $password=read-host "Password: " -AsSecureString
    $o365Cred = New-Object System.Management.Automation.PSCredential -ArgumentList $username,$password
    $password=""
}

$Session = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri https://ps.outlook.com/powershell/ -Credential $o365Cred -Authentication Basic -AllowRedirection
 Import-PSSession -AllowClobber $Session


 Get-DistributionGroupMember -Resultsize unlimited $groupEmail|select @{Label="Name";Expression={$_.DisplayName}},@{Label="Email Address";Expression={$_.PrimarySmtpAddress}}|export-csv -notype -path $output
