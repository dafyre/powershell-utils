<#

Sets the out of office message for a given user.



#>

Param(
[Parameter(Mandatory=$false) ] $o365Cred=$null, #Office 365 Admin Credentials
[Parameter(Mandatory=$true) ] [string] $userEmail="crashdummy@mydomain.local",  #email of the group you want to export
[Parameter(Mandatory=$false)] [switch] $activateMessage,
[Parameter(Mandatory=$false)] [string] $messageBody,
[Parameter(Mandatory=$false)] [datetime] $startDate,
[Parameter(Mandatory=$false)] [datetime] $endDate

)

$dummy="Hello Dummy!"

if ($o365Cred -eq $null) {
    write-host -foreground Yellow "Please enter your Office 365 Admin Credentials..."
    $username=read-host "Username: "
    $password=read-host "Password: " -AsSecureString
    $o365Cred = New-Object System.Management.Automation.PSCredential -ArgumentList $username,$password
    $password=""
}

$Session = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri https://ps.outlook.com/powershell/ -Credential $o365Cred -Authentication Basic -AllowRedirection
 Import-PSSession -AllowClobber $Session

if ([string]::isnullorempty($messageBody)) {

$messageBody=@"

This script is not complete.

"@

write-host "Message Body says:"
write-host $messageBody



}