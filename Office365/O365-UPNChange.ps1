﻿#Windows / VB.Net Libraries
[System.Reflection.Assembly]::LoadWithPartialName('Microsoft.VisualBasic') | Out-Null

#PowerShell Modules
import-module msonline


#$groupEmail=[Microsoft.VisualBasic.Interaction]::InputBox("Please type the email address of the Distribution List you want to reload...","Distribution List Email") 

$cred=get-credential

Connect-MsolService -Credential $cred

$currentUPN=[Microsoft.VisualBasic.Interaction]::InputBox("Please type the users **CURRENT** UPN","Current UPN") 
$newUPN=[Microsoft.VisualBasic.Interaction]::InputBox("Please type the users **NEW** UPN","NEW UPN") 

try {
 Get-MsolUser -UserPrincipalName $currentUPN -ErrorAction Stop
}
catch {
 write-warning "There was an error locating the original User Principal Name: $currentUPN"
 Write-Warning $_.Exception.Message
}


Set-MsolUserPrincipalName -UserPrincipalName $currentUPN -NewUserPrincipalName $newUPN


