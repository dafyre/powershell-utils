$userCred=Get-Credential
$session=$global:Session
write-host "Authenticating to O365..."
$Session = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri https://ps.outlook.com/powershell/ -Credential $userCred -Authentication Basic -AllowRedirection
Import-PSSession -AllowClobber $Session
if ($Session -ne $false) { $script:isAuthenticated=$true }
