<#
 .SYNOPSIS
    This script will generate the list of Top Recipients, Top Senders, a Mail Traffic Summary, and a DLP Policy Hits report and put them into Excel files.
    if No Credentials are given, it wlil prompt for them.


 .EXAMPLES
    ./O365-MailTrafficReport.ps1 -doDLP
    Generates an excel file called DLPPolicMatches*.xlsx -- The file name will include a time stamp.

    -------

    ./O365-MailtrafficReport.ps1 -doDLP -doTopSenders -doTopRecipients -doSummary
    Runs all of the reports and generates an excel file for each of them.


 .NOTES
    EXTERNAL REQUIREMENTS:

    Office 365 Connection
    ======================
    This script requires the Office 365 Powershell Administration tools to be installed on your computer.
    Sign in Assistant here: https://www.microsoft.com/en-us/download/details.aspx?id=41950

    Open PowerShell as an administrator and then 

    install-module msonline

    If you are asked to install the Nuget provider, answer Y for yes, and then hit enter.
    If prompted to install the module from PSGallery, answer Y for  yes, and then hit enter.


    IMPORTEXCEL MODULE
    ===================
    Script uses the ImportExcel module from https://github.com/dfinke/ImportExcel 
    to manipulate the excel spreadsheets.  The module can be installed using:

    Install-Module ImportExcel from a Powershell console as an Administrator

    In order for the commands to work properly, you must have the proper permissions on your Office 365 setup, or the commands 
    will not be exported for your logon session.
    

 .DESCRIPTION
    Date        UpdatedBy   Comments
    09/24/2018  BW          Live Run.  It works!
    09/17/2018  BW          Updated ExportExcel module to eliminate Spaces
    07/23/2018  BW          Updated to include the Mail Spam Traffic Report
    6/13/2018   BW          Initial Release
    
#>
[CmdletBinding(DefaultParametersetName='None')] 
Param(
    [Parameter(Mandatory=$true)] [PSCredential] $creds,
    [Parameter(Mandatory=$false)] [datetime]$start,
    [Parameter(Mandatory=$false)] [datetime] $end,
    [Parameter(Mandatory=$false)] [String] $aggregate="Summary",
    [Parameter(Mandatory=$false)] [switch] $doTopRecipients,
    [Parameter(Mandatory=$false)] [switch] $doTopSenders,
    [Parameter(Mandatory=$false)] [switch] $doSummary,
    [Parameter(Mandatory=$false)] [switch] $doDLP,
    [Parameter(Mandatory=$false)] [switch] $doSPAM,

    [Parameter(Mandatory=$false)] [switch] $doAll,
    [Parameter(ParameterSetName='emails',Mandatory=$false)] [switch] $doEMail,
    [Parameter(ParameterSetName='emails',Mandatory=$true)] [string] $fromEMail,
    [Parameter(ParameterSetName='emails',Mandatory=$true)] [string] $toEMail,
    [Parameter(ParameterSetName='emails',Mandatory=$false)] $emailCreds = $false,
    [Parameter(ParameterSetName='emails',Mandatory=$false)] [string] $smtpServer="smtp.office365.com",
    [Parameter(ParameterSetName='emails',Mandatory=$false)] [int] $smtpPort=587
)

### Windows /dot Net API
Add-Type -AssemblyName System.Windows.Forms
Add-Type -AssemblyName System.Windows.Forms.Datavisualization


### end Windows / dot Net API

### GLOBAL VARS
$isAuthenticated=$false
$Session=$global:Session
$reportSuffix="$(get-date -Format MMddyyyyhmmss).xlsx"
$chartSuffix="$(get-date -Format MMddyyyyhmmss)"


###

#Dot Net stuff

Add-Type -AssemblyName PresentationCore,PresentationFramework

################# UTILITY FUNCTIONS ###############################


### function authenticate() ###
function authenticate {
    $Session=$script:Session
    $userCred=$script:creds
    $credFile=$script:credFile

    write-host "Authenticating to Office 365..."
    if ($Session) {
     #Check for existing session that is available
     write-host "Checking for active sessions..."
     $activeSessions=get-pssession|where {$_.ComputerName -eq "ps.outlook.com" -and $_.Availability -eq "Available"}
     write-host "Found $($activeSessions.count)"

     if ($activeSessions.count -gt 0) {
      $Session=$activeSessions[0]
      
      write-host "Reusing session..."
      $script:isAuthenticated=$true

      return $true

     } else {
      #$userCred=new-object -typename System.Management.Automation.pscredential -argumentList $user,$securePass
      write-host "Starting new Office 365 Session."
      $Session = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri https://ps.outlook.com/powershell/ -Credential $userCred -Authentication Basic -AllowRedirection
      if ($session -isnot [System.Management.Automation.Runspaces.PSSession]) {return "False"}
     }
   
     Import-PSSession -AllowClobber $Session
     if ($Session -ne $false) {
       $script:isAuthenticated=$true
       
       return $true
      
     }
    }  #endif

    if (!$Session) {
        write-host "Starting new Office 365 Session."
        $Session = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri https://ps.outlook.com/powershell/ -Credential $userCred -Authentication Basic -AllowRedirection
        if ($session -isnot [System.Management.Automation.Runspaces.PSSession]) {return "False"}
        Import-PSSession -AllowClobber $Session
        if ($Session -ne $false) {
          $script:isAuthenticated=$true
          $script:Session=$Session
          $global:Session=$Session
          return $true
        }
    }

    if ($Session -ne $false) {return $true}
    return $false
   
} #end authenticate()

### end function authenticate() ###

function checkfor-Excelmodule () {
    write-host -ForegroundColor "Yellow" "Checking for ImportExcel module"
    $oldErrorPref=$ErrorActionPreference
    try {
        if (get-command export-excel) {return $true}
    } catch {
        $ErrorActionPreference=$oldErrorPref
        return $false
    }
}

function Export-ExcelChart {
    Param(
        [Parameter(Mandatory=$true)] $xlFile,
        [Parameter(Mandatory=$true)] $destFile,
        [Parameter(Mandatory=$false)] $sheetName="Sheet1"
    )

    try {$excelApp=new-object -ComObject "Excel.Application"} 
    catch {write-host "There was an error.  This usually means that excel is not installed."; exit -128}

    try {$workbook=$excelApp.Workbooks.open($xlFile)}
    catch {write-host "Unable to Open $xlFile!"; write-host $error[0]; exit -1024}

    $worksheet=$workbook.worksheets($sheetName)
    $chartCount=0
    foreach ($chart in $worksheet.ChartObjects([System.Type]::Missing)) {
        $x=$chartCount++

        write-host "Processing Charts..."
        $excelApp.goto($chart.TopLeftCell,$true)
        $destFile="$destFile`_$($chartCount.tostring("000")).png"
        if ($chart.Chart.Export($destFile,"PNG",$false)) {
            write-host "Exported $chartName to $destFile."
        } else { write-host "Error saving chart..." }
        #Check on this
        #[System.Runtime.Interopservices.Marshal]::ReleaseComObject($activate) | Out-Null
        #[System.Runtime.Interopservices.Marshal]::ReleaseComObject($xlsRng) | Out-Null
        $excelApp.DisplayAlerts=$false
        $x=[System.Runtime.Interopservices.Marshal]::ReleaseComObject($chart)
        $x=[System.Runtime.Interopservices.Marshal]::ReleaseComObject($worksheet)
        $workbook.close($false,$null,$null)
        $x=[System.Runtime.Interopservices.Marshal]::ReleaseComObject($workbook)
        $excelApp.Quit()
        $x=[System.Runtime.Interopservices.Marshal]::ReleaseComObject($excelApp)
    
    }
}
############## END UTILITY FUNCTIONS ##############################

   
############## MAIN PROGRAM #######################################
$hasExcel=checkfor-ExcelModule

if (!$hasExcel) {
    write-host -foregroundcolor "Red" "ImportExcel module was NOT FOUND!"
    write-host -foregroundcolor "Yellow" "To fix this, run PowerShell as an administrator and run the command:"
    write-host -forgegroundcolor "White" "install-module ImportExcel"
    write-host "Press the enter key to exit."
    Read-Host

    exit -1024

}

if ([string]::IsNullOrEmpty($start)) {
    $tmpDate=Get-Date
    

    do {
        if ($tmpDate.DayOfWeek -eq "Monday") {break}
        $tmpDate=$tmpDate.addDays(-1)
        #write-host $tmpDate

    } until ($tmpDate.DayofWeek -eq "Monday")
    $start=$tmpDate.addDays(-7)
        
}

if ([string]::IsNullOrEmpty($end)) {
    $end=$start.AddDays(6)
    $end=get-date $end -format "MM/dd/yyyy"

}

[string] $start=get-date $start -format "MM/dd/yyyy"
[string] $end = get-date $end -format "MM/dd/yyyy"

if ($doEMail) {
 if ($emailCreds -eq $false) {$emailCreds=$creds}
}

write-host -foreground "Yellow" "Running for $start to $end"
#Read-Host
$x=authenticate
$topRecipients=""
$topSenders=""

if (!$isAuthenticated) {
    write-host "Not authenticated!"
    Read-Host
    exit -4096
}

if ($isAuthenticated) {
    #We're authenticated, let's get to work!
    if ($doTopSenders -eq $false -and $doTopRecipients -eq $false -and $doSummary -eq $false -and $doDLP -eq $false -and $doSPAM -eq $false) {$doAll=$true}
    if ($doSummary -eq $true) {$doTopRecipients =$true; $doTopSenders = $true}


    #Top Mail Recipients
    if ($doTopRecipients -or $doAll) {
        $report=""
        try {
            $report=get-mailtrafficsummaryreport -startdate $start -enddate $end -category TopMailRecipient -pagesize 10|
            select @{label="Top Recipient";expr="C1"},@{label="Message Count";expr="C2"}
            $topRecipients=$report
        } catch {
            write-host -foregroundcolor "Yellow" "Error executing get-mailtrafficsummaryreport.  You may not have permissions to run these commands!"
            Read-Host
            exit -5120
        }
    
        #$report|out-gridview -Title "Top 10 Mail Recipients for week of $start to $end"
        write-host -foregroundcolor "Yellow" "Generating Top Recipients Report..."
        $recipientFile="MailTopRecipients_$reportSuffix"

        $topRecipients|export-excel $recipientFile -BoldTopRow -AutoSize

    }

    if ($doTopSenders -or $doAll) {
        #Top Mail Senders
        $report=""
        $report=get-mailtrafficsummaryreport -startdate $start -enddate $end -Category TopmailSender -pagesize 10|
        select @{label="Top Sender";expr="C1"},@{label="Message Count";expr="C2"}

        $topSenders=$report
        
        $senderFile="MailTopSenders_$reportSuffix"
        write-host -foregroundcolor "Yellow" "Generating Top Senders Report..."
        $topSenders|export-excel $senderFile -BoldTopRow -Autosize


        #$report|out-gridview -title "Top 10 Mail Senders for week of $start to $end"        
    }

    if ($doSummary -or $doAll) {
        #Mail Summary for Charting
        #$chartData=new-object System.Collections.ArrayList
        $objMailSummary=new-object System.collections.ArrayList
        
        write-host -foregroundcolor "Yellow" "Generating Mail Traffic Report..."

        $mailSummary=get-mailtrafficreport -startdate $start -enddate $end -EventType GoodMail,Malware,SpamContentFiltered,SpamIPBlock,SpamEnvelopeBlock,SpamDBEBFilter,TransportRuleMessages,AdvancedProtectionMalware
        $summaryDates=$mailSummary.Date|unique

        foreach ($itemDate in $summaryDates) {
            #write-host "Checking $itemDate"
            $itemDate=get-date -format "M/d/yyyy" $itemDate
            $reportItems=$mailSummary|where {(get-date -format "M/d/yyyy" $_.Date) -eq $itemDate}

            #$reportItems
            #write-host "Check the Report Items..."
            #Read-Host

            $tmpObj = [pscustomobject] @{
                Date = $itemDate
                GoodMail = 0
                Spam = 0
                Malware = 0
                TransportRuleHits = 0
                AdvancedMalware = 0
                TotalMessages = 0
                Start = $start
                End = $end

            }
            foreach ($item in $reportItems) {
                #$item
                if ($item.EventType -eq "GoodMail") {$tmpObj.Goodmail = $tmpObj.Goodmail+$item.MessageCount}
                if ($item.EventType -eq "Malware") {$tmpObj.Malware = $tmpObj.Malware + $item.MessageCount}
                if ($item.EventType -match "Spam*") {$tmpObj.Spam = $tmpObj.Spam + $item.MessageCount}
                if ($item.EventType -eq "TransportRuleMessages") {$tmpObj.TransportRuleHits=$tmpObj.TransportRuleHits+$item.MessageCount}
                if ($item.EventType -eq "AdvancedProtectionMalware") {$tmpObj.AdvancedMalware=$tmpObj.AdvancedMalware+$item.MessageCount}
                $tmpObj.TotalMessages=$tmpObj.TotalMessages+$item.MessageCount
                #write-host "Check the above item..."
                #read-host
            }
            $x=$objMailSummary.add($tmpObj)
        }

        #$objMailSummary|out-gridview -title "Sent & Received Summary for Week of $start to $end"
        $summaryFile="MailTrafficReport_$reportSuffix"

        #write-host "Mail Traffic Report File is: $summarytFile"
        #Read-Host

        $rangeSeries="GoodMail","Spam","Malware","TransportRuleHits","AdvancedMalware","TotalMessages"
        $xlChart=new-excelchart -Title "Mail Traffic Weekly $start to $end" -ChartType Line -XRange "Date" -YRange $rangeSeries -SeriesHeader $rangeSeries -width 700 -height 400

    
        $objMailSummary|export-excel -ExcelChartDefinition $xlChart $summaryFile -AutoNameRange -AutoSize -BoldTopRow

        if ($doEmail) {
            $summaryChartFile="$pwd\summaryChart_$chartSuffix"
            sleep -seconds 5 #Gives Export=Excel a chance to wrap up and save the file.
    
            Export-ExcelChart -xlFile $pwd\$summaryFile -destFile $summaryChartFile #no extension needed.  It is applied by export-excelchart

            $summaryChartFile="$pwd\summaryChart_$($chartSuffix)_001.png"

            write-host "Preparing $summaryChartFile ..."

            write-host "Sending Mail Traffic Summary Report"
            $subject = "Office 365 Mail Traffic Report week of $start"
            #$smtpServer = "smtp.office365.com"
            #$smtpPort = 587

            $smtp = new-object system.net.mail.smtpClient($smtpServer,$smtpPort)
            $smtp.EnableSsl = $true
            $smtp.credentials = $emailCreds

            $summaryMsg = new-object System.Net.Mail.MailMessage
            $summaryMsg.from = $fromEMail
            $summaryMsg.to.add($toEmail)
            $summaryMsg.subject=$subject
            $summaryMsg.IsBodyHtml = $true

            #Load Top Senders
            $topSenders = Import-Excel -Path $senderFile
            $objTopSenderTable = $topSenders|ConvertTo-Html

            $topRecipients = import-excel -path $recipientFile
            $objTopRecipientsTable = $topRecipients|ConvertTo-Html
            
            $body = "
            <html>
             <style>
             TABLE {border: 1px solid black; border-collapse: collapse;}
             TH    {border: 1px solid black; background:#dddddd; padding: 5px;font-weight:bold}
             TD    {border: 1px solid black; padding: 5px;}
             </style>
             <body>
              Hi All,<br/>
              <br/>
              Here's a copy of the Office 365 Mail Traffic Report for the week of $start to $end.<br /><br />
              <img src='cid:$summaryChartFile' /><br>
              <br />
              $objTopSenderTable <br/>
              <br/>
              $objTopRecipientsTable <br />
              <br />

              Thanks! <br />
              ~Brant

             </body>
            </html>  
            "

            $summaryMsg.body = $body

            $attachment = new-object System.Net.Mail.attachment -ArgumentList $summaryChartFile
            $attachment.ContentDisposition.Inline=$true
            $attachment.ContentType.MediaType = "image/png"
            $attachment.ContentId = $summaryChartFile

            $summaryMsg.attachments.add($attachment)

            $smtp.send($summaryMsg)

            $attachment.dispose()
            $summaryMsg.dispose()
            $smtp.dispose()

            write-host "$subject has been sent to $toEmail."

        }
    } #End doSummary

    if ($doDLP -or $doAll) {
        #DLP Policy Matches Report Weekly $start to $end
        write-host -foregroundcolor "Yellow" "Generating DLP Policy Hits Report..."
        $dlpData=get-mailtrafficpolicyreport -start $start -end $end -AggregateBy Day -Action SetAuditSeverityLow,SetAuditSeverityHigh -Direction Outbound -EventType DLPRuleHits|sort Date|select *
        $objDLPReport=new-object system.collections.ArrayList
        $objDLPTable=""

        $dlpDates=$dlpData.Date|unique

        foreach ($dlpDate in $dlpDates) {
            $itemDate = get-date -format "M/d/yyyy" $dlpDate
            $dlpItems=$dlpData|where {(get-date -format "M/d/yyyy" $_.Date) -eq $itemDate}
            $tmpDLP = [pscustomobject] @{
                Date=$itemDate
                SeverityLow=0
                SeverityHigh=0
            }
            foreach ($dlpItem in $dlpItems) {
                #write-host $dlpItem.Date - $dlpItem.Action

                if ($dlpItem.Action -eq 'SetAuditSeverityLow') { $tmpDLP.SeverityLow=$tmpDLP.SeverityLow + $dlpItem.MessageCount}
                if ($dlpItem.Action -eq 'SetAuditSeverityHigh') { $tmpDLP.SeverityHigh=$tmpDLP.SeverityHigh + $dlpItem.MessageCount}                
            } #end foreach $dlpItem in $dlpData

            $x=$objDLPReport.add($tmpDLP)
        } #end foreach $dlpDate in $dlpDates

        $tmpReport=$objDLPReport|select @{label="Date";expr="Date"},@{label="Severity Low";expr="SeverityLow"},@{label="Severity High";expr="SeverityHigh"}
        $objDLPReport.clear()
        $objDLPReport=$tmpReport

        $dlpFile="DLPPolicyMatches_$reportSuffix"
        $dlpHeader="Severity Low","Severity High"
        $dlpSeries=$dlpHeader -Replace(" ","_")

        
        $dlpChart=new-excelchart -Title "DLP Policy Matches Report Weekly`n $start to $end" -ChartType Line -xRange "Date" -YRange $dlpSeries -SeriesHeader $dlpHeader -width 700 -height 400

        $objDLPReport|Export-Excel -BoldTopRow -ExcelChartDefinition $dlpChart $dlpFile -AutoNameRange -AutoSize
        #$objDLPReport|Out-GridView
        
        #DLP Email
        if ($doEmail) {
            #Only need to extract the charts if we are sending the email.
            write-host "Extracting charts..."
            $dlpChartFile="$pwd\dlpchart_$chartSuffix"
            sleep -seconds 5 #This gives Export-Excel time to wrap up.
            export-excelchart -xlFile "$pwd\$dlpFile" -destFile $dlpChartFile #no extension needed
    

            $dlpChartFile="$pwd\dlpchart_$($chartSuffix)_001.png"
            write-host "Sending DLP Policy Report"
            write-host "Attached $($dlpChartFile)"
            $subject="Office 365 DLP Policy Matches Report Week of $start"
            #$smtpServer = "smtp.office365.com"
            #$smtpPort = 587
            $smtp = new-object System.Net.Mail.SmtpClient($smtpServer,$smtpPort)
            $smtp.EnableSsl=$true
            $smtp.Credentials = $emailCreds

            $dlpMessage=new-object System.Net.Mail.MailMessage
            $dlpMessage.from = $fromEMail
            $dlpMessage.to.add($toEmail)
            $dlpMessage.subject = $subject
            $dlpMessage.IsBodyHtml = $true

            $objDLPTable = $objDLPReport|ConvertTo-Html


            $body = "
            <html>
             <style>
             TABLE {border: 1px solid black; border-collapse: collapse;}
             TH    {border: 1px solid black; background:#dddddd; padding: 5px;font-weight:bold}
             TD    {border: 1px solid black; padding: 5px;}
             </style>
             <body>
              Hi All,<br/>
              <br/>
              Here's a copy of the DLP Policy Matches Report for the week of $start to $end.<br /><br />
              <img src='cid:$dlpChartFile' /><br>
              <br />
              $objDLPTable <br/>
              <br/>

              Thanks! <br />
              ~Brant

             </body>
            </html>
            "
            $dlpMessage.body = $body


            $attachment = new-object System.Net.Mail.Attachment -ArgumentList $dlpChartFile
            $attachment.ContentDisposition.Inline=$true
            $attachment.ContentType.MediaType = "image/png"
            $attachment.ContentId = $dlpChartFile
            $dlpMessage.Attachments.add($attachment)

            $smtp.send($dlpMessage)
            
            $attachment.dispose()
            $dlpMessage.dispose()
            $smtp.dispose()

            #Send-MailMessage -BodyAsHtml -From $fromEmail -To $toEmail -Subject $subject -Body $subject -smtpServer "smtp.office365.com" -credential $creds -UseSsl -Port 587 -Attachments $attachment
        }


    } #end $doDLP


    if ($doSPAM -eq $true -or $doALl -eq $true) {

        write-host -foregroundcolor "Yellow" "Generating Spam Hits Report..."

        $spamData=Get-MailTrafficReport -start $start -end $end -EventType SpamContentFiltered,SpamIPBlock
        $objSPAMReport=new-object System.Collections.ArrayList
        $spamDates=$spamData.Date|unique
        foreach ($spamDate in $spamDates) {
            $itemDate = get-date -format "M/d/yyyy" $spamDate
            $spamItems=$spamData|where {(get-date -format "M/d/yyyy" $_.Date) -eq $itemDate}
            $tmpSpam = [pscustomobject] @{
                Date = $itemDate
                SpamContentFiltered = 0
                SpamIPBlock = 0

            }

            foreach ($spamItem in $spamItems) {
                if ($spamItem.EventType -eq 'SpamContentFiltered') {$tmpSpam.SpamContentFiltered = $tmpSpam.SpamContentFiltered + $spamItem.MessageCount}
                if ($spamItem.EventType -eq 'SpamIPBlock') {$tmpSpam.SpamIPBlock = $tmpSpam.SpamIPBlock + $spamItem.MessageCount}
            } #end foreach $spamitem

            $x=$objSPAMReport.Add($tmpSpam)
        }#end foreach $spamDate

        $spamFile="MailTrafficSpam_$reportSuffix"
        $spamSeries="SpamContentFiltered","SpamIPBlock"
        $spamHeaders = "Content Filtered","IP Blocked"

        #$objSPAMReport|Out-GridView

        $spamChart = new-excelchart -Title "Spam Detections Report Weekly`n$start to $end" -ChartType Line -xRange "Date" -yRange $spamSeries -SeriesHeader $spamHeaders -width 700 -height 400
        $objSPAMReport|export-excel -BoldTopRow -ExcelChartDefinition $spamChart $spamFile -AutoNameRange -AutoSize
        $objSpamTable = $objSpamReport|ConvertTo-Html

        if ($doEmail -eq $true) {
            write-host "Sending Spam Hits Report..."
            $spamChartFile = "spamChart_$chartSuffix"
            sleep -seconds 5 #give Export-excel a chance to wrap up.
            
            export-excelchart "$pwd\$spamFile" -destFile "$pwd\$spamChartFile"

            $spamChartFile = "$($spamChartFile)_001.png"

            write-host "Sending Spam Traffic Report"
            $subject = "Office 365 Spam Traffic Report Week of $start"

            #$smtpServer = "smtp.office365.com"
            #$smtpPort = 587
            $smtp = new-object System.Net.Mail.SmtpClient($smtpServer,$smtpPort)
            $smtp.EnableSsl = $true
            $smtp.Credentials = $emailCreds

            $spamMessage = new-object System.Net.Mail.MailMessage
            $spamMessage.from=$fromEMail
            $spamMessage.to.add($toEMail)
            $spamMessage.Subject = $subject
            $spamMessage.IsBodyHtml = $true
            $body = "
            <html>
             <style>
             TABLE {border: 1px solid black; border-collapse: collapse;}
             TH    {border: 1px solid black; background:#dddddd; padding: 5px;font-weight:bold}
             TD    {border: 1px solid black; padding: 5px;}
             </style>
             <body>
              Hi All,<br/>
              <br/>
              Here's a copy of the Office 365 Spam Traffic Report for the week of $start to $end.<br /><br />
              <img src='cid:$spamChartFile' /><br>
              <br />
              $objSpamTable <br/>
              <br/>

              Thanks! <br />
              ~Brant

             </body>
            </html>
            "

            $spamMessage.body = $body

            $attachment = new-object System.Net.Mail.Attachment -ArgumentList "$pwd\$spamChartFile"
            $attachment.ContentDisposition.Inline=$true
            $attachment.contenttype.MediaType="image/png"
            $attachment.contentId = $spamChartFile
            $spamMessage.attachments.add($attachment)

            $smtp.send($spamMessage)

            $attachment.dispose()
            $spamMessage.dispose()
            $smtp.dispose()

        }
    }
} #End if isAuthenticated


############## END MAIN PROGRAM ###################################