Param(
    [Parameter(Mandatory=$false) ] $o365Cred=$null,
    [Parameter(Mandatory=$true) ] $o365User
)

$hasModule = try {get-command connect-msolservice} catch {$hasModule=$false}
$isConnected = try {get-command get-mailbox } catch {$isConnected -eq $false}

if (!$hasModule) {
    Write-host -foregroundcolor "Red" "Office 365 PowerShell Modules not installed.  Cannot continue."   
}

if (!$isConnected) {
    write-host -foreground Yellow "Please enter your Office 365 Admin Credentials..."
    $username=read-host "Username: "
    $password=read-host "Password: " -AsSecureString
    $o365Cred = New-Object System.Management.Automation.PSCredential -ArgumentList $username,$password
    write-host "Authenticating to O365..."
    $global:Session = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri https://ps.outlook.com/powershell/ -Credential $userCred -Authentication Basic -AllowRedirection
    Import-PSSession -AllowClobber $global:Session
}

$error.clear()
write-host -ForegroundColor Green "Opening Mailbox for $o365User"
try {
    $mailbox=get-mailbox $o365User
} catch {
    write-host "There was an error opening the mailbox for $o365User" -foregroundcolor Red
    $error[0]
    exit -1
}
write-host -ForegroundColor Green "Loading rules for $o365User"
$rules=get-inboxrule -mailbox $mailbox.alias

$rules|select *|Out-GridView -title "Rules for $o365User"

