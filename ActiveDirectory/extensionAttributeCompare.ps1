<#
<#

.SYNOPSIS
Exports AD Comparison files to CSV files.


.DESCRIPTION
   Date UpdatedBy Details
   01/15/2019 BW  Added creds as a parameter.
   03/30/2018 BW  Added $adServer as a parameter.
   01/22/2018 BW  Initial Coding.


#>
#>

param(
[Parameter(Mandatory=$true)] [string] $adGroup,
[Parameter(Mandatory=$true)] [string] $adAttribute,
[Parameter(Mandatory=$true)] [string] $adAttributeValue,
[Parameter(Mandatory=$true)] [string] $adServer,
[Parameter(Mandatory=$false)] [switch] $noTimeStamp,
[Parameter(Mandatory=$true)] [pscredential] $creds=$null,
[Parameter(Mandatory=$false)] [switch] $StudentWorkers
)

$ignoreStudentWorkers=!$StudentWorkers



$adUserList = new-object System.Collections.ArrayList
$startDate=[datetime]::Now

if (!$creds) {
write-host -foreground Yellow "Please enter your Active Directory Admin Credentials..."24
 $username=read-host "Username: "
 $password=read-host "Password: " -AsSecureString
 $creds = New-Object System.Management.Automation.PSCredential -ArgumentList $username,$password
}


### Loading and parsing specified AD Group ###
write-host "Loading AD Group: $adGroup"
if ($removeStudentWorkers -eq $true) {write-host -foregroundcolor yellow "Ignoring student workers."}

$staffGroup=get-adgroupmember $adGroup -server $adServer -Credential $creds

write-host "Parsing Group $adGroup"
$pass=0
foreach ($user in $staffGroup) {
    $pass++
    $percentComplete=$pass/$staffGroup.count * 100
    $adUser=get-aduser -server "$adServer" -credential $creds $user -properties Enabled,UserPrincipalName
    #write-progress -activity "Parsing user $($adUser.userprincipalname)" -percentComplete $percentComplete
    if ($Host.name -ne 'ConsoleHost' -and $pass% 100 -eq 0) {
        write-host -NoNewline "Percent Completed: $percentComplete `r"
    }  #else { write-inlineprogress -activity "Parsing user $($adUser.userprincipalname)" -percentComplete $percentComplete }
    if ($ignoreStudentWorkers -eq $true -and $adUser.UserPrincipalName -NotLike "*_*") {
        if ($aduser.Enabled -eq $true) {
            $x=$adUserList.add($adUser)
        }
    }
}
#write-inlineprogress -Completed

write-host "Exporting AD Group to adGroup-$adGroup.txt"
write-host "$($adUserList.Count) records to export."
$adUserList|sort|select UserPrincipalName > "adGroup-$adGroup.txt"


### END AD GROUP ###

###Pull from Specified Extension and look for the given value.
write-host  "Pulling down AD Users with the $adAttribute = '$adAttributeValue' ...(this may take a while)."

get-aduser -server $adServer -Credential $creds -filter "$adAttribute -eq '$adAttributeValue' -and Enabled -eq 'True'" -Properties Enabled,UserPrincipalName| sort | select UserPrincipalName > "adAttribute-$adAttribute.txt"

