﻿<#

.SYNOPSIS
Exports AD Group to CSV File


.DESCRIPTION
   Date UpdatedBy Details
   01/15/2019 BW  Added creds as a parameter.
   03/30/2018 BW  Added $adServer as a parameter.
   01/22/2018 BW  Initial Coding.


#>

param(
[Parameter(Mandatory=$true)] [string[]] $adGroup,
[Parameter(Mandatory=$true)] [string] $adServer,
[Parameter(Mandatory=$false)] [switch] $noTimeStamp,
[Parameter(Mandatory=$true)] [pscredential] $creds=$null
)

$startDate=[datetime]::Now

if (!$creds) {
write-host -foreground Yellow "Please enter your Active Directory Admin Credentials..."
 $username=read-host "Username: "
 $password=read-host "Password: " -AsSecureString
 $creds = New-Object System.Management.Automation.PSCredential -ArgumentList $username,$password
}

echo "Started at $startDate"

foreach ($group in $adGroup) {

 if ($noTimeStamp -eq $false) {
   $fileTime="$(Get-Date -format MM-dd-yyyy_HHmmss)" 
   $csvFile="$group-$fileTime.csv"
 } else {
    $csvFile="$($group).csv"
 }

 write-host -ForegroundColor Yellow "Loading Group $group"
 $members=get-adgroupmember -server $adServer -Credential $creds $group -Recursive

 write-host -ForegroundColor Yellow "Exporting $group to CSV $csvFile"
 $members|export-csv -Path $csvFile -NoTypeInformation

}