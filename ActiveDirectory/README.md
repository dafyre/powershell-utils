Active Directory Utilities
===

## DetailedUserInfo.ps1



This script pulls some user details from AD.
```-search```: Search for this.  Can be an array.

```-searchBy```: What do we want to search on?  Defaults to UserPrincipalName

```-domain```: What domain are we searaching?  Defaults to "mydomain.local"

```-displayWindow```: Display the output in an Out-Gridview.

```-save```: Save the output.

```-outputFile```: Where do we save the details?  Defaults to detailedinfo.csv.

```-adCred```: AD Credentials.  Can be specified with get-credential.  You get prompted otherwise.

## export-ADGroup.ps1


Exports the group members from the specified ADGroup into a CSV File with the group's name and a time stamp.

```-adGroup```: An Array of groups to export.

```-adServer```: Domain we are looking in.

```-noTimeStamp```: If specified, no time stamp is appended to the output file.

```-creds```: Active Directory Admin Credentials.

## import-ADGroup.ps1 


Imports a list of users from a CSV File into the AD Group specified.  See the script for details on the CSV File format.

## install-RSATModule.ps1


Installs the RSAT tools for Windows 10.

*has a good example of how to 'sudo' in PowerShell.

## NewUsersFromCSV.ps1 & NewUsersFromCSVGUI.ps1


Two examples of how to import users from CSV into AD.

