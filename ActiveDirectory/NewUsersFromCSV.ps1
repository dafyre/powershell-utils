﻿
#####################################################################################
#Create Student Worker AD accounts from a master csv
#
#IMPORTANT:  	SAVE THE IMPORT FILE AS A CSV FOR USE BY THE SCRIPT (and it keeps the
#		template with formulas intact for reuse)
#
#The only 5 fields to manually enter are:
#
#	GivenName
#	Surname
#	SWDept
#   employeeID (OneUSG Number)
#	employeeNumber (GCID)
#	otherPager (Birthdate)
#   termDate (Termination Date)
#
#All other fields are computed in the script.  
#########################################################################

$debug=$false

$userOU="My_OU"
$userDomain="mydomain.local"
$userDom="mydomain"
$domExt="local"


if (!$psISE) {
 write-host -foreground Yellow "Please enter your Active Directory Admin Credentials..."
 $username=read-host "Username: "
 $password=read-host "Password: " -AsSecureString
 $UserCredential = New-Object System.Management.Automation.PSCredential -ArgumentList $username,$password
} else {
 $UserCredential=Get-Credential

}

$userfile = (Read-Host "Enter name of file with accounts to be created")
#$acctexpire = (Read-Host "Enter account expiration date [ex:May 14, 2016]")

$users=Import-Csv $userfile

#Perform multiple actions on the data imported

foreach ($user in $users) {
	$Displayname = $user.GivenName + " " + $user.Surname
	$SAM = $user.GivenName.ToLower() + "_" + $user.Surname.ToLower()
	$UPN = $SAM + "@" + $userDomain
	$OU = "OU=$userOU,DC=$userDom,DC=$domExt"
	$Description = "Student Worker for " + $user.SWDept 
	$Passwd = "my first password is really strong"
    $acctexpire=$user.termDate

    write-host "Adding $Displayname ($UPN) to Active Directory."
    New-ADUser -Credential $UserCredential -Server $userDomain -Name "$Displayname" -SamAccountName $SAM -GivenName $user.GivenName -Surname $user.Surname -DisplayName "$DisplayName" -UserPrincipalName $UPN -AccountPassword (ConvertTo-SecureString $Passwd -AsPlainText -Force) -Enabled $true -Path "$OU" -Description "$Description" -AccountExpirationDate "$acctexpire"
    Set-ADUser -Credential $UserCredential -Server $userDomain -identity $SAM -Replace @{employeeID=$user.employeeID;employeeNumber=$user.employeeNumber;otherPager=$user.otherPager;telephoneNumber=$UPN}
     
    if ($debug -eq $true) {write-host "Adding $UPN to Student Workers group..."}
    Add-ADGroupMember -Credential $UserCredential -Server $userDomain -Identity "student_workers" -Members $SAM
}

