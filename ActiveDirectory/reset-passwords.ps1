Param(
    [Parameter(Mandatory = $false) ] [pscredential] $adCred,
    [Parameter(Mandatory = $false) ] [string] $adServer,
    [Parameter(Mandatory = $false) ] [string] $resetFile,
    [Parameter(Mandatory = $false) ] [string] $newPass="I hate computers!"

)


<# UTILITY FUNCTIONS #>
function New-OpenFileDialog { 
    param( 
        [string]$Title, 
        [string]$Directory, 
        [string]$Filter = "CSV and TXT Files (*.csv,*.txt)|*.csv;*.txt" 
    ) 
    [System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms") | Out-Null 
    $objForm = New-Object System.Windows.Forms.OpenFileDialog 
    $objForm.Title = $Title 
    $objForm.InitialDirectory = $Directory 
    $objForm.Filter = $Filter 
    $objForm.ShowHelp = $True 
         
    $retVal = $objForm.ShowDialog() 
         
    If ($retVal -eq "OK") { 
        Return $objForm.FileName 
    } 
    Else { 
        return $false
    } 
} 
<# END UTILITY FUNCTIONS #>

if ([string]::isnullorempty($adCred)) {
    write-host -foreground Yellow "Please enter your Active Directory Credentials..."
    $username = read-host "Username"
    $password = read-host "Password" -AsSecureString
    $adCred = New-Object System.Management.Automation.PSCredential -ArgumentList $username, $password
    $password=""

}

if ([string]::isnullorempty($adServer)) {
    write-host -foreground Yellow "Which Active Directory Server are you connecting to?"
    $adServer=read-host "AD Server"
}

if ([string]::isnullorempty($resetFile)) {
    $resetFile=New-OpenFileDialog -Title "Choose your Reset File" -Directory $PWD
    if ($resetFile -eq $false) {
        write-host -foregroundcolor RED "You MUST choose a file that contains a list of usernames!"
        exit 1
    }
}

write-host "Loading $resetFile"

$resetList=get-content $resetFile
2
foreach ($user in $resetList) {
    $SAM=$user -split ("@") #We can use emails or SAMACCOUNTNAMES
    $SAM=$SAM[0]
    write-host "Resetting password for $SAM"

    try {
        set-adaccountPassword -Credential $adCredential -server $adServer -identity $SAM -NewPassword (ConvertTo-SecureString $newPass -asPlaintext -force)   
        
    } catch {
        write-host -foregroundcolor red "Could not reset password for $SAM!"
        write-host -foregroundcolor red $error[0]
        write-host "Press the enter key to run for the next user"
        Read-Host
    }
}