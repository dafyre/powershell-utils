﻿<#

.SYNOPSIS
Return Detailed User information


.DESCRIPTION
   Date UpdatedBy Details
   10/22/2019 BW  Adjusted code to show all properties when using -displayWindow
   05/08/2019 BW  Fixed -displayWindow not displaying actual results.
                  Added -compareLike to allow for searching fields using the Like operator.
   02/07/2018 BW  Initial Coding



#>

Param(
[Parameter(Mandatory=$true) ] [string[]] $search="",
[Parameter(Mandatory=$true) ] [string] $searchBy="UserPrincipalName",
[Parameter(Mandatory=$false) ] [switch] $compareLike,
[Parameter(Mandatory=$true) ] [string] $domain="mydomain.local",
[Parameter(Mandatory=$false) ] [switch] $save,
[Parameter(Mandatory=$false) ] [switch] $displayWindow,
[Parameter(Mandatory=$false) ] [string] $outputFile = "detailedinfo.csv",
[Parameter(Mandatory=$false) ] $adCred = $null

)


$debug=$true
$pass=1

write-host "AD Cred says: $($adCred.Username)"
$isEmpty=[string]::isnullorempty($adCred.username)
write-host "Is empty says...  $isEmpty"

if ($isEmpty -eq $true) {
 write-host -foreground Yellow "Please enter your Active Directory Admin Credentials..."
 $username=read-host "Username: "
 $password=read-host "Password: " -AsSecureString
 $adCred = New-Object System.Management.Automation.PSCredential -ArgumentList $username,$password
}

<#
Properties we want:
DisplayName
UserPrincipalName
SamAccountName
Created
AccountExpirationDate
Department
EmployeeID
EmployeeNumber
Enabled
LastLogonDate
LockedOut
Modified
OfficePhone
PasswordLastSet
PasswordNeverExpires

#>
$userList=new-object System.Collections.ArrayList
$searchCount=$search.count
write-host "Found $searchCount $searchBy(s) to check."
write-host "Searching in $domain ..."
$pass=0;
foreach ($searchString in $search) {

   $pass++
   write-progress -Activity "Checking $searchString ..." -PercentComplete (($pass/$searchCount)*100)

   #Update Fields after the SELECT statement to get more or less information!
   if (!$compareLike) { $user=get-aduser -server $domain -credential $adCred -Filter "$searchBy -eq '$searchString'" -Properties * |select DisplayName,UserPrincipalName,SamAccountName,Created,AccountExpirationDate,Description,Department,EmployeeID,EmployeeNumber,Enabled,LastLogonDate,LockedOut,Modified,OfficePhone,PasswordLastSet,PasswordNeverExpires,OtherPager,OtherFacsimileTelephoneNumber }
   else { $user=get-aduser -server $domain -credential $adCred -Filter "$searchBy -like '$searchString'" -Properties * }

   foreach ($item in $user) {
    $x=$userList.add($item)
   }
   
   #if ($pass -gt 10) { break }
}

#if ($displayWindow -eq $true) { $users|Out-GridView } else {$users|ft -AutoSize}

if ($displayWindow -eq $true) {
   $userList|select *|Out-GridView -Title "Details for $search"
} else {
   $userList | select DisplayName, UserPrincipalName, SamAccountName, Description, Created, AccountExpirationDate, Department, EmployeeID, EmployeeNumber, Enabled, LastLogonDate, LockedOut, Modified, OfficePhone, PasswordLastSet, PasswordNeverExpires,OtherPager, OtherFacsimileTelephoneNumber |ft -auto
}
   
if ($save -eq $true) { $userList|convertto-csv -NoTypeInformation|out-file -FilePath $outputFile }



