<#
Script designed to query AD for all PCs in an OU and list any servers not running as a 
builtin account.

#>

param(
    [Parameter(Mandatory=$false)] [string] $adServer,
    [Parameter(Mandatory=$false)] [pscredential] $Credential,
    [Parameter(Mandatory=$false)] [string[]] $ComputerList,
    [Parameter(Mandatory=$false)] [string] $SearchBase = "OU=Servers,DC=MYDOMAIN,DC=LOCAL",
    [Parameter(Mandatory=$false)] [switch] $includeDesktops,
    [Parameter(Mandatory=$false)] [switch] $allServices

)
$servers=$null


if ($ComputerList.Count -eq 0) {
    if ([string]::isnullorempty($adServer) ) {
        write-host "No AD Server given... Using Defaults"
        $servers=get-adcomputer -filter * -properties * -SearchBase $searchBase
    } else {
        $isEmpty=[string]::isnullorempty($Credential)
        if ($isEmpty -eq $true) {
            write-host -foreground Yellow "Please enter your Active Directory Admin Credentials..."
            $username=read-host "Username: "
            $password=read-host "Password: " -AsSecureString
            $Credential = New-Object System.Management.Automation.PSCredential -ArgumentList $username,$password
        }
        
        write-host "Credentials received."
        $servers=get-adcomputer -filter * -properties * -SearchBase $searchBase -server $adServer -credential $Credential

        if ($includeDesktops -eq $false) {
            #Includes only Windows Servers
            $servers=$servers|where {$_.Operatingsystem -like "*Server*"}
        }
    }

} else {
    $servers=new-object System.Collections.ArrayList
    foreach ($computer in $computerList) {
        $tmp=get-adcomputer -Credential $Credential -server $adServer $computer
        $x=$servers.add($tmp)
    }
}

$pass=0
$psercentComplete=0
$auditList=new-object System.Collections.arraylist
$x=0


foreach ($server in $servers ) {
    $serverName = $server.name
    $dnsName=$server.dnshostName
    $pass++
    $percentComplete = (($pass/$servers.count) * 100 )
    write-progress -activity "Checking $serverName" -PercentComplete $percentComplete

    $serviceList=""
    try {
        $serviceList=get-wmiobject Win32_Service -computerName $dnsName -credential $Credential|select *
    } catch {
        write-host -foregroundcolor "Red" "There was an error getting services from $serverName..."
    }
    #select SystemName,Name,StartName,StartMode,State
    if ($allServices -eq $false) {
        $serviceList=$serviceList|where {$_.StartName -ne "LocalSystem" -and $_.StartName -notlike "NT AUTHORITY*"}|select *
    }

    write-host "$serverName`: $($serviceList.count) services discovered..."

    if ($serviceList.count -gt 0) {
        foreach ($service in $serviceList) {
            $x=$auditList.add($service)
        }
        #$x=$auditList.add($serviceList)
    }
}

write-host "Found $($auditList.count) services to audit."


$auditList=$auditList|select SystemName,Name,StartName,StartMode,State|ConvertTo-Csv -NoTypeInformation
$auditlist|out-file -filepath "serviceaudit.csv"




