﻿<#

.SYNOPSIS
Perform Student Worker Bulk Uploads GUI


.DESCRIPTION
   Date UpdatedBy Details
   02/06/2018 BW  Now checks for Admin Credentials
   12/07/2017 BW  Starts input in $txtUserName
                  Now updates status bar when finished if the file has a blank line at the end
   11/14/2017 BW  Tested with multiple accounts. Updated to output when adding accounts.
   11/13/2017 BW  Now Adds users to AD.  Tested against single user scenarios, added progress bar.
   11/10/2017 BW  Added output of user creation commands that would be executed.
   11/08/2017 BW  Added listboxes detailing what accounts were created or skipped.  Reads in SWTemplate.txt file and checks to see if AD accounts exist.
   08/28/2017 BW  Initial Coding.


#>


#Import Poweshell modules
import-module ActiveDirectory

#Load Windows / .Net Libraries
Add-Type -AssemblyName PresentationCore,PresentationFramework
[System.Reflection.Assembly]::LoadWithPartialName('Microsoft.VisualBasic') | Out-Null
[System.Reflection.Assembly]::LoadWithPartialName('System.Windows.Forms') | out-null
[System.Reflection.Assembly]::LoadWithPartialName('System.ComponentModel') | out-null
[System.Reflection.Assembly]::LoadWithPartialName('System.Data') | out-null
[System.Reflection.Assembly]::LoadWithPartialName('System.Drawing')  | out-null

##################### GLOBAL Variables ############################
$debug=$true
$fontSize=10
$userFont=""
$logFile=""
$userDomain="mydomain.local"
$userDom="mydomain"
$domExt="local"
$userOU="MY_OU"

################### END GLOBAL VARIABLES ##########################



########## FORM EVENTS ACTIONS ###############

$ShowHelp={
 #Display Tooltips on various objects
 switch ($this.name) {
  "txtFileName" {
      $tip="Click here to choose which file you want to load."
   }
 }
 $frmToolTip.settooltip($this,$tip)
}

$txtFileName_Click={
  $prgStatus.visible=$true
  $prgStatus.value=0
  $logFile=""

  $adCred=""
  $lblStatus.text="Connecting to AD"

  $username=$txtAdminUser.Text
  $password=$txtAdminPass.text
  
  if ([string]::isNullOrEmpty($username) -or [string]::isNullOrEmpty($password)) {
   $lblStatus.text="Please enter your admin credentials..."

   return $false
  }
  
  $password=convertto-securestring $password -asplaintext -force
  $adCred=New-Object System.Management.Automation.PSCredential -ArgumentList $username,$password

  $lblStatus.text="Reading user list..."
  $myFile=new-openfiledialog("Load Student Worker File",$PWD)
  $txtFilename.text=$myFile
  
  if ($myFile -eq $false) {
   if ($debug -eq $true) {write-host -ForegroundColor Yellow "No file chosen to load!"}

   $lblStatus.text="No file chosen to load!"
   $txtFilename.text=""

   return $false
  }

  $logFile="$($myFile)_$(get-date -Format MM-dd-yyyy_Hmmss).log"

  if ($debug -eq $true) {write-host -ForegroundColor Yellow "Logging to $logFile"}


  if ($debug -eq $true) { write-host -ForegroundColor yellow "Loading data from file $myFile" }
  $newUsers=get-content -Path $myFile
  $recordCount=$newUsers.count-1

  write-host -ForegroundColor yellow "Found $recordCount user(s) to process..."
  $lblstatus.text="Found $recordCount user(s) to process"
  #There is always a 1-line header in the template file.  thus the reason for -gt 1.
  if ($newusers.count -gt 1) {
   $newUsers=$null
   $newUsers=import-csv $myFile
   
   $pass=0
   foreach ($user in $newUsers) {
    $pass++
    if ($debug -eq $true) { write-host -foregroundcolor yellow "Checking $pass of $recordCount" }
    $prgStatus.value=[int]$pass/$recordCount

    #Data comes from CSV File here
    $sn=$user.surname -replace '[^a-zA-Z-]',''
    $Displayname = $user.GivenName + " " + $user.Surname
	$SAM = $user.GivenName.ToLower() + "_" + $user.Surname.ToLower()
	$UPN = $SAM + "@" + $userDomain
	$OU = "OU=$userOU,DC=$userDom,DC=$domExt"
	$Description = "Student Worker for " + $user.SWDept 
	$Passwd = "Change@1st"
    $acctexpire=$user.termDate
    

    #Check for Existing AD Account with info provided.  This try/catch block will return false if the account does not exist.
    try {
     $userCheck=get-aduser -Properties * $sam -Credential $adCred -server "$userDomain"
    } catch [Microsoft.ActiveDirectory.Management.ADIdentityNotFoundException] {
       #User is NOT found.
       $userCheck=$false
    } catch [System.Security.Authentication.AuthenticationException] { 
       #Invalid username and Password given.
       $message="Please enter your Admin Credentials to use this tool."
       if ($debug -eq $true) { write-host -foregroundcolor red $message }
       $lblStatus.text=$message
       $lblStatus.forecolor=[System.Drawing.Color]::Red
       break
    } catch {
        #Kill script on other errors, such as authentication errors, etc.  This is NOT graceful right now.
        if ($debug -eq $true) { write-host -foregroundcolor red $_.Exception.message }
        $lblStatus.text=$_.Exception.Message
        $lblStatus.forecolor=[System.Drawing.Color]::Red
        break

    }

    
    if ($userCheck -ne $false) {
     if (($userCheck.employeeNumber -eq $user.employeeNumber) -or ($userCheck.otherPager -eq $user.otherPager) -or ($userCheck.empleeID -eq $user.employeeID)) {
         #User Exists
         $message="Cannot add $upn, account already exists."
         write-host -ForegroundColor yellow $message
         $lstExistingUsers.items.add($upn)
         $lblStatus.text=$message
     }
    }

    if ($userCheck -eq $false) { 
        write-host "Adding $Displayname ($UPN) to Active Directory."
        New-ADUser -Credential $adCred -Server $userDomain -Name "$Displayname" -SamAccountName $SAM -GivenName $user.GivenName -Surname $sn -DisplayName "$DisplayName" -UserPrincipalName $UPN -AccountPassword (ConvertTo-SecureString $Passwd -AsPlainText -Force) -Enabled $true -Path "$OU" -Description "$Description" -AccountExpirationDate "$acctexpire"
        Set-ADUser -Credential $adCred -Server $userDomain -identity $SAM -Replace @{employeeID=$user.employeeID;employeeNumber=$user.employeeNumber;otherPager=$user.otherPager;telephoneNumber=$UPN;extensionAttribute13='staff'}
        
        $lstNewUsers.items.add($upn) 
        $lblStatus.text="Added $UPN."
    }
    #$newValue=[int]$pass/$recordCount
    $prgStatus.value=[int]$pass/$recordCount*100
    #write-host "New value is $newValue"

   } #end foreach($user in $newUsers)
   $prgStatus.value=100
   $lblStatus.forecolor=[System.Drawing.Color]::Black
   $lblStatus.text="Done."

  } #end if ($newUsers.count -gt 1)
} #end $txtFileName_Click


########## END FORM EVENTS ACTIONS ###########



########### UTILITY FUNCTIONS ################

function New-OpenFileDialog  { 
    param( 
    [string]$Title, 
    [string]$Directory, 
    [string]$Filter="CSV and TXT Files (*.csv,*.txt)|*.csv;*.txt" 
    ) 
        [System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms") | Out-Null 
        $objForm = New-Object System.Windows.Forms.OpenFileDialog 
        $objForm.Title = $Title 
        $objForm.InitialDirectory = $Directory 
        $objForm.Filter = $Filter 
        $objForm.ShowHelp = $True 
         
        $retVal = $objForm.ShowDialog() 
         
        If ($retVal -eq "OK") 
        { 
            Return $objForm.FileName 
        } 
        Else 
        { 
            return $false
        } 
} 


###### END UTILITY FUNCTIONS ################





#### BEGIN PROGRAM CODE ###

$frmMain = new-object System.Windows.Forms.Form
$frmMain.text="New Student Worker GUI"
$frmMain.size=new-object System.Drawing.Size(1024,768)

$formTop=0
$formLeft=0

$frmToolTip=new-object System.Windows.Forms.ToolTip

### Credentials Frame ###
$gcCredFrame=new-object system.windows.forms.groupbox
$gcCredFrame.text="Admin Credentials:"
$gcCredFrame.top=$formTop+10
$gcCredFrame.left=$formLeft+10
$gcCredFrame.width=300
$gcCredFrame.height=150
$userFont=$gcCredFrame.font
$userBold=new-object system.drawing.font($userFont.fontFamily,$fontSize,[System.Drawing.FontStyle]::Bold)
$gcCredFrame.font=$userBold

$lblAdminUsername=new-object System.Windows.Forms.Label
$lblAdminUsername.text="Username: "
$lblAdminUsername.top=25
$lblAdminUsername.left=10
$lblAdminUsername.font=$gcCredFrame.font

$txtAdminUser=new-object System.Windows.Forms.TextBox
$txtAdminuser.name="txtAdminuser"
$txtAdminUser.top=$lblAdminUsername.Top
$txtAdminUser.left=$lblAdminUsername.left+$lblAdminUsername.width+ 10
$txtAdminuser.width=150

$lblAdminPassword=New-Object System.Windows.Forms.Label
$lblAdminPassword.text="Password: "
$lblAdminPassword.top=$lblAdminUsername.top+$lblAdminUsername.height+10
$lblAdminPassword.left=10
$lblAdminPassword.font=$userBold

$txtAdminPass = new-object System.Windows.Forms.TextBox
$txtAdminPass.name="txtAdminPass"
$txtAdminPass.top=$lblAdminPassword.Top
$txtadminPass.left=$lblAdminPassword.left+$lblAdminPassword.width+10
$txtAdminPass.width=150
$txtAdminPass.PasswordChar="x"

$lblFileName = new-object System.Windows.Forms.Label
$lblFilename.left=$gcCredFrame.left+$gcCredFrame.width+25
$lblFilename.top=$gcCredFrame.Top
$lblFilename.font=$userBold
$lblFilename.text="File To Read:"

$txtFileName = new-object System.Windows.Forms.TextBox
$txtFileName.left=$lblFileName.left+$lblfilename.width+15
$txtFilename.width=180
$txtFileName.text=""
$txtFileName.name="txtFileName"
$txtFilename.top=$lblFilename.Top
$txtFileName.add_MouseHover($ShowHelp)
$x=$txtFilename.add_click($txtFileName_Click)


$lblUserList=new-object System.Windows.Forms.Label
$lblUserlist.text="New Users, Accounts Created:"
$lblUserlist.top=$gcCredFrame.bottom+25
$lblUserlist.left=$gcCredFrame.Left
$lbluserList.font=$userBold
$lbluserlist.width=300

$lstNewUsers= new-object System.Windows.forms.listbox
$lstNewUsers.left=$gcCredFrame.Left
$lstNewUsers.top=$lblUserlist.bottom+15
$lstNewUsers.width=300

$lblExistingUsers=new-object System.Windows.Forms.Label
$lblExistingUsers.text="Existing Users not Processed:"
$lblExistingUsers.top=$lblUserList.top
$lblExistingUsers.left=$lblUserList.left+$lblUserList.width+125
$lblExistingUsers.font=$userBold
$lblExistingUsers.width=300

$lstExistingUsers= new-object System.Windows.forms.listbox
$lstExistingUsers.left=$lblExistingUsers.left
$lstExistingUsers.top=$lblExistingUsers.bottom+15
$lstExistingUsers.width=300

$lblStatus=new-object System.Windows.Forms.Label
$lblStatus.top=$frmMain.bottom - 75
$lblStatus.left=$gcCredFrame.Left
$lblStatus.font=$userBold
$lblStatus.width=$frmMain.width
$lblStatus.text="Ready."

$prgStatus = new-object system.windows.forms.progressbar
$prgStatus.top=$lblStatus.top-$prgstatus.height
$prgStatus.left=$lblStatus.Left
$prgStatus.width=$lblStatus.width
$prgStatus.value=0
$prgStatus.Style="Continuous"
$prgStatus.visible=$false

$x=$frmMain.controls.add($gcCredFrame)
$x=$gcCredFrame.controls.add($lblAdminUsername)
$x=$gcCredFrame.controls.add($txtAdminUser)
$x=$gcCredFrame.controls.add($lblAdminPassword)
$x=$gcCredFrame.controls.add($txtAdminPass)
$x=$frmMain.controls.add($lblFileName)
$x=$frmMain.controls.add($txtFileName)
$x=$frmMain.controls.add($lblUserList)
$x=$frmMain.controls.add($lstNewUsers)
$x=$frmMain.controls.add($lblExistingUsers)
$x=$frmMain.controls.add($lstExistingUsers)
$x=$frmMain.controls.add($lblStatus)
$x=$frmMain.controls.add($prgStatus)


$frmMain.showDialog()
$txtAdminUser.Focus()


#### END PROGRAM CODE   ###
