<#
.DESCRIPTION
get-osinfo returns the Windows OS Version for the servers listed.

This is best run from a Domain-joined machine.

.EXAMPLE 
  ./get-osinfo -servers SERVERA,SERVER_B,SERVERC.MYDOMAIN.NET

.NOTES
    Date        Updated By      Details
    05-06-2019      BW          Initial release.

#>
Param (
    [Parameter(Mandatory=$false) ] [string[]] $servers=("ServerA","ServerB","ServerN"),
    [Parameter(Mandatory=$false) ] [string] $logFile
)
#Get-CimInstance win32_operatingsystem -computername "SERVER-1.DOMAIN.local"|select PSComputerName,Caption,Version
$osinfo = new-object System.Collections.ArrayList


foreach ($server in $servers) {
write-host "Checking $server"
$tmpObject=Get-CimInstance win32_operatingsystem -computername $server|select PSComputerName,Caption,Version
$x=$osInfo.add($tmpObject)

}

if ([string]::isnullorempty($logFile) -eq $true) {
    $osinfo|ft -AutoSize
} else {
    $osInfo|convertto-csv -NoTypeInformation|out-file -filepath $logFile

}

