<#
Get-Log.ps1

Gets Given Windows Event Log from the specified servers.  Currently requires the computer running this script to be on the 
same domain unless WinRM is properly configured.

Defaults to showing the last 3 days of logs.

DATE        Updated By      Details
03/20/2019      BW          Updated Defaults for $logStartDate
03/15/2019      BW          Initial Coding.

#>

Param (
    [Parameter(Mandatory=$false) ] [string] $eventLogName='Microsoft-Windows-Backup',
    [Parameter(Mandatory=$true) ] [string[]] $servers,
    [Parameter(Mandatory=$false) ] [System.DateTime] $logStartDate=(Get-Date).adddays(-3),
    [Parameter(Mandatory=$false) ] [string] $logFile="logResults.csv"
)

$myEvents=$null

foreach ($server in $servers) {
    $scriptBlock= {param($pEventLogName,$pLogStartDate)
    $filter=@{
    LogName=$pEventLogName;
    StartTime=Get-Date -Year $plogStartDate.Year -Month $plogStartDate.Month -Day $plogStartDate.day -Hour $plogStartDate.hour -minute $plogStartDate.Minute

    }

    Get-WinEvent -FilterHashtable $filter|Select PSComputerName,LogName,TimeCreated,Message,ID,Level,Task,OpCode
    }
    write-host "Checking $server ..."
    $myEvents+=invoke-command -ComputerName $server $scriptBlock -ArgumentList $eventLogName,$logStartDate

}

$myEvents|export-csv -NoTypeInformation -path $logFile