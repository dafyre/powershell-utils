<#
 The WINRM bits need to be configured so that the server will accept the connections.  
 That bit needs to run as admin.  The script self elevates to run.

  The CSV file format should have headers and data to match:
  Name,OperatingSystem,IP
  
 Name = Display name in the file (doesn't have to be FQDN)
 OperatingSystem = Windows Linux Mac, need I say more?  This only works for windows systems, so... 
 IP = How do we contact said server?


 * Use ABSOLUTE path to CSVFile.
 * 

#>

Param(
    [Parameter(Mandatory = $true)] [string] $csvFile,
    [Parameter(Mandatory = $false)] [string] $saveFile,
    [Parameter(Mandatory = $false)] [PSCredential] $Credential
)


$user = [Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()
$isAdmin = $user.isInRole([Security.Principal.WindowsBuiltInRole] "Administrator")

$domainCheck = get-wmiobject win32_computersystem

if ($domainCheck.partofdomain -eq $false) {
    write-host -foregroundcolor yellow -backgroundcolor black "This PC must be on a Domain for this process to work."
    exit -1024
}

if ($isAdmin -eq $false) {
    write-host "Not an admin!"
    $paramInfo = ""
    if ([string]::isnullorempty($csvFile) -eq $false) {
        $paramInfo = "-csvFile `"$csvFile`""
    }

    if ([string]::isnullorempty($saveFile) -eq $false) {
        $paramInfo = "$paramInfo -saveFile $saveFile"
    }

    if ([string]::isnullorempty($Credential)) {
        $paramInfo="$paramInfo -Credential $Credential"
    }

    $CommandLine = "-File `"" + $MyInvocation.MyCommand.Definition + "`" $paramInfo"
    #Start-Process -FilePath Powershell.exe -Verb RunAs -ArgumentList $CommandLine
    Start-Process -FilePath Powershell.exe -Verb RunAs -ArgumentList $CommandLine
    exit 0
}


if ($isAdmin) {
    $serverList = import-csv -literalpath $csvFile
    $serverList = $serverList | where { [string]::isnullorempty($_.IP) -eq $false }
    write-host "Loaded $($serverList.count) servers to process."

    $code_block = {
        $objUpdates = new-object -com "Microsoft.Update.AutoUpdate"
        $objUpdates.results
    }

    if (!$Credential) {
        write-host -foreground Yellow "Please enter your AD Credentials..."
        $username = read-host "Username: "
        $password = read-host "Password: " -AsSecureString
        $Credential = New-Object System.Management.Automation.PSCredential -ArgumentList $username, $password
    }


    $oldWSMAN = get-item WSMan:\localhost\Client\TrustedHosts
    $ipList = $serverlist.ip -join ","

    #write-host $ipList

    set-item wsman:\localhost\client\Trustedhosts $ipList


    $windowsList = new-object system.collections.ArrayList

    foreach ($server in $serverList) {
        $tmpUpdate = ""
        $tmpUpdate = [pscustomObject] @{
            Name        = $server.name
            IP          = $server.ip
            LastUpdated = ""
            LastChecked = ""
        }

        write-host -nonewline "Checking $($server.Name)..."
        if ($server.OperatingSystem -match "Windows") {
            $objOutput = invoke-command -scriptBlock $code_block -computername $server.ip -credential $Credential
            write-host $objOutput.LastInstallationSuccessDate
            $tmpUpdate.lastchecked = $objOutput.LastSearchSuccessDate
            $tmpUpdate.lastUpdated = $objOutput.LastInstallationSuccessDate
            [void] $windowsList.add($tmpUpdate)
            #read-host "Pausing for effect..."
        }
        else { write-host -foregroundcolor yellow "Not Windows..." }
    }

    set-item WSMan:\localhost\Client\TrustedHosts $oldWSMAN -force -erroraction silentlycontinue


    if ([string]::isnullorempty($saveFile) -eq $false) {
        write-host "Saving file to $saveFile"
        $windowsList | export-csv -LiteralPath $saveFile -notypeinformation
    }

    $windowsList | out-gridview

}
read-host "Press any key to finish."
