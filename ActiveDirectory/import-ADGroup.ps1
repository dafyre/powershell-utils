<#

.SYNOPSIS
Imports CSV into AD Group

The CSV File must have two columns to process such as:  
       GroupName,UserName
                - OR -
        GroupName,FullName


GroupName is the name of the group we are adding the user to.
UserName is the  SamAccountName or UserPrincipalName of the User we are adding to the group.

**DEVELOPERS NOTES: 
The way we pull the headers out of the CSV Sorts them in Alphabetical Order:
GroupName,Fullname becomes FullName,Groupname... 
GroupName,UserName remains GroupName,UserName


.DESCRIPTION
    Date     UpdatedBy  Notes
    10/22/2019  BW      Improved Username & Password checking in Test-Auth.
                        Added File dialog, and changed "SamAccountName" to be "UserName" where appropriate
    09/03/2019  BW      Improved Prompting for Parameters, added Test-Auth function.
    2/04/2019   BW      Added support for FullName column name.  Searches AD by the DisplayName.
    1/28/2019   BW      Initial Coding.   

#>

param(
[Parameter(Mandatory=$false)] [string] $adServer,
[Parameter(Mandatory=$false)] [pscredential] $creds,
[Parameter(Mandatory=$false)] [string] $CSVFile
)

import-module ActiveDirectory

### UTILITY FUNCTIONS ###

function New-OpenFileDialog { 
    param( 
        [string]$Title, 
        [string]$Directory, 
        [string]$Filter = "CSV and TXT Files (*.csv,*.txt)|*.csv;*.txt" 
    ) 
    [System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms") | Out-Null 
    $objForm = New-Object System.Windows.Forms.OpenFileDialog 
    $objForm.Title = $Title 
    $objForm.InitialDirectory = $Directory 
    $objForm.Filter = $Filter 
    $objForm.ShowHelp = $True 
         
    $retVal = $objForm.ShowDialog() 
         
    If ($retVal -eq "OK") { 
        Return $objForm.FileName 
    } 
    Else { 
        return $false
    } 
} 

function Test-Auth ($adServer,$adCred) {
    $userName=$adCred.Username
    if ($userName -match "@") {
        $userName = $username -split ("@")
        $userName = $userName[0]

    } else {
        $userName=$username -split("\\")
        $userName=$userName[1]
    }

    try  {
        $checkAuth=get-adobject -server $adServer -credential $adCred -filter "SamAccountName -eq '$userName'"
        if ($checkAuth -ne $null) {$checkAuth=$true}

    } catch {
        $checkAuth = $false
    }
    return $checkAuth
}

###

$startTime=(Get-Date)

$errorForeground="Red"
$goodFile=$false
$messageForeground="Yellow"
$percentComplete=0

if ([string]::IsNullOrEmpty($adServer)) {
    $adServer=read-host -P "Active Directory Server"
}

if ($creds -eq $null) {
    write-host -foreground Yellow "Please enter your AD Admin Credentials..."
    $username=read-host "Username: "
    $password=read-host "Password: " -AsSecureString
    $creds = New-Object System.Management.Automation.PSCredential -ArgumentList $username,$password
    $password=""
}

$check=Test-Auth -adServer $adServer -adCred $creds

if (!$check) {
    write-host "Error Authenticating to $adServer!"
    exit 1
}

write-host -foregroundcolor yellow "Please make sure your CSV file is formatted with a header of GroupName,UserName"

if ([string]::IsNullOrEmpty($csvFile)) {
    $CSVFile=New-OpenFileDialog -Title "AD Group Import File"
    if ($CSVFile -eq $false) {
        write-host -foregroundcolor "RED" "Cannot continue without a file to import."
        exit 0
    }

    $isFile=Test-Path $CSVFile

    if (!$isFile -eq $true) {
        write-host -ForegroundColor $errorForeground "$CSVFile Could not be opened."
        $goodFile=$false
        exit 1
    }
}
write-host -foregroundColor $messageForeground "Loading file $CSVFile..."
$csvData=Import-csv -path $CSVFile
$csvHeaders=$csvData|get-member -memberType NoteProperty|foreach {$_.name}

#$csvHeaders
$headerCount=$csvHeaders.count
#$headerCount

if ($headerCount -ne 2) {
    write-host -ForegroundColor $errorForeground "Cannot identify the file: $CSVFile!"
    $goodFile=$false
    exit 2
}

if ($CSVHeaders[0] -eq "GroupName" -and $CSVHeaders[1] -eq "UserName") {
    write-host -ForegroundColor $messageForeground "Searching by UserName..."
    $goodFile=1
}elseif ($CSVHeaders[1] -eq "GroupName" -and $CSVHeaders[0] -eq "FullName"){
    #the $CSVHeaders are sorted in Alphabetical order!
    write-host -ForegroundColor $messageForeground "Searching by Full Name..."
    $goodFile=2
}else {
    write-host -ForegroundColor $errorForeground "Cannot identify the file: $CSVFile!"
    $goodFile=$false
    exit 3
} 

if ($goodFile -eq $false) {
    write-host -ForegroundColor $errorForeground "Please check the headers in your CSV File and make sure they match what is given in the comments above!"
    exit -3
}

$recordCount=$csvData.GroupName.count

write-host "Preparing to load $recordCount users..."

$pass=0
$adObject=$false
foreach ($record in $csvData) {
    $groupName=$record.GroupName
    $user=$record.SamAccountName
    switch ($goodFile) {
        "1" { $user=$record.UserName       }
        "2" { $user=$record.FullName       }
    }
    $pass++
    $percentComplete=(($pass/$recordCount)*100)
    write-progress -Activity "Importing Users..." -Status "Adding $user to $groupName..." -PercentComplete $percentComplete
    #sleep -Milliseconds 500 #introduce artificial delay
    #write-host -ForegroundColor $messageForeground "Adding $user to $groupName...($percentComplete%)"
    $error.clear()

    #write-host -foregroundcolor $messageForeground "Searching Active Directory for $user. "
    if ($user -like "*@*") {
        $adObject=get-adobject -server $adServer -credential $creds -Filter ("UserPrincipalName -eq '$user'") -Properties *
    } elseif($goodFile -eq 2) {
        
        $adObject=get-adObject -server $adServer -credential $creds -Filter ("DisplayName -eq '$user'") -Properties *
    }else {
        $adObject=Get-adobject -server $adServer -credential $creds -Filter ("SamAccountName -eq '$user'") -Properties *
    }

    try {
        if (!$adObject) {
            write-host -ForegroundColor $errorForeground "$user was not found in Active Directory!"  
        } else {
            $user= [string] $adObject.SamAccountName
            
            add-adgroupmember -Identity $groupName -Members $user -server $adServer -Credential $creds
        }
    } catch  {
        if ($error[0].Exception.Message -ne "The specified account name is already a member of the group") {
            write-host "$user is already a member of $groupName"
        }
        if ($error[0].Exception.Message -ne "The specified account name is already a member of the group") {
            write-host -foregroundcolor $errorForeground "-----------------------------------------------------------"
            write-host -foregroundcolor $errorForeground  "There was an error!"
            $error[0]
            write-host -foregroundcolor $errorForeground  "Press the enter key to continue, or CTRL+C to stop."
            write-host -foregroundcolor $errorForeground  "-----------------------------------------------------------"
            Read-Host
        }
    }
}

$endTime=(Get-Date)

$timeSpan=(New-TimeSpan $startTime $endTime).toString("hh'h:'mm'm:'ss's'")

write-host -ForegroundColor $messageForeground "$recordCount entries sucessfully impored."
write-host -ForegroundColor $messageForeground "Completed in $timeSpan."
