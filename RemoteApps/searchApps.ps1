﻿<#
 .SYNOPSIS
  This command searches the list of applications.  You can specify how to search, and whether or not you are searching for just an app or if you are searching for Permissions as well.

 .EXAMPLE
 ./searchApps -search *
   Will list all applications
 
 ./searchApps -search * -searchByFolder -SearchFolder "Microsoft Office"
   Will show you all of the applications in the Microsoft Office folder.
 
 ./searchApp -search iexplore -searchByAlias
   Will show you any applications that have an alias that is like *iexplore*

 ./searchApp -search "ChemDraw Ultra 6.0"
   Will show you only the ChemDraw Ultra 6.0 application.  **NOTE** Quotes MUST be used when searching by display name if the applications name has spaces! **
 
 ./searchApp -search * -searchByPermissions -searchPerm "Username","GroupName,"UserName","GroupName"...
   Will display where the searchPerms are granted or denied access to the applications that match the search criteria.  You can specify multiple users or AD groups with the searchPerm by separating them with comas.

  
 .DESCRIPTION
  Created By: Brant Wells
   Last Modified By: Brant Wells
   Last Update: 06/16/2016

  Update Details: 
   (06/16/2016)
    Displays in a Gridview window now.
   (11/06/2015)
    Corrected an issue with the -audit parameter preventing output to the screen. 
  
#>
param(
[Parameter(Mandatory=$false)] [string[]] $groups,
[Parameter(Mandatory=$false) ] [string] $search="*",
[Parameter(Mandatory=$false)] [switch] $searchByAlias,
[Parameter(Mandatory=$false)] [switch] $searchByFolder,
[Parameter(Mandatory=$false)] [string] $searchFolder,
[Parameter(Mandatory=$false)] [switch] $searchByPermissions,
[Parameter(Mandatory=$false)] [string[]] $searchPerm,
[Parameter(Mandatory=$false)] [switch] $useGrid

)

#Modules to Import and Variable Definitions
import-module remotedesktop #Imports Remote Desktop Powershell Module
$debug=$PSBoundParameters['Debug'] #Easy Debug Parameter.
$stopWatch=new-object System.Diagnostics.stopwatch
Add-Type -AssemblyName System.DirectoryServices.AccountManagement

#Utility Functions
function ExpandGroup($groupName,$recursive=$true) {
$dContext=[System.DirectoryServices.AccountManagement.ContextType]::Domain
$group=[System.DirectoryServices.AccountManagement.GroupPrincipal]::FindByIdentity($dContext,$groupName)

if ($group -ne $null) { 
 $members=$group.GetMembers($recursive) #$recursive 
} else {
 #group is null, simply return $null
 return $null

}
return $members

} #end function ExpandGroup


#Checks Permissions for the app.

function hasPermission ($app,$hPerm) {
 $groups=$app.usergroups
 $hPerm=$hPerm.tolower()
 $hGroup=isGroup($hPerm) #is the passed permission a group level permission or individual?  $false = individual
 $appName=$app.displayName
  if ($groups.length -lt 1) {
   #write-host "$appName has no permissions, defaults to granted!"
   #write-host "Group Says: $groups"
   return $true
  } #if there are no groups, then the app is available to everybody in the collection


 if ($hGroup -eq $true) { #this is a group, we don't need to expand or do anything fancy
  #write-host "Permission represented shows as a group."
  $match=$false
  $retval=$groups|foreach {
   $group=$_
   $group=$group.tolower()
   $group=$group.split("\") #
   $group=$group[1]         # these two lines strip out the domain name and the \
   if ($group -eq $hPerm) {$match =$true}
  }
  return $match
 }#end if $hGroup -eq $true


 if ($hGroup -eq $false){ #$hPerm is a user, check against the expanded groups.
  #write-host "Permission requested for $hPerm"
  $match=$false
  #write-host "Checking $groups"
  $retval=$groups|foreach{
   $group=$_
   $group=$group.tolower()
   $group=$group.split("\")
   $group=$group[1]
   $group=$group -replace "\."," "
   #write-host "Group is $group"
   if (isGroup($group) -eq $true) {$members=ExpandGroup($group)} else {$members=@{}; $members.samaccountname="$group"}

   #write-host "Members says: $members"
   if ($members.length -gt 0) {
    $retval=$members|foreach {
     $member=[string]$_.samaccountname
     $member=$member.tolower()
     
     if ($member.tostring() -eq $hPerm.tostring()) {$match=$true}
     #write-host "$member = $hPerm ($match)"
    }#end $members|foreach

   }#end if $members.length > 0

  } #end $groups foreach
  return $match
 } #end if $hGroup -eq $false
 
 return $false
}#end function hasPermission()

function isGroup ($iGroup) {

$dContext=[System.DirectoryServices.AccountManagement.ContextType]::Domain
$group=[System.DirectoryServices.AccountManagement.GroupPrincipal]::FindByIdentity($dContext,$iGroup)

if ($group -ne $null) {return $true} else {return $false}

}



##BEGINNING OF POWERSHELL SCRIPT

if ([string]::IsNullOrEmpty($search) -eq $false) {$search="*$search*"}
write-host "Searching applications..."

if ($searchByAlias -eq $false) {
 #Searching by App Display Name
   $apps=get-rdremoteapp|where {$_.displayName -like $search}
} else {
  #Searching By App Alias
  $apps=get-rdremoteapp|where {$_.alias -like $search}
}

if ($searchByFolder -eq $true) {

 $apps=$apps|where {$_.folderName -eq $searchFolder}

}

$appHeader=""
$fileData=""
$fileLine=""

if ($searchByPermissions -eq $true) {
 write-host "Sorting applications by Display Name..."
 $retval=$apps|sort-object -property DisplayName|foreach {
  $app=$_
  $appName=$app.displayName
  $appHeader += "$appName,"
  $fileLine=""
  $searchPerm|foreach {
    $perm=$_
    $fileLine="$perm," 
    $allowed=hasPermission -app $app -hPerm $perm
     
    if ($audit -eq $false) {
     if ($allowed -eq $true) {
      write-host $perm is -NoNewline; write-host -f green " GRANTED " -NoNewline; write-host access to $app.displayName
     } else {
      write-host $perm is -NoNewline; write-host -f RED " DENIED " -NoNewline; write-host access to $app.displayName
     }#end if $allowed -eq $true 
    }#end if ($audit -eq $false)
   }#end $searchPerm|foreach
 }#end $apps|foreach
 exit
}#end if $searchByPermissions -eq $true

$stopwatch.start()
write-host "Sorting applications..." -NoNewline
$apps=$apps|Sort-Object -Property DisplayName
$stopwatch.stop()
write-host $stopwatch.elapsed


$apps|Out-GridView
