﻿ <# 
 .SYNOPSIS
  This command caches the Users and Remote Apps from your RDSH Infrastructure into CSV files.

 .EXAMPLE
  ./rappCache -userCache -userCacheFile "myUsers.cache" -userCacheGroups "MYDOMAIN\Group1","MYDOMAIN\Group2",..."MYDOMAIN\GroupN"
   Will download the entire list of users and their group membershipsAD infrastructure. and saves it into "myUsers.cache"
   Fair Warning: This script can take a while to run on large AD Groups (34k Users caches in approximately 1hr)

 
 .DESCRIPTION
  DATE UpdatedBy Details
  03/03/2016 BW Sanitization for code sharing
  02/24/2016 BW Rewrote logic in group2file to write all items that pass the "inactive" check to the users.cache file.  sortCache() now handles removing the duplicates.
  02/22/2016 BW Minor adjustments to memory management.
  12/21/2015 BW The cache system will now only store users who are not in an OU that has the word Inactive in it.  (Case sensitivity has not been tested yet).
  12/07/2015 BW Improved performance slightly, bringing the caching process down to an hour and fifteen minutes for 35k users.
                Cleaned up and simplified code.
  12/02/2015 BW Minor tweaks to Garbage collection timing to improve caching times on large groups.
                Switched from text output to using write-progress bar for status updates.
  11/24/2015 BW Timed runs show that the caching process will run in about an hour and thirty minutes for 35k users depending on various factors
  11/20/2015 BW Test Runs complete
  11/18/2015 BW Initial Creation
 
#>

Param(
[Parameter(Mandatory=$false,ParameterSetName='appCache') ] [switch] $appCache,
[Parameter(Mandatory=$false,ParameterSetName='appCache') ] [string] $appCacheFile="apps.cache",
[Parameter(Mandatory=$false,ParameterSetName='appCache') ] [string] $connectionBroker="",
[Parameter(Mandatory=$false,ParameterSetName='sortCache') ] [switch] $sortOnly,
[Parameter(Mandatory=$true,ParameterSetName='sortCache') ] [string] $sortFile,
[Parameter(Mandatory=$false,ParameterSetName='UserCache') ] [switch] $userCache,
[Parameter(Mandatory=$false,ParameterSetName='UserCache') ] [string] $userCacheFile="users.cache",
[Parameter(Mandatory=$true,ParameterSetName='UserCache') ] [string[]] $userCacheGroups
)


#-------------function apps2File ----------------------------------------#
function apps2File ($append=$false,$outFile) {
 import-module remotedesktop #Imports Remote Desktop Powershell Module 
 $broker=$script:connectionBroker

 $masterGroup=new-object system.collections.arrayList
 $appTimer = new-object system.diagnostics.stopwatch
 $defaultDomain=$script:domain
 write-host "================================================================================"
 write-host "Loading Remote App Collections from $broker..."
 $collections=get-rdsessioncollection -connectionbroker $broker
 $retval=foreach($collection in $collections) {
  $collectionName=$collection.collectionName
  write-host -nonewline "$collectionName..."
  $config=get-rdsessioncollectionconfiguration -connectionbroker $broker -CollectionName $collectionName -UserGroup
  $retval=foreach ($group in $config.UserGroup) {
   if ($masterGroup.contains($group) -eq $false) {$masterGroup.add($group)}
  }#end foreach($group in $config.UserGroup
  write-host "processed...  (" $masterTimer.elapsed ")"
  write-host "================================================================================"
 }#end foreach($collection in $collections)

 $masterGroup=$masterGroup|sort|unique

 $pass=0
 $appTimer.start()
 write-host -nonewline "Loading Remote App List..."
 $apps=get-rdremoteapp -connectionbroker $broker|sort-object -property DisplayName
 write-host "(" $appTimer.Elapsed ")"

 $retval=foreach ($app in $apps) {
  $pass++
  $appName=$app.DisplayName

  write-host -NoNewline "Proessing $appName..."
  if ($app.usergroups.count -eq 0) {$groupList=$masterGroup -join ","} else {
   $groupList=$app.usergroups|sort|unique
   $groupList=$groupList -join ","
   
  }#end if ($app.usergroups.count -eq 0)
  $fileLine="$appName,$groupList"
  #$fileLine=$fileLine -replace("$script:domain\\","")
  
  $fileLine|out-file -append -filepath $outFile
  write-host "(" $appTimer.Elapsed ")"
  $appTimer.restart()
 }#end foreach($app in $apps)
 

}#end function apps2File
#-------------end function apps2File ------------------------------------#


#-------------function garbageCollection --------------------------------#
function garbageCollection($pause=0) {
for ($x=0;$x -lt 10;$x++) {
 [GC]::Collect()
}
sleep -Seconds $pause
} 
#----------end function garbageCollection --------------------------------#


#-------------function group2File --------------------------------#
function group2File ($groupName,$append=$false,$outFile="users.cache",$fullRun=$false) {
 $memberList=new-object system.collections.arrayList
 
 $timer=new-object System.Diagnostics.Stopwatch
 $timer.start()
 write-host "---------------------------------------------"
 write-host "Loading Group $groupName..."
 $group=string2group -strGroup $groupName
 
 $everybody=$group.GetMembers($true) #recursively get users from the group.
  #$memberCount=$everybody.count
 
 $pass=0
 
 $timeStamp=$timer.elapsed
 $retval=foreach($member in $everybody) {
  $pass++
  $line=""
  if ($pass % 100 -eq 0) {$timeStamp=$timer.elapsed}
  #$timeStamp=$timer.elapsed
  if ($pass -lt 100 -or $pass % 25 -eq 0) {write-host -nonewline "($timeStamp) Processing member $pass`r"}

  #write-host -nonewline "($timeStamp) Processing member $pass`r"
  $userName=$member.userprincipalname
  $userDN=$member.distinguishedName
  $userGroups=$member.getAuthorizationgroups()
  $userGroups=$userGroups.samaccountname -join ","
  $line="$userName,$userGroups"
  if (($userDN.contains("Inactive") -eq $false -and $userDN.contains("inactive") -eq $false) -or $fullRun -eq $true) {
   $line|out-file -Append -FilePath $outfile
  }
     
  #if ($pass % 10 -eq 0) {write-host $line}
  if ($pass % 1000 -eq 0) {garbageCollection -pause 1}
  #if ($pass % 1000 -eq 0) {break}  #uncomment this line and change 1000 to the number you want to stop at.

 } #end foreach ($member in $everybody)
#>
 write-host "`nParsed $pass members..."
 
 #$memberList=$memberList|sort|unique
 <#
 write-host "Saving file..."
 if ($append -eq $false) {
  $memberList|out-file -FilePath $outFile
 } else {
  #append=true, make sure we are set to use append
  $memberList|out-file -Append -FilePath $outFile

 } #end if ($append -eq $false)
 #>

 $everybody=$null
 $office=$null
 write-host "$groupName Done.  Completed in " $timer.Elapsed
 write-host "---------------------------------------------"
 garbageCollection -pause 3

}
#-------------end function group2File ----------------------------#


#-------------function sortCache --------------------------------#
function sortCache($sCache) {
 write-host "Loading $sCache..."
 $sContent=get-content($sCache)
 write-host "Sorting and removing duplicates..."
 $sContent=$sContent|sort|unique
 $sContent|out-file -FilePath $sCache
 write-host "Done!"
}

#-------------end function sortCache ----------------------------#


#-------------function string2User --------------------------------#
function string2User($strUser) { #This function converts  [string]$strUser to a DSAM.UserPrincipal object and returns that to where the function was called.
 $dContext=[System.DirectoryServices.AccountManagement.ContextType]::Domain
 $user=[System.DirectoryServices.AccountManagement.UserPrincipal]::FindByIdentity($dContext,$strUser)
 if ($user -ne $null) {return $user} #Returns a DSAM.UserPrincipal object.
 return $false
}
#-------------end function string2User ----------------------------#


#----------------function string2Group ----------------------------#
function string2Group([string] $strGroup) { #This function converts  [string]$strGroup to a DSAM.GroupPrincipal object and returns that to where the function was called.
 $dContext=[System.DirectoryServices.AccountManagement.ContextType]::Domain
 $domain=$script:domain
 if ($strGroup.contains("\") -eq $true) {
  $group=$strGroup.split("\")
  $domain=$group[0]
  $group=$group[1]
 }
 #write-host "Domain says $domain"
 $pContext=new-object System.DirectoryServices.AccountManagement.PrincipalContext($dContext,$domain)
 $group=[System.DirectoryServices.AccountManagement.GroupPrincipal]::FindByIdentity($pContext,$strGroup)

 #write-host "Group says $group"
 if ($group -ne $null) {return $group} #Returns a DSAM.GroupPrincipal object.
 return $false
}
#-------------end function string2Group----------------------------#



#-----------------------------Initial Module Imports, App Startup, etc... ------------------------#
Add-Type -AssemblyName System.DirectoryServices.AccountManagement #For prarsing users and groups without access to PowerShell AD modules.
import-module remotedesktop

$debug=$PSBoundParameters['Debug'] #Easy Debug Parameter.
$masterTimer=new-object System.Diagnostics.Stopwatch
$masterTimer.start()

if ([string]::IsNullOrEmpty($connectionBroker) -eq $true) {
write-host "Determining Conneciton Broker..."
$rdHAConfig=get-RDConnectionBrokerHighAvailability
$connectionBroker=$rdHAconfig.ActiveManagementServer
write-host "Connection Broker is: $connectionBroker"
}

if ($appCache -eq $true) {
 
 apps2File -append $true -outFile $appCacheFile
}


if ($sortOnly -eq $true) {
 sortCache -sCache $sortFile
}#end if $sortOnly -eq $true



#-----------------User Caching ------------------#
if ($userCache -eq $true) {
 $memberList=new-object system.collections.arrayList
 
 $retval=foreach($group in $userCacheGroups) {
  group2File -groupName $group -append $true -outFile $userCacheFile

  #Garbage collection after group2File finishes processing.
 }#End foreach ($group in $userCacheGroups)

 write-host "Sorting file..."
 sortCache -sCache $userCacheFile #sort, remove duplicates

}#end if $userCache -eq $true


write-host "Script finished in " $masterTimer.Elapsed #finally done!


<#
$memberList=new-object system.collections.arrayList

$timer=new-object System.Diagnostics.Stopwatch
$timer.start()
write-host "Loading Groups..."
$office=string2group -strGroup "RAPP_OFFICE2013"

$everybody=$office.GetMembers($true)
#$memberCount=$everybody.count

$pass=0

$retval=foreach($member in $everybody) {
 $pass++
 $line=""
 if ($pass % 100 -eq 0) {$timeStamp=$timer.elapsed}
 write-host -nonewline "($timeStamp) Processing member $pass`r"
 $userName=$member.userprincipalname
 $userGroups=$member.getAuthorizationgroups()
 $userGroups=$userGroups.samaccountname -join ","
 $line="$userName,$userGroups"
 $memberList.add($line)
 #if ($pass % 10 -eq 0) {write-host $line}
 if ($pass % 100 -eq 0) {garbageCollection -pause 0}
 #if ($pass % 1000 -eq 0) {break}  #uncomment this line and change 1000 to the number you want to stop at.
} #end foreach ($member in $everybody)
write-host "`nSorting $pass members..."
$memberList=$memberList|sort|unique
write-host "Saving file..."
$memberList|out-file -FilePath "test.users.cache"


write-host "Done.  Completed in " $timer.Elapsed
#>