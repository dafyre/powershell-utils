﻿ <# 
 .SYNOPSIS
  This command Displays the actively running executable files run by the specified user on the remote server.

 .EXAMPLE
  ./activeApps.ps1 -serverName SERVER -userName user.name -userDomain MYDOMAIN
    Connects to SERVER and displays all of the actively running applications for the user "user.name"

 .DESCRIPTION
  DATE UpdatedBy Details
  08/06/2016 BW Added Excluding apps when user is specified.
  04/29/2016 BW Added support for excluding apps from the apps list, such as rundll, rdpinit, taskhostex...
                These are things that run when anyone logs on.
  04/26/2016 BW Added support for searching for all users by default.
  04/22/2016 BW Added support for searching all RemoteApp servers by default.
  02/02/2016 BW Initial Creation

#>
Param(
[Parameter(Mandatory=$false) ] [string[]] $serverName=("SK_1","SK_2","SK_3","SK_4","SK_5","SK_6","SK_7","SK_8","RDGW"),
[Parameter(Mandatory=$false) ] [string] $userName,
[Parameter(Mandatory=$false) ] [string] $userDomain="MYDOMAIN"
)

$VerbosePreference="Continue" #Display Verbose Output
#$VerbosePreference="SilentlyContinue" #Do Not Display Verbose Output

$userDomain="*$userDomain*"
$apps=@()
$excludeApps=("rundll32.exe","rdpinit.exe","taskhostex.exe","rdpclip.exe",
"vmtoolsd.exe","kavtray.exe","conhost.exe","rdpshell.exe","AdobeARM.exe","TabTip.exe",
"TabTip32.exe", "rdpinput.exe")

#$apps=get-wmiobject -computername $serverName -query "Select * from Win32_Process"|select @{Label='Server';Expression={$_.PSComputerName}},Name,@{Label='Owner';Expression={$_.GetOwner().User}},@{Label='Domain';Expression={$_.GetOwner().Domain}}

foreach ($server in $serverName) {
 Write-Verbose "Checking running applications on $server"
 $temp=get-wmiobject -computername $server -query "Select * from Win32_Process"|select @{Label='Server';Expression={$_.PSComputerName}},Name,@{Label='Owner';Expression={$_.GetOwner().User}},@{Label='Domain';Expression={$_.GetOwner().Domain}},Path,CommandLine,Sessionid
 $apps+=$temp
}

#|where {$_.Domain -like $userDomain -and $_.Owner() -eq $userName}|out-gridview -Title "Running Applications on Remote Desktop Servers"

if ([string]::IsNullOrEmpty($userName)) {
 $apps|where {$_.Domain -like $userDomain -and $_.SessionId -ne 0 -and $excludeApps -notcontains $_.Name}|out-gridview -Title "All Running Applications on $serverName"
} else {
 $apps|where {$_.Domain -like $userDomain -and $_.Owner -eq $userName -and $excludeApps -notcontains $_.Name}|out-gridview -Title "$userName's Running Applications on $serverName"
}