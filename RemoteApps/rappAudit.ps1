﻿<# 
 .SYNOPSIS
  This command performs an Audit of all Remote Apps against an AD User group.

 .EXAMPLE
  ./rappAudit.ps1 -auditFile "staff_audit.csv" -groupName "staff"
  This will perform an audit of ALL Staff access to the RAPPs.
  The test run on 11/13/2015 took approximately 4 hours and 20 minutes to process all applications for every member of the Staff group.
  At the time of writing, it is not feasible to perform this against the student groups with an estimated 34k members!

  Inside of the Audit file,x indicates access is granted, and - indicates access is denied.  
  
  The Audit File has a format of:
  User,App Name 1, App Name 2, App Name 3
  user.student@student.mydomain.net,x,-,-
  staff.account@mydomain.net,x,x,x
  unreal.user@mydomain.net,-,-,-

  In this case, user.student would have access to only "App Name 1"; staff.account would have access to all of the apps, and unreal.user has access to none of the Apps.

 .EXAMPLE
  ./rappAudit.ps1 -useCache
  Performs an audit of all Apps and Users that have been cached.  Requires running rappCache first!

 .DESCRIPTION


  Date Updated By Notes
  04/21/2016 BW Correct issue introduced by code sanitization.  
  03/03/2016 BW Code sanitation for sharing with others
   
  03/02/2016 BW Corrected sliding scale counter.
  02/29/2016 BW Removed Live run from Audit.  It now requires cache files to be generated for use.  This cut audit time from an hour down to less than 10 minutes.
                Moved logic from loadUsers out to the main program logic.
                Remove loadUsers function.
  02/22/2016 BW Changed order of this change log.
                Memory optimization by reducing the number of calculations necessary for percentages and counters.
  12/21/2015 BW Removed group options from the Cached runs.  The system will run on the cache for ALL users.
  12/17/2015 BW Huge speed boost and code rewrite.
  12/02/2015 BW Read Cache File, ignore duplicate users using unique (around line 190, $userCache=$userCache|sort-object|unique #added |unique 12/02/2015,bw)
  11/24/2015 BW Updated description tags
  11/23/2015 BW Added a cached mode.  Allowed for a full run, takes around 24 hours for 34k users.  Spot check of test results appear accurate
  11/13/2015 BW Added Description, verified test results.
  11/11/2015 BW Initial Release
  
#>
Param(
[Parameter(Mandatory=$false) ] [string] $auditFile="audit.csv",
#[Parameter(Mandatory=$false,ParameterSetName='liveAudit') ] [string[]] $groupName,
[Parameter(Mandatory=$false,ParameterSetName='cacheinfo')] [switch] $useCache,
[Parameter(Mandatory=$false,ParameterSetName='cacheinfo')] [string] $appCacheFile="apps.cache",
[Parameter(Mandatory=$false,ParameterSetName='cacheinfo')] [string] $userCacheFile="users.cache"

)

import-module remotedesktop #Imports Remote Desktop Powershell Module
$debug=$PSBoundParameters['Debug'] #Easy Debug Parameter.

$masterTimer=new-object System.Diagnostics.stopwatch


Add-Type -AssemblyName System.DirectoryServices.AccountManagement #For prarsing users and groups

$loadedGroups=new-object System.Collections.ArrayList
$loadedUsers=new-object system.collections.arraylist
$userCount=0

#------------- function hasGroup ----------------------------------------#
function hasGroup($hSource,$hSearch) {

 $myGroups=$hSource|where {$hSearch -contains $_}
 $myStatus=[string]::IsNullOrEmpty($myGroups)
 if ($myStatus -eq $true)  {
  #write-host "$myGroups...  $myStatus"
  return $false;
 }
 return $true;

}#end function hasGroup
#-------------function hasGroup -----------------------------------------#


#-------------function loadApps ----------------------------------------#
function loadApps($cache=$true,$cacheFile="apps.cache") {
 $appInfo=@{}
 $arApps=new-object System.Collections.ArrayList
 #We are now assuming that we will operate from Cache mode only.
 <#
 if ($cache -eq $false) {
  write-host "Loading Applications from Live Servers..." 
  $apps=Get-RDRemoteApp|sort -property DisplayName
  
  $retval=foreach ($app in $apps) {
   $appInfo=@{}
   $appInfo.Name=$app.displayName
   $appInfo.Groups=$app.usergroups -split ","
   $arApps.add($appInfo)
   #write-host $appInfo.Name
  }#end foreach ($app in $apps)
  return $arApps
 }#end if ($cache -eq $false)
 #>
 if ($cache -eq $true) {
  write-host "Loading Applications from cache file $cacheFile"
  $apps=get-content -Path $cacheFile
  $retval=foreach($app in $apps) {
   $app=$app.replace("$myDomain\","")
   $appInfo=@{}
   $app=$app -split ","
   $lastGroup=$app.count-1
   $appGroups=$app[1..$lastGroup]
   $appInfo.Name=$app[0]
   $appInfo.Groups=$appGroups
   $arApps.add($appInfo)
  }#end foreach($app in $apps)
  return $arApps
 }#end if ($cache -eq $true)
}#end function loadApps
#-------------end function loadApps ------------------------------------#


#-------------Program Logic----------------------------#

$masterTimer.start() #Starts the timer

$myApps=loadApps -cache $useCache #loads the apps
write-host "Apps Loaded in " $masterTimer.Elapsed
$masterTimer.restart()#restarts the timer

#$myUsers=loadUsers -cache $useCache #-group "MYDOMAIN\myGroup" #loads users from group

#$loadedUsers is a global variable set near the top of the script.
#write-host $loadedUsers.count "Users loaded in " $masterTimer.Elapsed

$loadedUsers=get-content $userCacheFile
$userCount=$loadedUsers.count


$masterTimer.restart()
$appCount=$myApps.Count
write-host "Ready to process $appCount apps..."
$appPass=0
$pass=0
$scale=0
$userCount=$loadedUsers.count
$appList=$myApps.Name -join ","
$header="Username,$appList"

$header|out-file -FilePath $auditFile

$userVal=foreach($user in $loadedUsers) {
 $pass++
 $elapsed=$masterTimer.Elapsed
 $user=$user -split ","
 $userName=$user[0]
 $lastGroup=$user.count-1
 $userGroups=$user[1..$lastGroup]
 $isArray=$userGroups -is [system.array]
 #write-host "Username: $userName"
 #write-host "Groups: $userGroups"
 #write-host "Group is array? " $isArray
 
 if ($scale -eq 0) {$scale=1}
 
 
 if ($pass % $scale -eq 0) {
  $percentComplete=($pass/$userCount) *100
  $percentText="{0:N2}" -f $percentComplete
  if ($userCount-$pass -lt 25) {$scale=1}
  if ($userCount-$pass -ge 25) {$scale=5}
  if ($userCount-$pass -ge 100) {$scale=10}
  if ($userCount-$pass -ge 500) {$scale=25}
  if ($userCount-$pass -ge 1000) {$scale=50}
  $message="Checking User $pass of $userCount ($percentText, Elapsed: $elapsed) `r"
  write-host -nonewline $message
 }
 #write-progress -id 1 -activity $message -PercentComplete $percentComplete
 $appPass=0
 $line="$username,"

 $appVal=foreach($app in $myApps) {
  $appPass++
  $appName=$app.Name
  $appGroups=$app.Groups

  #$appStatus="Checking $appName"
  #$appComplete=($appPass/$appCount)*100
  #write-progress -id 2 -activity $appStatus -PercentComplete $appComplete
  $inGroup=hasGroup -hSource $userGroups -hSearch $appGroups
  #if ($pass % 5 -eq 0) {
  # write-host "User Groups: $userGroups"
  # write-host "App Groups: $appGroups"
  #}

  if ($inGroup -eq $true) {$line+="x,"} else {$line+="-,"}
  #write-host "$appName says $inGroup for $userName"
  #exit
 }#end $appVal=foreach($app in $myAppss
  $line=$line.substring(0,$line.length-1)
  
  $line|out-file -append -FilePath $auditFile

}#end $userVal=foreach($user in $myUsers)

write-host "`n"
write-host "Script finished... " $masterTimer.Elapsed

$masterTimer.stop()
