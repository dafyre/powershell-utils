﻿ <# 
 .SYNOPSIS
  This command updates the Cache files for the Remote Apps and named user groups.  Best used as a job in Task Scheduler.

 .EXAMPLE
  ./rappUpdate.ps1 -userGroups "MYDOMAIN\Group1","MYDOMAIN\Group2",..."MYDOMAIN\GroupN"
   Will download the entire list of users and their group membershipsAD infrastructure. and saves it into "myUsers.cache"
   Fair Warning: This script can take a while to run on large AD Groups (34k Users caches in approximately 1hr)

 .DESCRIPTION
 
  DATE       UpdatedBy Details
   02/23/2016 BW       Resource usage consumes a large amount of memory that should be freed by the various garbage collection checks that are run throughout each process.
                       The memory is not being freed in these cases.
  02/02/2016  BW       Initial Creation

#>

Param(
[Parameter(Mandatory=$true) ] [string[]] $userGroups
)

function garbageCollection ($passCount=10,$pauseMS=0){
 for ($i=0;$i -lt $passCount; $i++) {
  [GC]::Collect()
  sleep -Milliseconds $pauseMS
 }

}

$date=[DateTime]::Now
$day=$date.day.tostring("00")
$month=$date.month.tostring("00")
$year=$date.year.tostring("0000")
$auditFile="full_audit_$month$day$year.csv"

$runTime = new-object system.diagnostics.stopwatch
$runTime.start()

del apps.cache

./rappCache -appCache

write-host "Application cache replaced in $($runtime.Elapsed)"

$runTime.restart()

./rappCache.ps1 -userCache -userCacheGroups $userGroups

write-host "User Cache replaced in $($runTime.Elapsed)"
write-host "Performing Garbage collection..."

garbageCollection -passCount 20 -pauseMS 500

$runTime.restart()
write-host "Beginning Audit process...."

./rappaudit -usecache -auditFile $auditFile

write-host "Full audit run completed in $($runTime.Elapsed)."
$runtime.stop()
