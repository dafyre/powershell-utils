﻿<#
 .SYNOPSIS
 Forces a log off of the specified users.

 Parameters:
  -noGrid: Prevents usage of the out-GridView for manually selecting user session(s) to log off.  Generally used to force a log off of all sessions for a given user.
  -user: Specifies the username to force the logoff with.  (in the format of domain\user.name)
  -server: specifies the RDS server to search for the User sessions
  -userSession: Specifies the unified session id of the user session to kill

  Notes: This command can kill more than one user session for a user when the -useSession parameter is issued!

  .EXAMPLE
  ./killUser.ps1 -user "MYDOMAIN\user.name"
  This will display a gridview of all sessions for the user.  This will include full RDP sessions, remote apps, and VDI sessions.

  .EXAMPLE
  ./killUser.ps1 -user "MYDOMAIN\user.name" -noGrid
  This will kill all user sessions for the given user without displaying a gridview.

  .DESCRIPTION
  Created By: Brant Wells
  Last Update: 9/28/2015
  Update Notes: 
   (10/12/2015)
    * Displays User's logon domain in the search results.
   (9/30/2015)
    * Brings Out-Gridview display in line with UserSessionDetails.


#>
param(
[Parameter(ParameterSetName='DisplayGrid',Mandatory=$false)] [switch] $noGrid,
[Parameter(ParameterSetName='SingleUser',Mandatory=$true)] [string] $user,
[Parameter(Mandatory=$false)][int] $userSession
)
import-module remotedesktop
$debug=$PSBoundParameters['Debug']
$noUser=[string]::IsNullOrEmpty($user)
$useGrid=!$noGrid

function getRDSessions($rdSessions) {
 $details=@()
 $rdSessions|foreach{
  $userName=$_.username
  $domain=$_.domainName
  $appType=$_.applicationtype
  $hostServer=$_.hostserver
  $activeServer=$_.servername
  $sessionID=$_.sessionID
  $unifiedSession=$_.unifiedSessionId
  $createTime=$_.createTime
  $disconnectTime=$_.disconnectTime
  $sessionState=$_.sessionState
  $collectionName=$_.collectionName

  if ($appType -eq 'rdpinit.exe' -and $activeServer -notlike 'MSVDI*') {
    $appType='RemoteApp'
  } elseif ($activeServer -notlike 'MSVDI*') {
    $appType='Remote Desktop'
  } else {
   $appType='VDI'
  }

  $objSession=[pscustomobject]@{
   userName=$userName
   domain=$domain
   userType=$appType
   hostServer=$hostServer
   activeServer=$activeServer  #This should be the connection broker for RemoteApps and RDP Sessions
   collectionName=$collectionName
   sessionID=$sessionID
   unifiedSessionID=$unifiedSession
   connectTime=$createTime
   disconnectTime=$disconnectTime
   sessionState=$sessionState
  }
  $details+=$objSession
 }

 return $details
}


if ($debug) {
 write-host "User Session is $userSession"
 if ([string]::IsNullOrEmpty($user)) {write-host "User is not specified!"}
}

if ($userSession -eq 0 -and $useGrid -eq $false) {write-host "Killing all sessions for $user!"}

if ($userSession -eq 0) {
#$connections=get-rdusersession|select Username,ServerName,HostServer,CreateTime,UnifiedSessionID,DisconnectTime,SessionState|sort UserName,ServerName|where {$_.UserName -eq $user}
$connections=GetRDSessions(get-rdusersession|select *)|sort UserName,ServerName|where {$_.UserName -eq $user}
}

if ($userSession -ne 0) {
 #$connections=get-rdusersession|select Username,ServerName,HostServer,CreateTime,UnifiedSessionID,DisconnectTime,SessionState|sort UserName,ServerName|where {$_.UserName -eq $user -and $_.UnifiedSessionId -eq $userSession}
 $connections=$connections=GetRDSessions(get-rdusersession|select *)|select Username,ServerName,HostServer,CreateTime,UnifiedSessionID,DisconnectTime,SessionState|sort UserName,ServerName|where {$_.UserName -eq $user -and $_.UnifiedSessionId -eq $userSession}
}


if ($useGrid -eq $true) {
 if ($debug -eq $true)   {write-host "Preparing Grid View..."}
 if ($noUser -eq $true)  {
  if ($debug -eq $true)   {write-host "No User specified.  Displaying all connected sessions..."}
  #$connections=get-rdusersession|select Username,ServerName,HostServer,CreateTime,UnifiedSessionID,DisconnectTime,SessionState|sort UserName,ServerName
  $connections=GetRDSessions(get-rdusersession|select *)|sort UserName,ServerName
 }
 $killRDP=$connections|out-gridview -passthru -title "Choose connections to kill..."

 #$connections|fl
 
} else {$killRDP=$connections}


$sessionCount=0;
$killRDP|foreach{
 $sessionCount++
}

if ($debug) {
 write-host $sessionCount sessions to process...
}


if ($sessionCount -gt 0) {
 $killRDP|foreach {
  if ($debug) {write-host Invoke-RDUserLogoff -HostServer $_.HostServer -UnifiedSessionId $_.UnifiedSessionId -Force issued for $_.username}

  Invoke-RDUserLogoff -HostServer $_.HostServer -UnifiedSessionId $_.UnifiedSessionId -Force
 }
}
#>
