<#
 .SYNOPSIS
  This command sets up the Remote App options for a given collection.  If no collection is specified, it will pop up and ask.

  .DESCRIPTION
  DATE UpdatedBy Details
  03/06/2019  BW  -gateway is now required.
  05/15/2017  BW  Changed options on certificate verification due to Split Brain DNS naming.
  03/24/2017	BW	Adjusted to not automatically redirect drives until we can figure out how to name them.

#>
param(
[Parameter(Mandatory=$true)] [string] $collection,
[Parameter(Mandatory=$true)] [string] $gateway
)

import-module remotedesktop

#Global Vairables

$RDPOptions="gatewayhostname:s:$gateway`ngatewayusagemethod:i:1`ngatewaycredentialssource:i:4`nkeyboardhook:i:1`nprompt for credentials on client:i:0`nusbdevicestoredirect:s:*`ndrivestoredirect:s:`nuse multimon:i:1`nauthentication level:i:0"

$connectionBroker="" #Code block below determines connection broker, or you can hard code it.
if ([string]::IsNullOrEmpty($connectionBroker) -eq $true) {
write-host "Determining Conneciton Broker..."
$rdHAConfig=get-RDConnectionBrokerHighAvailability
$connectionBroker=$rdHAconfig.ActiveManagementServer
write-host "Connection Broker is: $connectionBroker"
}


#set-RDSessionCollectionConfiguration -connectionbroker $connectionBroker -collectionName $collection -customrdpproperty "gatewayhostname:s:$gateway`ngatewayusagemethod:i:1`nconnection type:i:3`nkeyboardhook:i:1`ndrivestoredirect:s:*`nusbdevicestoredirect:s:*"
set-RDSessionCollectionConfiguration -connectionbroker $connectionBroker -collectionName $collection -customrdpproperty $RDPOptions


