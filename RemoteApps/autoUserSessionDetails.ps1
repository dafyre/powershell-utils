﻿<#

.SYNOPSIS
  This displays a list of users that have connected to the RemoteApps, Remote Desktops, or VDI Sessions and automatically refreshes it at a set interval (default is 60 seconds)

  Note: This code is adapted from userSessionDetails which uses the out-gridview powershell command.


.DESCRIPTION
  DATE UpdatedBy Details
  04/11/2019 BW Script now Elevates privileges to Admin and will prompt on systems that have UAC enabled.
  03/14/2017 BW Adjusted filter to search for both usernames and active server names to more easily see who is connected to a given server
  09/01/2016 BW Added checkboxes for showing / hiding the Active / Connected / Disconnected users.

  08/24/2016 BW Added support to automatically pick the connection broker if it is not running on the current machine.
                Added connectionBroker parameter.  If specified, the system will not try to automatically choose.
  08/03/2016 BW Added View Session Details button for RemoteApp and Remote Desktop connections
  08/02/2016 BW Added wildcard support to search (still username only)
  08/01/2016 BW Added searching (username only)
  08/01/2016 BW Sorted Gridview by username

  07/26/2016 BW Initial revision


#>

Param(
[Parameter(Mandatory=$false) ] [string] $connectionBroker=""
)

#Necessary .Net Libraries for forms, etc.
Add-Type -AssemblyName PresentationCore,PresentationFramework

[System.Reflection.Assembly]::LoadWithPartialName('System.Windows.Forms') | out-null
[System.Reflection.Assembly]::LoadWithPartialName('System.ComponentModel') | out-null
[System.Reflection.Assembly]::LoadWithPartialName('System.Data') | out-null
[System.Reflection.Assembly]::LoadWithPartialName('System.Drawing')  | out-null

import-module remotedesktop

#Global Variables
$refreshCount=0 #number of times the view has been refreshed
$refreshInterval=60000  #number of milliseconds to wait before refreshing the data.


write-host "Checking for admin privileges..."

$user=[Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()
$isAdmin=$user.isInRole([Security.Principal.WindowsBuiltInRole] "Administrator")

if ($isAdmin -eq $false) {
    #This is how to elevate permissions.
    write-host -foregroundcolor Red "Elevating Privileges..."
    $CommandLine="-File `"" + $MyInvocation.MyCommand.Definition + "`""
    Start-Process -FilePath Powershell.exe -Verb RunAs -ArgumentList $CommandLine

}

if ($isAdmin -eq $false) {
 write-host -ForegroundColor Red "This script should have opened a new window in Administrator mode."
 exit

}


if ([string]::IsNullOrEmpty($connectionBroker) -eq $true) {
write-host "Determining Conneciton Broker..."
$rdHAConfig=get-RDConnectionBrokerHighAvailability
$connectionBroker=$rdHAconfig.ActiveManagementServer
write-host "Connection Broker is: $connectionBroker"
}



#Form Events

$cmdKillSession_Click={
 $cmdKillSession=$script:cmdKillSession
 $connectionBroker=$script:connectionBroker

 $grid=$script:grid
 $timer=$script:timer
 $timer.Stop()


 $row=$grid.currentcell.rowIndex
 $column=$grid.currentCell.columnIndex
 $dgRow= new-object System.Windows.Forms.DataGridViewRow
 
 # write-host "Processing selected row..."
 # write-host $grid.CurrentCellAddress
 # write-host $grid.Rows[$grid.currentcelladdress.y].Cells[0].Value

  [string] $user=$grid.Rows[$grid.currentcelladdress.y].Cells[0].Value.toString()
  [string] $hostServer=$grid.Rows[$grid.currentcelladdress.y].Cells[3].Value.toString()
  [string] $unifiedSessionID=$grid.Rows[$grid.currentcelladdress.y].Cells[7].Value.toString()

  $messageTitle="Kill User Session"
  $messageButton=[System.Windows.MessageBoxButton]::YesNo
  $messageBody="Do you really want to kill the session for $($user)?"
  $messageIcon=[System.Windows.MessageBoxImage]::Exclamation
  $answer=[System.Windows.MessageBox]::Show($messageBody,$messageTitle,$messageButton,$messageIcon)
  write-host "You said: $answer"
  if ($answer -eq 'Yes') {
   write-host "Killing user $user on $hostServer ... "
   Invoke-RDUserLogoff -HostServer $hostServer -UnifiedSessionId $unifiedSessionID -Force
   Invoke-Command $script:tmrRefresh_Tick

  }
  $cmdKillSession.enabled=$false
}

$cmdRefresh_Click={
  $script:timer.stop()
  Invoke-Command $script:tmrRefresh_Tick
}
$frmMain_Close={
 $frmMain=$script:frmMain
 $timer=$script:timer

 $timer.Stop()
 $timer.dispose()

 $frmMain.close()
 $frmMain.dispose()
 
}

#cmdSessionDetails_Click
$cmdSessionDetails_Click={
 $grid=$script:grid
 $row=$grid.CurrentCell.RowIndex
 $user=$grid.Rows[$row].cells['userName'].value
 $server=$grid.rows[$row].cells['hostServer'].value
 $domain=$grid.rows[$row].cells['Domain'].value
 $runCommand=".\activeApps.ps1 -serverName $server -username $user -userDomain $domain"
 write-host $runCommand
 .\activeApps.ps1 -serverName $server -userName $user -userDomain $domain

}

#grid_CellClick
$grid_Click={
 $grid=$script:grid
 $cmdKillSession=$script:cmdKillSession
 $cmdSessionDetails=$script:cmdSessionDetails
 $cmdKillSession.enabled=$true

 $row=$grid.CurrentCell.RowIndex
 $userType=$grid.rows[$row].cells['userType'].value
 if ($userType -eq 'VDI') {
  write-verbose "Session Details are not available for VDI sessions."
  $cmdSessionDetails.enabled=$false
 } else {$cmdSessionDetails.enabled=$true}

}


#Timer Refresh Tick
$tmrRefresh_Tick={
 $script:frmMain.text='Loading user session details...'
 $script:timer.interval=$script:refreshInterval

 $filter=$script:txtFilter
 $filter=$filter.text


 $script:timer.stop()
 #write-host "Timer stopped!"

 write-host "Loading sessions...$($script:refreshCount)"
 $rdSessions=Get-RDUserSession -ConnectionBroker $connectionBroker
 #counters
 $active=0
 $connected=0
 $disconnected=0

 #$script:frmMain.Refresh()
 $details=new-object System.Collections.ArrayList
 
 $rdSessions|foreach{
 $userName=$_.username
 $appType=$_.applicationtype
 $hostServer=$_.hostserver
 $domain=$_.domainname
 $activeServer=$_.servername
 $sessionID=$_.sessionID
 $unifiedSession=$_.unifiedSessionId
 $createTime=$_.createTime
 $disconnectTime=$_.disconnectTime
 $sessionState=$_.sessionState
 $collectionName=$_.collectionName

 if ($appType -eq 'rdpinit.exe' -and $activeServer -notlike 'GCVDI*') {
   $appType='RemoteApp'
 } elseif ($activeServer -notlike '*VDI*') {
   $appType='Remote Desktop'
 } else {
  $appType='VDI'
 }

 
 $objSession=[pscustomobject]@{
  userName=$userName
  domain=$domain
  userType=$appType
  hostServer=$hostServer
  activeServer=$activeServer  #This should be the connection broker for RemoteApps and RDP Sessions
  collectionName=$collectionName
  sessionID=$sessionID
  unifiedSessionID=$unifiedSession
  connectTime=$createTime
  disconnectTime=$disconnectTime
  sessionState=$sessionState
 }
 
 $doAdd=$false

 if ($objSession.Sessionstate -eq "STATE_ACTIVE" -and $script:chkActive.checked -eq $true) {
   $active++
   $doAdd=$true
 }
 if ($objSession.Sessionstate -eq "STATE_CONNECTED" -and $script:chkConnected.checked -eq $true) {
   $connected++
   $doAdd=$true
 }
 if ($objSession.Sessionstate -eq "STATE_DISCONNECTED" -and $script:chkDisconnected.checked -eq $true) {
   $disconnected++
   $doAdd=$true
 }
  
  #write-host "Do Add? $doAdd"

  if ($doAdd -eq $true) {$x=$details.add($objSession)}


}


if ($filter -ne "") {
 $objDetails=new-object System.Collections.ArrayList
 $sortedDetails=new-object System.Collections.ArrayList
 
 if ($filter -like "*" -eq $false) {
  write-host "Filter has no wildcard!"
  $filter="*$filter*"
 }

 foreach ($objItem in $details) {
  if ($objItem.userName -like "$filter" -or $objItem.activeServer -like "$filter") {
   $objDetails +=$objItem
  }
 }
 
 $objDetails=$objDetails|Sort-Object -Property userName
 $objDetails|foreach{
  $sortedDetails.add($_)
 }

} else {
 $details=$details|sort-object -Property userName
 $sortedDetails=new-object System.Collections.ArrayList

 $details|foreach {
  $sortedDetails.add($_)
 }
}
#write-host "Finished loading sessions..."
$script:grid.DataSource=$sortedDetails

$script:grid.refresh()

#$script:frmMain.text="Data loaded! ($($script:refreshCount))"
$script:frmMain.text="User Session Details (Active: $active, Connected: $connected, Disconnected: $disconnected)"

$script:frmMain.refresh()


$script:timer.start()
#write-host "Timer restarted!"
$script:refreshCount++
}
#end tmrRefresh_Tick


#txtFilter_KeyUp

$txtFilter_KeyUp={
 #write-host "Key Pressed! ( $($_.KeyCode) )"

 if ($_.KeyCode -eq 'Return') {
  write-host "Filtering results..."
  $script:timer.stop()
  Invoke-Command $script:tmrRefresh_Tick

 }
}

#Main Form
$frmMain = new-object System.Windows.Forms.Form
$frmMain.text="User Session Details"
$frmMain.size=new-object System.Drawing.Size(800,600)


#Data Grid to hold the list of user sessions
$grid=new-object System.Windows.Forms.DataGridView
$grid.DataBindings.DefaultDataSourceUpdateMode=0
$grid.name="grdDetails"
$grid.DataMember=''
$grid.TabIndex=0
$grid.Location=new-object System.Drawing.Size(13,48)
$grid.Dock=[System.Windows.Forms.DockStyle]::Top
$grid.width=800
$grid.height=400
$grid.top=1
$grid.left=1
$grid.MultiSelect=$false
$grid.ReadOnly = $true
$grid.add_CellClick($grid_Click)

#Timer Object to refresh the data grid
$timer=new-object System.Windows.Forms.Timer
$timer.Interval=1000
$timer.add_tick($tmrRefresh_Tick)

$cmdKillSession = new-object System.Windows.Forms.Button
$killLocationTop=$grid.Top + $grid.height + 10
$cmdKillSession.Location = new-object System.Drawing.Size(1,$killLocationTop)
$cmdKillSession.left=10
$cmdKillSession.text="Kill Session"
$cmdKillSession.Enabled=$false
$cmdKillSession.add_click($cmdKillSession_Click)

$cmdSessionDetails = new-object System.Windows.Forms.Button
$cmdSessionDetails.text="View Session Details"
$cmdSessionDetails.Location=new-object System.Drawing.Size(1,$killLocationTop)
$cmdSessionDetails.left = $cmdKillSession.Left + $cmdKillSession.width + 10
$cmdSessionDetails.width=155
$cmdSessionDetails.enabled=$false

$cmdSessionDetails.add_click($cmdSessionDetails_Click)



$txtFilterLocationTop=$cmdKillSession.top+$cmdKillSession.height+10

$lblFilter=new-object System.Windows.Forms.Label
$lblFilter.width=45
$lblFilter.text="Filter: "
$lblFilter.top=$txtFilterLocationTop
$filterFont=$lblFilter.Font
$filterBold=new-object System.Drawing.Font($filterFont.FontFamily,$filterFont.Size, [System.Drawing.FontStyle]::Bold)
$lblFilter.Font=$filterBold



$txtFilter=new-object System.Windows.Forms.TextBox
$txtFilter.Location=new-object System.Drawing.Size(1,$txtFilterLocationTop)
$txtFilter.name='txtFilter'
$txtFilter.Text=""
$txtFilter.top=$txtFilterLocationTop
$txtFilter.left=$lblFilter.left + $lblFilter.width + 10
$txtFilter.add_KeyUp($txtFilter_KeyUp)

<#
$chkActive=new-object System.Windows.Forms.Checkbox
$chkActive.top=$txtFilterLocationTop
$chkActive.left=$txtFilter.left+$txtFilter.width+25
$chkActive.text="Active Sessions"
$chkActive.checked=$true
#>

$chkActive=new-object System.Windows.Forms.Checkbox
$chkActive.top=$txtFilterLocationTop+$txtFilter.height+25
$chkActive.left=$txtFilter.left
$chkActive.text="Active Sessions"
$chkActive.checked=$true

$chkConnected=new-object System.Windows.Forms.Checkbox
$chkConnected.top=$chkActive.top
$chkConnected.left=$chkActive.left+$chkActive.width+25
$chkConnected.text="Connected Sessions"
$chkConnected.width=150
$chkConnected.checked=$true

$chkDisconnected=new-object System.Windows.Forms.Checkbox
$chkDisconnected.top=$chkActive.top
$chkDisconnected.left=$chkConnected.left+$chkConnected.width+25
$chkDisconnected.text="Disonnected Sessions"
$chkDisconnected.width=150
$chkDisconnected.checked=$true

$cmdRefresh=new-object system.windows.forms.button
$cmdRefresh.text="Refresh"
$cmdRefresh.top=$chkActive.top+$chkActive.height+15
$cmdRefresh.left=$chkActive.Left
$cmdRefresh.add_click($cmdRefresh_Click)


$frmMain.controls.add($grid)
$frmMain.controls.add($cmdKillSession)
$frmMain.controls.add($cmdSessionDetails)
$frmMain.controls.add($lblFilter)
$frmMain.controls.add($txtFilter)
$frmMain.controls.add($chkActive)
$frmMain.controls.add($chkConnected)
$frmMain.controls.add($chkDisconnected)
$frmMain.controls.add($cmdRefresh)

#write-host $txtFilterLocationTop

$timer.start()
$retVal=$frmMain.ShowDialog()

$timer.stop()
$timer.Dispose()
